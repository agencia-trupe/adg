function displayAlert(data){
    var type = 'alert-' + data.status;
    $('.top-alert').hide().addClass(type).fadeIn('fast', function(){
      $('.alert-content').text(data.texto);
    });
}

function getBaseUrl(target){
  return location.protocol + '//' + location.hostname + '/' + target
}

/*setTimeout(function(){
  var data = [{name:'Teste2',value:'2'},{name:'Teste1',value:'1'}];
   $.each(data, function(idx, obj){ 
            var tag_template = ich.tag_template({name:obj.name, value:obj.value});
            $('#myTags').prepend(tag_template);
  });
}, '1000');*/

$(function(){
  //Selecionador de datas
  $( "#datepicker" ).datepicker({dateFormat: "dd/mm/yy"});

  $('input.tag').tagedit({
    autocompleteURL:'/perfis/admin_especialidades/serve_json',
    allowAdd: false,
  });

  //Tags
  $("#myTags").tagit({
    fieldName: 'tag[]',
    tagSource: function( request, response ) {
    //console.log("1");
    $.ajax({
        type: 'POST',
        url: getBaseUrl('tags/admin_tags/get_tags'), 
        data: { term:request.term },
        dataType: "json",
        success: function( data ) {
          response( $.map( data, function( item ) {
            var resposta = {label:item.nome, value:item.id};
            console.log(resposta);
            return {
              label: item.nome,
              value: item.id,
            }
          })
          );
        }
    });
  }
  });

  //Formulários de adição de mídia
  var newimage = $('.newimage');
  var newyoutube = $('.newyoutube');
  var formimage = $('.form-image-file');
  var formyoutube = $('.form-youtube-video');

  newimage.click(function(){
    formimage.slideDown('slow');
  });

  newyoutube.click(function(){
    formyoutube.slideDown('slow');
  });

   $('.form-image-file').submit(function(e) {
      e.preventDefault();
      form_data = {
        'ajax':'1',
        'titulo': $('.image-title').val()
      }
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.hostname + "/site_midia/admin_site_midia/image_upload",
         secureuri      : false,
         fileElementId  : 'media',
         dataType    : 'json',
         data:form_data,
         success  : function(data){
            var alerta = {'status':data.status, 'texto' : data.msg}
            if(data.status == 'error')
            {
              displayAlert(alerta);
            }
            else
            {
              $('.image-title').val('');
              var newImageTemplate = ich.newimagetemplate({'filepath':data.thumb_url, 'filename':data.image_name, 'alt':form_data.titulo});
              $('.available-images').append(newImageTemplate).slideDown('slow');
              displayAlert(alerta);
            }
         }
      });
      return false;
   });

  $('.form-youtube-video').submit(function(){
    var form_data = {
      'ajax':'1',
      'youtube_url':$('.youtube_url').val()
    }
    $.ajax({
      url: location.protocol + "//" + location.host + "/site_midia/admin_site_midia/new_youtube",
      type: 'POST',
      async : false,
      data: form_data,
      success  : function (data)
      {
         data = JSON.parse(data);
         var alerta = {'status':data.status, 'texto' : data.msg}
         if(data.status == 'error')
         {
           displayAlert(alerta);
         }
         else
         {
           var template = {'thumbpath':data.thumbnail_url, 'videoid':data.video_id}
           var newVideoTemplate = ich.newvideotemplate(template);
           $('.available-images').append(newVideoTemplate).slideDown('slow');
           displayAlert(alerta);
         }
      }
    });
    return false;
  });

  $(document.body).on('click', '.new-image-thumb', function(){
    var img = $(this).children('img').data('name');
    var alt = $(this).children('img').attr('alt');
    console.log(img);
    var shortcode = '[ai:imagesc nome=' + img + '|alt='+ alt + ' ]';
    $('textarea.tinymce').append(shortcode);
    console.log(shortcode);
    return false;
  });

  $(document.body).on('click', '.new-video-thumb', function(){
    var video = $(this).children('img').data('id');
    console.log(video);
    var shortcode = '[ai:videosc id=' + video + ']';
    $('textarea.tinymce').append(shortcode);
    console.log(shortcode);
    return false;
  });

});
$(document.body).on('click', '.btn-reordenar', function(){
    $("#projeto-images").sortable({'disabled': false, 'delay':'100'});
    $("#projeto-images li").css({'cursor':'pointer'});
    $('.fotos-lista').addClass('reordena');
    $(this).removeClass('btn-info btn-reordenar')
           .addClass('btn-warning btn-salva-ordem')
           .text('Salvar ordem')
    return false;
  }).on('click', '.btn-salva-ordem', function(){
      $(this).removeClass('btn-warning btn-salva-ordem')
             .addClass('btn-info btn-reordenar')
             .text('Reordenar');
      $.ajax({
        type: "POST",
        url: location.protocol + "//" + location.hostname + "/painel/perfil/sort_fotos/",
        data: $("#projeto-images").sortable("serialize"),
        success: function(status){
          $("#projeto-images").sortable( "option", "disabled", true );
          $("#projeto-images li").css({'cursor':'default'});
          $(".fotos-lista").removeClass('reordena');
          var alerta = {'status':'success', 'texto':'Fotos reordenadas com sucesso.'}
          displayAlert(alerta);
        }
      });
      return false;
});
$(function(){
  /**
   * Fecha os alertas do topo.
   */
  $('.top-alert').on('click', '.close', function(){
    $('.top-alert').slideUp('fast', function(){
      $(this).removeClass('alert-success alert-error');
      $('.alert-content').text('');
    });
    return false;
  });
  /**
   * Exibe o formulário de adição de fotos;
   */
  $('.btn-adicionar-foto').click(function(){
    $('.form-adicionar-foto').slideDown().removeClass('invisible');
    return false;
  });
  $('.btn-adicionar-foto-cancela').click(function(){
    $('.form-adicionar-foto').slideUp();
    return false;
  });
  /**
   * Exclui uma foto relacionada a um projeto
   */
  function deletaFoto(target){
    var foto = target.parent();
    $.ajax({
        type: "POST",
        url: location.protocol + "//" + location.hostname + "/painel/perfil/deleta_foto/",
        data: {
          'ajax' : 1,
          'foto_id' : target.data('id')
        },
        success: function(msg){
          obj = JSON.parse(msg);
          var alerta = {'status':obj.status, 'texto':obj.msg }
          if(obj.status == "success"){
            $(foto).hide();
            displayAlert(alerta);
          } else {
            displayAlert(alerta);
          }
        }
    });
  }
  $(document.body).on('click', '.btn-delete', function(){
    deletaFoto($(this));
    return false;
  });
  /*$(document.body).on('click', 'btn-delete', function(){
    
    return false;
  });*/
  function displayReordena()
  {
    if($('.btn-reordenar').hasClass('invisible'))
    {
      $('.btn-reordenar').removeClass('invisible');
    }
  }
  $('#projetos-upload').submit(function(e) {
      e.preventDefault();
      $.ajaxFileUpload({
         url         : location.protocol + "//" + location.hostname + "/painel/perfil/adiciona_foto/",
         secureuri      : false,
         fileElementId  : 'projeto-foto-upload',
         dataType    : 'json',
         data        : {
            'ajax' : 1,
            'perfil_id' : $('.id').val()
         },
         success  : function (data, status)
         {
            var alerta = {'status':data.status, 'texto' : data.msg}
            if(data.status == 'error')
            {
              displayAlert(alerta);
            }
            else
            {
              console.log(data);
              var foto_template = ich.foto_template(data);
              console.log(foto_template);
              displayReordena();
              $('#projeto-images').append(foto_template);
              displayAlert(alerta);
            }
         }
      });
      return false;
   });
});