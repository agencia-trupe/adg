//Jquery Accordion - empresas
$(".accordionLink").click(function() {
    var id = $(this).data('panel');
    $( "#accordion" ).accordion( "option", "active", id );
});

$(function() {
  function insertCount(slide_curr,slide_count){
    $('#slide-counter').html((slide_curr + 1) +' de '+ slide_count);
  };

  var slider = $('.bxslider').bxSlider({
    auto: true,
    pager: false,
    mode:'fade',
    onSliderLoad: function (){
      var slide_count = slider.getSlideCount();
      var slide_curr = slider.getCurrentSlide();
      insertCount(slide_curr,slide_count);
    },
    onSlideNext: function (){
      var slide_count = slider.getSlideCount();
      var slide_curr = slider.getCurrentSlide();
      insertCount(slide_curr,slide_count);
    },
    onSlidePrev: function (){
      var slide_count = slider.getSlideCount();
      var slide_curr = slider.getCurrentSlide();
      insertCount(slide_curr,slide_count);
    }
  });

  //Muda o calendário ao clicar na navegaçao
  function changeCal(ano, mes)
  {
    $.ajax({
        url: location.protocol + "//" + location.host + "/calendario/serve_events_calendar/" + ano + '/' + mes,
        type: 'GET',
        async : false,
        success: function(response) {
          response = JSON.parse(response);
          $('.calendar-wrapper').html(response.calendar);
          $('.prev_link').data('year', response.prev.year);
          $('.prev_link').data('month', response.prev.month);
          $('.next_link').data('year', response.next.year);
          $('.next_link').data('month', response.next.month);
          $('.month_name').text(response.month_name + ' | ' + response.year);
        }
      });
  }

  $('.nav_link').click(function(){
    var year = $(this).data('year');
    var month = $(this).data('month');
    changeCal(year, month);
    return false;
  });

  //Select arquivos
  $('.arquivo_ano').dropkick({
    change: function (value, label) {
      $.ajax({
        url: location.protocol + "//" + location.host + "/blog/arquivo/serve_json/" + value,
        type: 'GET',
        async : false,
        success: function(response) {
          $('.calendario ul.arquivo').fadeOut('slow', function(){
            $('.calendario ul.arquivo').empty().html(response).fadeIn('slow');
            console.log(response);
          });
          
        }
      });
    }
  });

  $('.arquivo_eventos').dropkick({
    change: function (value, label) {
      $.ajax({
        url: location.protocol + "//" + location.host + "/calendario/serve_json/" + value,
        type: 'GET',
        async : false,
        success: function(response) {
          $('.calendario ul.arquivo').fadeOut('slow', function(){
            $('.calendario ul.arquivo').empty().html(response).fadeIn('slow');
            console.log(response);
          });
          
        }
      });
    }
  });

  
  $("#accordion").accordion({ 
    autoHeight: false,
    collapsible: true, 
  });
  $( "#tabs" ).tabs();
  $("a[rel^='prettyPhoto']").prettyPhoto({
      allow_resize: false,
      overlay_gallery: false,
      show_title: false,
      deeplinking: false,
      markup: '<div class="pp_pic_holder"> \
            <div class="ppt">&nbsp;</div> \
            <div class="pp_content_container"> \
                <div class="pp_content"> \
                  <div class="pp_loaderIcon"></div> \
                  <div class="pp_fade"> \
                                    <a class="pp_close" href="#">Close</a> \
                    <a href="#" class="pp_expand" title="Expand the image">Expand</a> \
                    <div class="pp_hoverContainer"> \
                      <a class="pp_next" href="#">next</a> \
                      <a class="pp_previous" href="#">previous</a> \
                    </div> \
                    <div id="pp_full_res"></div> \
                    <div class="pp_details"> \
                      <div class="pp_nav"> \
                        <a href="#" class="pp_arrow_previous">Previous</a> \
                        <a href="#" class="pp_arrow_next">Next</a> \
                      </div> \
                      <div class="pp_description_wrapper"> \
                      <div class="pp_description" style="display: block"></div> \
                      </div> \
                      {pp_social} \
                    </div> \
                  </div> \
                </div> \
            </div> \
          </div> \
          <div class="pp_overlay"></div>',
    });
    
});

//Verifica o suporte ao HTML Placeholder - home/contato
jQuery(function() {
  jQuery.support.placeholder = false;
  test = document.createElement('input');
  if('placeholder' in test) jQuery.support.placeholder = true;
});

//Hack para navegadores que não oferecem suporte ao HTML5 Placeholder - home/contato
$(function() {
  if(!$.support.placeholder) { 
    var active = document.activeElement;
    $(':text').focus(function () {
      if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
        $(this).val('').removeClass('hasPlaceholder');
      }
    }).blur(function () {
      if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
        $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
      }
    });
    $(':text').blur();
    $(active).focus();
    $('form').submit(function () {
      $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
    });
  }
});

//Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
//contato - contato
$(function() {
 $('#contato-form').submit(function() {
    $('#message').html('Enviando...'); 
    var form_data = {
      nome : $('.nome_form').val(),
      email : $('.email_form').val(),
      telefone : $('.telefone_form').val(),
      assunto : $('.assunto_form').val(),
      mensagem : $('.mensagem_form').val(),
       ajax : '1'
    };
    $.ajax({
      url: location.protocol + "//" + location.host + "/contato/ajax_check",
      type: 'POST',
      async : false,
      data: form_data,
      success: function(msg) {
        $('#message').empty().html(msg);
      }
    });
    return false;
  });
}); 

