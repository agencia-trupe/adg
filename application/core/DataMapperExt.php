<?php 

class DataMapperExt extends DataMapper {
    function __construct($id = NULL) {
        parent::__construct($id);
    }

    var $created_field = 'created';
    var $updated_field = 'updated';

    // Add your method(s) here, such as:

    // only get if $this wasn't already loaded
    function get_once()
    {
        if( ! $this->exists())
        {
            $this->get();
        }
        return $this;
    }

    function fetchAll($list = NULL)
    {
        if ($list) $this->order_by($list[0], $list[1]);

        $this->get();
        
        $arr = array();
        
        foreach($this->all as $pagina)
        {
            $arr[] = $pagina;
        }

        if(sizeof($arr)) return $arr;

        return FALSE;
    }

    function  fetch_limit($limit, $offset)
    {
        $this->get($limit, $offset);
        
        $arr = array();
        
        foreach($this->all as $pagina)
        {
            $arr[] = $pagina;
        }
        if( ! sizeof($arr)) return FALSE;

        return $arr;
    }

    function fetch_by_key($list = NULL, $key, $value)
    {
        if($list) $this->order_by($list[0], $list[1]);

        $this->where($key, $value)
             ->get();

        $arr = array();

        foreach($this->all as $pagina)
        {
            $arr[] = $pagina;
        }
        
        if( ! sizeof($arr)) return FALSE;
        return $arr;
    }


    function fetch_object($field, $value)
    {
        $this->where($field, $value)
             ->get();
        
        if( ! $this->exists() ) return FALSE;
        
        return $this;
    }

    function insert($dados)
    {
        foreach ($dados as $key => $value)
        {
            $this->$key = $value;
        }

        if( ! $this->save() ) return FALSE;
        
        return ($this->id);
    }

    function change($dados)
    {
        $this->where('id', $dados['id']);
        
        $update_data = array();
        
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        
        if( ! $this->update($update_data)) return FALSE;

        return TRUE;
    }


    function deletar($id)
    {

        $this->where('id', $id)
             ->get();
        
        if( ! $this->delete()) return FALSE;
        
        return TRUE;
    }

    function getSlug($object_id)
    {
        $this->where('id', $object_id)
             ->select('slug')
             ->get('1');

        if( ! $this->exists()) return FALSE;

        return $this->slug;
    }

    /**
     * Exibe a barra lateral
     * 
     * @param integer $categoria_mae_id id da categoria mãe
     * 
     * @return string                   
     */
    function get_sidebar($categoria_mae_id)
    {
        $object = new $this;
        $paginas = $object
                    ->fetch_by_key(NULL, 'categoria_mae_id', $categoria_mae_id);

        $sidebar = null;
        foreach($paginas as $pagina)
        {
            $sidebar .=  '<li>';
            $sidebar .=  '<a class="archive-link" href="';
            $sidebar .=  site_url($pagina->slug);
            $sidebar .=  '">' . $pagina->titulo . '</a></li>';
        }
        return $sidebar;
    }
}