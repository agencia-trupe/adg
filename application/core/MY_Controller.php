<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MY_Controller Class
 *
 *
 * @package Base Project
 * @subpackage  Controllers
 */
class MY_Controller extends MX_Controller {
    public function __construct() {
        parent::__construct();
    }
}

class ProfillingController extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(TRUE);
    }

}

class BlogController extends MX_Controller
{
    public $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('blog/blog_model', 'blog');
        $this->load->model('tags/tag');
        $this->data['pagina'] = 'blog';
        $this->data['categoria_mae'] = 'blog';
        //$this->load->library('shortcodes');
        $this->data['archive'] = $this->get_archive('array');
        $this->data['tag_list'] = $this->tag->get_menu();
    }

    /**
     * Obtem o arquivo do calendário retornando no formato passado como
     * parâmetro.
     * 
     * @param mixed $format json ou array
     * 
     * @return mixed retorno no formato passado como parâmetro
     */
    public function get_archive($format, $ano = NULL)
    {
        if($format = 'array')
        {
            $archive = $this->blog->get_archive();
        }
        return $archive;
    }
}

class AuthController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri', uri_string());
            $this->session->set_flashdata('message', 'You need to log in.');
            redirect('/login');
        }
    }
}

class CrudController extends AuthController
{
    public $model_path;
    public $model;
    public $data;
    public $path;

    public function __construct($model_path, $module, $path = NULL)
    {
        parent::__construct();
        $this->data['module'] = $module;
        $model = explode('/', $model_path);
        $this->model = $model[1];
        $this->load->model($model_path);
        if(isset($path)) $this->path = $path;
    }

    public function index()
    {
        $this->listar();
    }

    public function listar()
    {
        $model = $this->model;
        $this->data['result'] = $this->$model->fetchAll($this->list);
        $this->data['conteudo'] = $this->data['module'] .'/admin/' . $model . '/lista';
        $this->load->view('start/template', $this->data);
    }

    public function editar($key)
    {
        $model = $this->model;
        $this->data['acao'] = 'editar';
        $this->data['object'] = $this->$model->fetch_object('id', $key);
        $this->data['conteudo'] = $this->data['module'] . '/admin/'. $model .'/edita';
        $this->load->view('start/template', $this->data);
    }

    public function cadastrar()
    {
        $model = $this->model;
        $this->data['acao'] =  'cadastrar';
        $this->data['conteudo'] = $this->data['module'] . '/admin/'. $model .'/edita';
        $this->load->view('start/template', $this->data);
    }

    public function processa()
    {
        $model = $this->model;

        if( ! $this->form_validation->run($this->model))
        {
            $this->data['acao'] = 'editar';
            $this->data['object'] = $this->$model->fetch_object('id', $this->input->post( 'id' ), 'id');
            $this->data['conteudo'] = $this->data['module'] . '/admin/' . $model . '/edita';
            $this->load->view('start/template', $this->data);
        }
        $post = array();

       
        //Itera sobre os dados do post e atribui suas chaves e valores para
        //o array $post
        foreach($_POST as $key => $value)
        {
            $post[$key] = $value;
        }
        
        //Verifica se algum dos dados do post é do tipo data e realiza 
        //as conversões necessárias
        if(isset($post['data']))
        {
            $post['data'] = $this->_filtra_data($post['data']);
        }
        
        $form = null;

        if($form == 'blog')
        {
            if(! isset($post['destaque']))
            {
                $post['destaque'] = FALSE;
            }
        }
        
        //Verifica se foi feita a postagem de um arquivo de imagem
        //e faz o upload caso verdadeiro
        if(sizeof($_FILES) && strlen($_FILES["imagem"]["name"])>0)
        {
            //Configurações para o upload
            $config['upload_path'] = './assets/img/eventos/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '2000';
            $config['max_width']  = '1600';
            $config['max_height']  = '1200';

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('imagem'))
            {

                //Caso a tentativa de upload não seja bem sucedida, retorna
                //para a página de edição, exibindo o erro
                $this->data['error'] = array('error' => $this->upload->display_errors());
                $this->data['acao'] = 'editar';
                $this->data['object'] = $this->$model->fetch_object('id', $post['id'] );
                $this->data['conteudo'] = $this->data['module'] . '/admin/'. $model .'/edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {

                $this->load->library('image_moo');
                //Resposta do upload
                $upload_data = $this->upload->data();

                //Caminho para o arquivo atual
                $file_uploaded = $upload_data['full_path'];
                //Caminho para o arquivo alterado
                $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                if(
                    $this->image_moo->load($file_uploaded)
                        ->resize(600,600,$pad=FALSE)
                        ->save($new_file,true)
                    )
                {
                    $post['imagem'] = $upload_data['file_name'];
                }
                else
                {
                    //Caso a tentativa de alteração da imagem não seja 
                    //bem sucedida, retorna para a página de edição, 
                    //exibindo o erro
                    $this->data['error'] = 'Erro de upload';

                    $this->data['acao'] = 'editar';
                    $this->data['object'] = $this->$model->fetch_object('id', $this->input->post( 'id' ), 'id');
                    $this->data['conteudo'] = $this->data['module'] . '/admin/'. $model .'/edita';
                    $this->load->view('start/template', $this->data);
                }
            }
        }

        if($this->$model->change($post))
        {
            $this->session->set_flashdata('success', 'Registro alterado com sucesso');
            redirect('painel/' . $this->data['painel_module']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                Tente novamente ou entre em contato com o suporte');
            redirect('painel/' . $this->data['painel_module'] . '/edita/' . $post['id']);
        }
    }

    public function processa_cadastro()
    {
        $model = $this->model;
        if(!$this->form_validation->run($this->model))
        {

            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = $this->data['module'] . '/admin/'. $model .'/edita';
            $this->load->view('start/template', $this->data);
        }
        else
        {

            $post = array();

            //Itera sobre os dados do post e atribui suas chaves e valores para
            //o array $post
            foreach($_POST as $key => $value)
            {
                $post[$key] = $value;
            }

            //Verifica se algum dos dados do post é do tipo data e realiza 
            //as conversões necessárias
            if(isset($post['data']))
            {
                $post['data'] = $this->_filtra_data($post['data']);
            }

            //Verifica se foi feita a postagem de um arquivo de imagem
            //e faz o upload caso verdadeiro
            if(strlen($_FILES["imagem"]["name"])>0)
            {

                //Configurações para o upload
                $config['upload_path'] = './assets/img/eventos/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2000';
                $config['max_width']  = '1600';
                $config['max_height']  = '1200';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('imagem'))
                {

                    //Caso a tentativa de upload não seja bem sucedida, retorna
                    //para a página de edição, exibindo o erro
                    $this->data['error'] = array('error' => $this->upload->display_errors());
                    $this->data['acao'] = 'editar';
                    $this->data['object'] = $this->$model->fetch_object('id', $post['id'] );
                    $this->data['conteudo'] = $this->data['module'] . '/admin/edita';
                    $this->load->view('start/template', $this->data);
                }
                else
                {
                    $this->load->library('image_moo');
                    //Resposta do upload
                    $upload_data = $this->upload->data();

                    //Caminho para o arquivo atual
                    $file_uploaded = $upload_data['full_path'];
                    //Caminho para o arquivo alterado
                    $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                    if(
                        $this->image_moo->load($file_uploaded)
                            ->resize(600,600,$pad=FALSE)
                            ->save($new_file,true)
                        )
                    {
                        $post['imagem'] = $upload_data['file_name'];
                    }
                    else
                    {
                        //Caso a tentativa de alteração da imagem não seja 
                        //bem sucedida, retorna para a página de edição, 
                        //exibindo o erro
                        $this->data['error'] = 'Erro de upload';

                        $this->data['acao'] = 'cadastrar';
                        $this->data['conteudo'] = $this->data['module'] . '/admin/'. $model .'/edita';
                        $this->load->view('start/template', $this->data);
                    }
                }
            }

            if($this->$model->insert($post))
            {
                $this->session->set_flashdata('success', 'Registro incluido com sucesso');
                redirect('painel/' . $this->data['painel_module']);
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível incluir o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/' . $this->data['painel_module']);
            }
        }
    }


    public function apagar($id)
    {
        $model = $this->model;
        $return_path = (isset($this->path)) ? 'painel/' . $this->path 
                                                : 'painel/' . $this->data['painel_module'];

        if($this->$model->deletar($id))
        { 
            $this->session->set_flashdata('success', 'Registro apagado com sucesso');
            redirect($return_path);
        }
        else
        {
            $this->session->set_flashdata('error', 'Não foi possível incluir o registro.
                Tente novamente ou entre em contato com o suporte');
            redirect($return_path);
        }
    }

    private function _filtra_data($post)
    {
        $data = explode('/', $post);
        $data = $data[1] . '/' . $data[0] . '/' . $data[2];
        return strtotime($data);
    }
}

/* End of file  */
/* Location: ./application/core/ */