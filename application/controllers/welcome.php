<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends DisplayController
{
    public function index()
    {
        $quotes = array(
            'C.A.R. Hoare, The 1980 ACM Turing Award Lecture' 
            => 'There are two ways of constructing a software design: 
            One way is to make it so simple that there are obviously 
            no deficiencies and the other way is to make it so 
            complicated that there are no obvious deficiencies.',

            'E. W. Dijkstra' => 'The computing scientist’s main challenge 
            is not to get confused by the complexities of his own making.',

            'Gordon Bell' => 'The cheapest, fastest, and most reliable ,
            components are those that aren’t there.',

            'Ken Thompson' => 'One of my most productive days was throwing away 
            1000 lines of code.',

            'Ken Thompson' => 'When in doubt, use brute force.',

            'Jeff Sickel'  => 'Deleted code is debugged code.'
        );

        $author = array_rand($quotes);
        $quote = $quotes[$author];

        echo '"' . $quote . '" &middot ' . $author;
    }
}