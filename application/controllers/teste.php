<?php

class Teste extends MX_Controller
{
	public function index()
	{
		echo get_slug('Açsçéáàãẽõĩáéŕýúí');

		$this->load->library('event_calendar');

		$eventos = array();

		$eventos['04'] = array(
			'03' => array(
					'link' => site_url('calendario/dia/2013/05/3'),
					array(
						'title' => 'Evento teste',
					),
					array(
						'title' => 'Evento teste',
					)
				),
			'07' => array(
				'title' => 'Evento teste',
				),
			'08' => array(
				'title' => 'Evento teste',
				),
			);

		$calendar = $this->event_calendar->build(2013, 04, $eventos['04']);
		
		print_r($calendar);
	}

	public function autocomplete()
	{
		$autocompletiondata = array(
		    3 => 'Hazel Grouse',
		    4 => 'Common Quail',
		    5 => 'Common Pheasant',
		    6 => 'Northern Shoveler',
		    7 => 'Greylag Goose',
		    8 => 'Barnacle Goose',
		    9 => 'Lesser Spotted Woodpecker',
		    10 => 'Eurasian Pygmy-Owl',
		    11 => 'Dunlin',
		    13 => 'Black Scoter',
		    14 => 'Eurasian Wryneck',
		    15 => 'Little Owl',
		    16 => 'Eurasian Curlew',
		    17 => 'Ruff',
		    18 => 'Little Tern',
		    19 => 'Merlin',
		    20 => 'Bluethroat',
		    21 => 'Redwing',
		    22 => 'Wood Nuthatch',
		    23 => 'Firecrest',
		    24 => 'Goldcrest',
		    25 => 'Bearded Parrotbill',
		    26 => 'Chaffinch',
		    27 => 'Brambling',
		    28 => 'Hawfinch',
		    29 => 'Goldcrest',
		);

		$term = $this->input->get('term');

		if(isset($term)) {
		    $result = array();
		    foreach($autocompletiondata as $key => $value) {
		        if(strlen($term) == 0 || strpos(strtolower($value), strtolower($term)) !== false) {
		            $result[] = '{"id":"'.$key.'","label":"'.$value.'","value":"'.$value.'"}';
		        }
		    }
		    
		    echo "[".implode(',', $result)."]";
		}
	}

	public function relations()
	{
		$atual = array('a', 'b', 'c', 'd');

		$adicionado = array('a', 'b', 'c', 'd', 'e', 'f');

		$removido = array('a', 'b', 'd');

		$ambos = array('g', 'h', 'i');

		print_r(array_diff($atual, $adicionado));
		print_r(array_diff($atual, $removido));
		print_r(array_diff($atual, $ambos));
		print_r(array_diff($atual, $atual));
		print_r(array_diff($ambos, $atual));

	}
}