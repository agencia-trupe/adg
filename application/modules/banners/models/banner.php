<?php
class Banner extends Datamapper
{
    var $table = 'banners';
    
    function get_all()
    {
        $s = new Banner();
        $s->order_by('posicao', 'asc');
        $s->get();

        $arr = array();
        foreach ($s->all as $slide)
        {
            $arr[] = $slide;
        }

        return $arr;
    }

    function cadastra_imagem($dados){

        $slide = new Banner();

        $slide->titulo = $dados['titulo'];
        $slide->link = $dados['link'];
        $slide->imagem = $dados['imagem'];

        if($slide->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    function atualiza_imagem($dados){

        $slide = new Banner();
        $slide->where('id', $dados['id']);
        $update = $slide->update(array(
            'titulo' => $dados['titulo'],
            'link' => $dados['link'],
            'imagem' => $dados['imagem'],
        ));
        return $update;
    }

    function atualiza($dados){

        $slide = new Banner();
        $slide->where('id', $dados['id']);
        $update = $slide->update(array(
            'titulo' => $dados['titulo'],
            'link' => $dados['link'],
        ));
        return $update;
    }

    function get_slide($id)
    {
        $slide = new Banner();
        $slide->where('id', $id);
        $slide->limit(1);
        $slide->get();

        return $slide;
    }

    function delete_slide($id){
        $slide = new Banner();
        $slide->where('id', $id)->get();


        if($slide->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
}