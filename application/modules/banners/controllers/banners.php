<?php
/**
* File: banners.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Banners
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Banners extends MX_Controller
{
	
	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Carrega a ação principal
	 * 
	 * @return
	 */
	public function index()
	{
		
	}

	public function display($posicao)
	{
		$this->load->model('banners/banner');
		$banner = new Banner();
		$banner->where('posicao', $posicao)->get(1);
		echo '<a href="' . $banner->link . '">'
			.'<img src="'. base_url('assets/img/banners/' . $banner->imagem) 
			.'"></a>';
	}
}
/* End of file blog.php */
/* Location: ./modules/blog/controllers/blog.php */