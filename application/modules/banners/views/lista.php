<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Banners </legend>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span2">Posição</th><th class="span3">Imagem</th><th>Título</th><th>link</th><th>Ações</th>
            </tr>
            <tbody>
                <?php foreach ($banners as $banner): ?>
                <tr>
                    <td><?=$banner->posicao_descricao; ?></td>
                    <td><img width="100%" height="100%" src="<?=base_url(); ?>assets/img/banners/<?=$banner->imagem; ?>" alt=""></td>
                    <td><?=$banner->titulo; ?></td>
                    <td><?=$banner->link; ?></td>
                    <td><a href="<?=site_url('painel/banners/editar/' . $banner->id); ?>" class="btn btn-info btn-mini">Editar</a>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </thead>
    </table>
</div>