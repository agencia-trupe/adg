<div class="span12">

<legend><?php echo $acao == 'editar' ? 'Editar Banner' : 'Novo Banner' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php
    echo form_open_multipart(($acao == 'editar') ? 'painel/banners/atualiza' : 'painel/banners/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php if($acao == 'edita')
      {
        echo form_hidden('acao', 'edita');
      }
      else{
        echo form_hidden('acao', 'cadastra');
      }
      ?>  
      </div>
    <?php if($acao == 'editar') : ?>
    <?php echo
    form_hidden('id', set_value('id', $banner->id));
    echo form_hidden('posicao', $banner->posicao );
    ?>
  <?php endif; ?>
    <div class="row-fluid">
        <div class="span9">
            <label for="">Título</label>
            <?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $banner->titulo : ''), 'class' => 'span12',)); ?>
            <?php echo form_error('titulo'); ?>
        </div>
    </div>
    <br>
  <div class="row-fluid">
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="link">Link</label>
            <div class="controls">
              <?php echo form_input('link', set_value('link', $acao == 'editar' ? $banner->link : ''), 'class="span6"'); ?>
              <?php echo form_error('link'); ?>
            </div>
      </div>
     <?php if($acao == 'editar'): ?>
     <?php if($banner->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/banners/<?php echo $banner->imagem; ?>" alt="<?php echo $banner->titulo; ?>" >
     <?php endif; ?>
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Adicionar / Alterar Imagem' : 'Imagem'); ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
  </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>