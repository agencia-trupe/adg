
            
          </div>

</div><!--/row-->      
<hr>

      <footer>
      </footer>

    </div><!--/.fluid-container-->
<!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script> 
  <script  src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  <!-- scripts concatenated and minified via ant build script-->
  <script  src="<?php echo base_url(); ?>assets/js/admin.js"></script>
  <script  src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/tiny_mce/jquery.tinymce.js"></script>
     
    <script type="text/javascript">
    $(function(){$("textarea.tinymce").tinymce({
    // Location of TinyMCE script
    script_url : "<?php echo base_url(); ?>assets/js/vendor/tiny_mce/tiny_mce.js",
     
    // General options
    theme : "advanced",
    plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
     
    // Theme options
    theme_advanced_buttons1 : "styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link",
    theme_advanced_buttons3 : "bullist,numlist,|,blockquote",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    
    max_chars : 260,
    height: 300,
    max_chars_indicator : "characterCounter",  
    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js"
    });});
    </script>

<script>

$('#removelink').click(function(e) {  
  
    e.preventDefault();  
    thisHref    = $(this).attr('href');  
  
    if(confirm('Você tem certeza que deseja remover esse registro?')) {  
        window.location = thisHref;  
    }  
  
});  
</script>
<script id="foto_template" type="text/html">
    <li class="projeto-foto" id="foto_{{ id }}">
      <img width="121" height="121" style="margin-bottom:10px;"
           src="/assets/img/perfil/thumbs/{{ nome }}" 
           alt="">
      <a href="#" data-id="{{ id }}" 
                  class="btn btn-delete btn-mini btn-danger">
          <i class="icon-trash icon-white"></i></a>
    </li>;
  </script>
	<script id="tag_template" type="text/html">
    <li class="tagit-choice ui-widget-content ui-state-default ui-corner-all tagit-choice-editable">
      <span class="tagit-label">{{name}}</span>
      <a class="tagit-close"><span class="text-icon"></span>
      <span class="ui-icon ui-icon-close"></span></a>
      <input type="hidden" style="display:none;" value="{{value}}" name="tags">
    </li>
  </script>
  <script id="newimagetemplate" type="text/html">
    <a href="#" class="new-image-thumb">
    <img width="120" height="120" data-name="{{filename}}" src="{{filepath}}" alt="{{alt}}"/>
    </a>
  </script>
  <script id="newvideotemplate" type="text/html">
    <a href="#" class="new-video-thumb">
    <img width="120" height="120" data-id="{{videoid}}" src="{{thumbpath}}" />
    </a>
  </script>
  <!-- Change UA-XXXXX-X to be your site's ID -->
  <script>
    window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
    Modernizr.load({
      load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
    });
  </script>


  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
  
</body>
</html>