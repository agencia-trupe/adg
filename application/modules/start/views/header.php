<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><? $this->seo->get_title(); ?></title>
  <meta name="description" content="">
  <!-- Mobile viewport optimized: h5bp.com/viewport -->
  <meta name="viewport" content="width=device-width">
  <?php 
    if(isset($metas)){
         echo $metas['title'];
         echo $metas['description'];
         echo $metas['tags'];
    }
  ?>
  <?php if(isset($fb)): ?>
  	<meta property="og:title" content="<?php echo $fb['title']; ?>" />
	<meta property="og:description" content="<?php echo $fb['description']; ?>" />
	<meta property="og:image" content="<?php echo $fb['image']; ?>" />
  <?php endif; ?>  

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/interagir-base.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ui-lightness/jquery-ui-1.8.21.custom.css" >
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.lightbox-0.5.css">

  

  <?php 
  if(isset($css_files)): ?>
<?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
    <?php endif; ?> 


  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="<?php echo base_url(); ?>assets/js/libs/modernizr-2.5.3.min.js"></script>
</head>
<body>

<div class="topnav">
	<div class="inner">
	<ul>
	<?php if(!$this->tank_auth->is_logged_in()): ?>
	<li><a href="<?php echo base_url(); ?>login">Fazer login</a></li>
	<li><a href="<?php echo base_url(); ?>registrar">Criar conta</a></li>
	<?php else: ?>
    <?php if($this->tank_auth->is_role('admin')): ?>
    <li><a href="<?php echo base_url() . 'turmas/'?>">Painel de Controle</a></li>
    <?php endif; ?>
    <li><a href="<?php echo base_url() . 'usuarios/perfis/detalhe/'?>">Minha Conta</a></li>
    <li><a href="<?php echo base_url() . 'logout'; ?>">Sair</a></li>
    <?php endif; ?>
	</ul>
	</div>

</div>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
    <div id="container">
        <header>
            <div id="logo-info">
            <a href="<?php echo base_url(); ?>" title="Concept" id="logo"></a>
            <div id="header-info">
                <p>Uma empresa do
                <a href="http://grupodsrh.com.br/" target="_blank" title="Grupo DSRH®">Grupo DSRH®
                </a></p>
            </div>
            </div>
            <div id="divider"></div>
            <div id="myCarousel" class="carousel slide">
            <div class="carousel-inner">
              <a href="<?php echo base_url(); ?>workshops/detalhe/etica_e_postura_profissional_" class="item active">
                <img src="<?php echo base_url(); ?>assets/img/slides/banner_etica.jpg" alt="">
              </a>
              <a href="<?php echo base_url(); ?>palestras/detalhe/como_manter_meu_colaborador_motivado" class="item">
                <img src="<?php echo base_url(); ?>assets/img/slides/banner_motivacao.jpg" alt="">
                
              </a>
              <a href="<?php echo base_url(); ?>workshops/detalhe/gerenciando_seu_auto_desenvolvimento" class="item">
                  <img src="<?php echo base_url(); ?>assets/img/slides/banner_auto-desenvolvimento.jpg" alt="">
                
              </a>
               <a href="<?php echo base_url(); ?>cursos/detalhe/liderana_de_alta_performance_" class="item">
                  <img src="<?php echo base_url(); ?>assets/img/slides/banner_lideranca-performance.jpg" alt="">
                
              </a>
               <a href="<?php echo base_url(); ?>cursos/detalhe/a_arte_de_dar_e_receber_feedback_" class="item">
                  <img src="<?php echo base_url(); ?>assets/img/slides/banner_feedback.jpg" alt="">
                
              </a>
               <a href="<?php echo base_url(); ?>palestras/detalhe/pcd_entendendo_a_diferena_incluso_de_pessoas_com_deficincia_nas_organizaes_" class="item">
                  <img src="<?php echo base_url(); ?>assets/img/slides/banner_pcds.jpg" alt="">
                
              </a>
            </div>
            
          </div>
            	 <div class="clearfix"></div>
        </header>
        
		<div id="corpo">
                    <div id="anteEsquerda">
				<ul><li></li></ul>
			</div>
