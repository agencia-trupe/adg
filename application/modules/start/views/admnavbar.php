    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url(); ?>">ADG</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="<?php echo ($module == 'blog') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/blog'); ?>">Blog</a>
              </li>
              <li class="<?php echo ($module == 'calendario') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/calendario'); ?>">Calendário</a>
              </li>
              <li class="<?php echo ($painel_module == 'paginas') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/paginas'); ?>">Páginas</a>
              </li>
              <li class="<?php echo ($painel_module == 'historia') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/historia'); ?>">História</a>
              </li>
              <li class="<?php echo ($module == 'contato') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/contato'); ?>">Contato</a>
              </li>
              <li class="<?php echo ($painel_module == 'perfil') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('painel/perfil/editar'); ?>">Meu Perfil</a>
              </li>
              <li>
                <div class="alert span4 top-alert">
                  <span class="alert-content"></span>
                  <a class="close" href="#">&times;</a>
                </div>
              </li>
              
            </ul>
            <ul class="nav pull-right">
                <li><?php echo anchor('logout', 'Sair'); ?></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">