</div>
  <footer>
      <div id="footer-content"> 
        <ul id="bottom-nav">
            <li><h3>Concept</h3></li>
            <li><a href="<?php echo base_url() . 'paginas/politica-cancelamento'; ?>">Política de Cancelamento</a></li>
            <li><a href="<?php echo base_url() . 'paginas/politica-privacidade'; ?>">Política de Privacidade</a></li>
            <li><a href="<?php echo base_url() . 'contato'; ?>">Fale Conosco</a></li>
        </ul>

        <div id="pagamento">
            <h3>Formas de Pagamento</h3>
            <div id="cartoes">
                <span>Cartões de Crédito</span>
                <a href="#"></a>
            </div>
            <div id="bancos">
                <span>Débito online e boleto bancário</span>
                <a href="#"></a>
            </div>
        </div>
        <a id="dsrh-footer" target="_blank" href="http://institutointeragir.com.br">
              
        </a>
      </div>
      <div class="clearfix"></div>
      <p>Todos os direitos reservados &copy; 2012 - Concept</p>
     
  </footer>


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>

  <!-- scripts concatenated and minified via build script -->
 
  <script src="<?php echo base_url(); ?>assets/js/libs/bootstrap.min.js"></script>
    <script>
    $('.carousel').carousel({
  interval: 8500
    })
  </script>
  <script>
        $(function(){
        // bind change event to select
        $('#auto-inscricao').bind('change', function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
        });
    </script>
    <script>  
$(function ()  
{ $("#example").popover();  
});  
</script>  
    <script>
        $(function(){
        // bind change event to select
        $('#qtd-inscricao').bind('change', function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/libs/jquery.ui.datepicker-pt-BR.js"></script>
		      <script src="<?php echo base_url(); ?>assets/js/libs/jquery.lightbox-0.5.js"></script>
		      
    <script type="text/javascript">
$(function() {
    $('.gallery a').lightBox(); // Select all links in object with gallery ID
    // This, or...
   });
</script>

  <script type="text/javascript">
    $(function(){
    $("#datepicker").datepicker({dateFormat: 'yy-mm-dd',  yearRange:"-90:+0" , changeYear: true
});
    });
  </script>


    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=270195273079984";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
      <?php 
  if(isset($js_files)): ?>
      <?php foreach($js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
    <?php endif; ?>
  <!-- end scripts -->
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=8210536; 
var sc_invisible=1; 
var sc_security="6d469abd"; 
</script>
<script type="text/javascript"
src="http://www.statcounter.com/counter/counter.js"></script>
<noscript><div class="statcounter"><a title="hit counter"
href="http://statcounter.com/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/8210536/0/6d469abd/1/"
alt="hit counter"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
       
  <script>
    var _gaq=[['_setAccount','UA-34180603-1'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>
</body>
</html>