<?php
/**
* File: Admin_site_midia.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_site_midia
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_site_midia extends CrudController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	var $data;

	/**
	 * Critério de ordenação da lista do CRUD.
	 *
	 *  @var boolean
	 */
	var $list;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct($model_path = 'site_midia/site_midia_model', $module = 'site_midia');
		$this->list = FALSE;
		$this->load->model('site_midia_model', 'midia');
	}
	
	public function listar($tipo)
	{
		if($tipo == NULL)
			$tipo = 'imagem';
		$this->load->model('site_midia/site_midia_model', 'midia');
		$this->load->library('pagination');
		$pagination_config = array(
					'base_url'       => base_url() . 'site_midia/' . $tipo . '',
					'total_rows'     => $this->db->get('site_midia')->num_rows(),
					'per_page'       => 8,
					'num_links'      => 5,
					'next_link'      => '&gt',
					'prev_link'      => '&lt',
					'first_link'     => TRUE,
					'last_link'      => TRUE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		$this->pagination->initialize($pagination_config);

		//Obtendo resultados no banco
		$this->data['result'] = $this
								->midia
								->fetch_limit($pagination_config['per_page'], 
									$this->uri->segment(3));
		
		//Definição de parcial de conteúdo e retorno da view.
		//$this->data['main_content'] = 'site_midia/admin/lista';
		$this->load->view('site_midia/admin/lista', $this->data);
	}

	public function image_upload()
	{
		$this->load->model('site_midia_model', 'midia');
		//var_dump($_FILES); exit;
		if(sizeof($_FILES) && strlen($_FILES["media"]["name"])>0)
        {
            //Configurações para o upload
            $config['upload_path'] = './assets/img/images/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '8000';
            $config['max_width']  = '4000';
            $config['max_height']  = '4000';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('media'))
            {

                $this->load->library('image_moo');
                //Resposta do upload
                $upload_data = $this->upload->data();

                //Caminho para o arquivo atual
                $file_uploaded = $upload_data['full_path'];
                //Caminho para o arquivo alterado
                $new_file = $upload_data['file_path'] . './507_' . $upload_data['file_name'];

                if(
                    $this->image_moo->load($file_uploaded)
                        ->resize(507,507,$pad=FALSE)
                        ->save($new_file,true)
                    )
                {
                	$dados = array(
                		'titulo' => $this->input->post('titulo'),
                		'tipo' => 'imagem',
                		'filename' => $upload_data['file_name'],
                		);
                	if($this->midia->add($dados))
                	{
                		$data['status'] = 'success';
            			$data['msg'] = 'imagem adicionada com sucesso';
            			$data['thumb_url'] = base_url('assets/img/images') . '/507_' . $upload_data['file_name'];
                		$data['image_name'] = '507_' . $upload_data['file_name'];
                	}
                }
            }
        }
        else
        {
            //Caso a tentativa de alteração da imagem não seja 
            //bem sucedida, retorna para a página de edição, 
            //exibindo o erro
            $data['status'] = 'error';
        	$data['msg'] = 'erro ao realizar upload';
        }
		echo json_encode($data);
	}

	public function new_youtube()
	{
		$video_url = $this->input->post('youtube_url');
		$video_data = $this->_get_youtube_video_data($video_url);
		$media_data = array(
			'titulo' 		=>	htmlentities($video_data['title']),
			'video_thumb'	=>	(string) $video_data['thumbnail_url'], 
			);
		if($this->midia->add($media_data))
		{
			$data = array();
			$data['status'] = 'success';
			$data['msg'] = 'Vídeo adicionado com sucesso';
			$data['thumbnail_url'] = $media_data['video_thumb'];
			$data['video_id'] = $video_data['id'];

			echo json_encode($data);
		}
		else{
			$data = array();
			$data['status'] = 'error';
			$data['msg'] = 'Erro ao salvar vídeo';
		}
	}

	private function _get_youtube_video_id($youtube_url)
	{
		parse_str( parse_url( $youtube_url, PHP_URL_QUERY ), $my_array_of_vars );
		return $my_array_of_vars['v'];   
	}

	private function _get_youtube_video_data($youtube_video_url)
	{
		//The Youtube's API url
		define('YT_API_URL', 'http://gdata.youtube.com/feeds/api/videos?q=');
		 
		//The video id.
		$video_id = $this->_get_youtube_video_id($youtube_video_url);
		 
		//Using cURL php extension to make the request to youtube API
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, YT_API_URL . $video_id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//$feed holds a rss feed xml returned by youtube API
		$feed = curl_exec($ch);
		curl_close($ch);
		 
		//Using SimpleXML to parse youtube's feed
		$xml = simplexml_load_string($feed);

		$entry = $xml->entry[0];
		//If no entry whas found, then youtube didn't find any video with specified id
		if(!$entry) exit('Error: no video with id "' . $video_id . '" whas found. Please specify the id of a existing video.');
		$media = $entry->children('media', true);
		$group = $media->group;
		 
		$title = $group->title;//$title: The video title
		$desc = $group->description;//$desc: The video description
		$vid_keywords = $group->keywords;//$vid_keywords: The video keywords
		$thumb = $group->thumbnail[0];//There are 4 thumbnails, the first one (index 0) is the largest.
		//$thumb_url: the url of the thumbnail. $thumb_width: thumbnail width in pixels.
		//$thumb_height: thumbnail height in pixels. $thumb_time: thumbnail time in the video
		list($thumb_url, $thumb_width, $thumb_height, $thumb_time) = $thumb->attributes();
		$content_attributes = $group->content->attributes();
		//$vid_duration: the duration of the video in seconds. Ex.: 192.
		$vid_duration = $content_attributes['duration'];
		//$duration_formatted: the duration of the video formatted in "mm:ss". Ex.:01:54
		$duration_formatted = str_pad(floor($vid_duration/60), 2, '0', STR_PAD_LEFT) . ':' . str_pad($vid_duration%60, 2, '0', STR_PAD_LEFT);
		 
		$video_data = array(
			'id'			=>	$video_id,
			'url'			=>  $youtube_video_url,
			'title' 		=>  $title,
			'desc'			=>	$desc,
			'keywords' 		=> 	$vid_keywords,
			'thumbnail_url'	=>	$thumb_url,
			'duration'		=>	$duration_formatted,
			);
		
		return $video_data;
	}
}
/* End of file admin_blog.php */
/* Location: ./modules/blog/controllers/admin_blog.php */