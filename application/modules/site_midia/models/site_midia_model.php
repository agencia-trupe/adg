<?php
/**
* File: blog_model.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Blog_model
 * 
 * @category ADG
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Site_midia_model extends DataMapperExt
{
	/**
	 * Nome da tabela.
	 * 
	 * @var string
	 */
	var $table = 'site_midia';

	/**
	 * Relacionamento 1 to N.
	 * 
	 * @var array
	 */
	var $has_many = array('tag');

	/**
	 * Auto popular o relacionamento.
	 * 
	 * @var boolean
	 */
	var $auto_populate_has_many = TRUE;

	public function add($dados)
	{
		$midia = new Site_midia_model();
		foreach($dados as $key => $value)
		{
			$midia->$key = $value;
		}
		if($midia->save())
		{
			return TRUE;
		}
		return FALSE;
	}
}
/* End of file blog_model.php */
/* Location: ./modules/blog/models/blog_model.php */