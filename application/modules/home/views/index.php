<div class="home">
    <div class="header">
        <div class="slideshow">
            <ul class="bxslider">
              <?php foreach ($slides as $slide): ?>
              <li>
                <a href="<?=$slide->link; ?>">
                <img src="<?=base_url(); ?>assets/img/slides/<?=$slide->imagem; ?>"  alt="<?=$slide->titulo; ?>" />
                <div class="slide-legenda">
                    <div class="clearfix"></div>
                    <div class="titulo">
                        <span>
                            <?=$slide->titulo; ?>
                        </span>
                    </div>                    
                </div>
                </a>
              </li>
            <?php endforeach; ?>
            </ul>
             <div id="slide-counter"></div>
        </div>
        <div class="banners">
            <div class="banner">
                <?php echo Modules::run('banners/display', '1') ?>
            </div>
            <div class="banner">
                <?php echo Modules::run('banners/display', '2') ?>
            </div>            
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="banner-maior">
        <?php echo Modules::run('banners/display', '3') ?>
    </div>
    <div class="clearfix"></div>
    <div class="left-content">
        <div class="blog-destaque-home">
            <?php echo Modules::run('blog/destaques') ?>
        </div>
        <div class="clearfix"></div>
        <div class="noticias-destaque-home">
            <?php echo Modules::run('blog/noticias_home') ?>
        </div>
        <div class="sociais">
            <div class="twitter-home">
                <?php echo Modules::run('twitter') ?>
            </div>
            <div class="separador"></div>
            <div class="facebook-home">
                <h1><a href="http://facebook.com/adgbr">Facebook / ADGBR</a></h1>
                <div class="facebook-outer">
                    <div class="facebook-inner">
                              <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fadgbrasil&amp;width=300&amp;height=258&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;border_color=%23fff&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:258px;" allowTransparency="true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="banner">
            <?php echo Modules::run('perfis/destaque_home') ?>
        </div>
        <div class="banner">
            <?php echo Modules::run('banners/display', '4') ?>
        </div>
        <div class="separador"></div>
        <div class="calendario-home-wrapper">
            <?php echo Modules::run('calendario/calendario_home') ?>
        </div>
        <div class="eventos-home">
            <?php echo Modules::run('calendario/destaques', 'home') ?>
        </div>
    </div>      
</div>