<?php
/**
* File: home.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Home
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Home extends ProfillingController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	public  $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->data['pagina'] = 'home';
	}
	/**
	 * Carrega a ação principal
	 * 
	 * @return object um objeto view
	 */
	public function index()
	{
		$seo_data = array(
			'title' => 'Home',
			'description' => 'ADG - Associação dos Designers Graficos do Brasil'
			);
		$this->load->library('seo', $seo_data);

		$this->load->model('admin_slideshow/slide');
		$this->data['slides'] = $this->slide->get_all();

		$this->data['conteudo'] = 'home/index';
		$this->load->view('layout/template', $this->data);
	}

	
}
/* End of file home.php */
/* Location: ./modules/home/controllers/home.php */