<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Twitter extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -   
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('feed');
		$username = 'adgbr';
		$data['tweets'] = $this->feed->twitter_feed($username, $max = 1, $cache_time = 3600, $timeout = 1, $clear_cache = true);
		$this->load->view('twitter/feed', $data);

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */