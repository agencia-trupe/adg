<div class="twitter-feed">
	<div class="feed">
		<div class="separador"></div>
		<h1>@adgbr</h1>
			<div class="content">
				<div class="seta"></div>
				<div class="tweets">
					<?php foreach($tweets as $tweet): ?>
						<div class="tweet">
							<div class="clearfix"></div>
							<a href="http://twitter.com/adgbr" class="avatar">
								<img width="48" height="48" src="<?php echo base_url('assets/img/twitter.jpeg') ?>" alt="">
							</a>
							<p><?php echo $tweet['text']; ?></p>
							<a class="time" target="_blank" href="<?php echo $tweet['link']; ?>"><?php echo date('d', strtotime($tweet['datetime'])) 
							. ' ' . month_name(strtotime($tweet['datetime']), TRUE, TRUE)
							. ' - ' .date('G', strtotime($tweet['datetime'])); ?></a>
						</div>
					<?php endforeach; ?>	
				</div>
			</div>
	</div>
</div>