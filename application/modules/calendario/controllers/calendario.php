<?php
/**
* File: calendario.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Calendario
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Calendario extends MX_Controller
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	public  $data;

	/**
	 * [__construct construtor]
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('calendario/calendario_model', 'calendario');
		$this->data['categoria_mae_id'] = '2';
		$this->data['categoria_mae'] = 'calendario';
		$this->data['pagina'] = 'calendario';
		$this->data['sidebar'] = NULL;
		$this->list = NULL;
		$this->load->helper('date');
		$this->data['archive'] = $this->get_archive('array');
		$this->data['calendario_sidebar'] = $this->_get_calendar_events(date('Y'), 5);
	}
	/**
	 * Carrega a ação principal
	 * 
	 * @return object um objeto view
	 */
	public function index()
	{
		$this->destaques();
	}

	/**
	 * Exibe os destaques no calendário
	 * 
	 * @return void
	 */
	public function destaques($local = NULL)
	{
		if(isset($local) && $local === 'home')
		{
			$destaques_qdt = 4;
		}
		else
		{
			$destaques_qtd = 8;
		}
		//Array de objetos para exibição
		$this->data['destaques'] = $this->calendario->get_destaques_calendario($destaques_qdt);
		
		//Biblioteca de SEO
		$seo_tags = array(
			'title' => 'Calendário de Eventos',
			'description' => 'Calendário de Eventos da ADG - Associação de 
			Designers gráficos do Brasil'
			);
		$this->load->library('seo', $seo_tags);

		//Definição de parcial de conteúdo e retorno da view.
		if(isset($local) && $local === 'home')
		{
			$this->load->view('calendario/destaques_home.php', $this->data);
		}
		else
		{
			$this->data['conteudo'] = 'calendario/destaques.php';
			$this->load->view('layout/template', $this->data);
		}
	}

	public function destaque_unico()
	{
		//Array de objetos para exibição
		$this->data['destaques'] = $this->calendario->get_destaques_calendario(1);
		$this->load->view('calendario/destaque_unico.php', $this->data);
		
	}

	public function calendario_home()
	{
		$data['calendario'] = $this->_get_calendar_events(date('Y'), 5);
		$this->load->view('calendario/calendario_home', $data);
	}

	public function eventos_home()
	{
		$data['eventos'] = $this->calendario->get_eventos_home();
		$this->load->view('calendario/eventos_home', $data);
	}



	/**
	 * Exibe a lista com todos os eventos que ainda não aconteceram
	 * 
	 * @return void
	 */
	public function lista()
	{      
		$this->load->library('pagination');
		$pagination_config = array(
					'base_url'       => base_url() . 'calendario/lista/',
					'total_rows'     => $this->db->get('eventos')->num_rows(),
					'per_page'       => 3,
					'num_links'      => 5,
					'next_link'      => '&gt',
					'prev_link'      => '&lt',
					'first_link'     => FALSE,
					'last_link'      => FALSE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		$this->pagination->initialize($pagination_config);

		//Obtendo resultados no banco
		$this->data['eventos'] = $this
								->calendario
								->fetch_limit($pagination_config['per_page'], 
									$this->uri->segment(3));

		//Biblioteca de SEO
		$seo_tags = array(
			'title' => 'Calendário de Eventos',
			'description' => 'Calendário de Eventos da ADG - Associação de 
			Designers gráficos do Brasil'
			);
		$this->load->library('seo', $seo_tags);
		
		//Definição de parcial de conteúdo e retorno da view.
		$this->data['conteudo'] = 'calendario/lista';
		$this->load->view('layout/template', $this->data);
	}

	public function view($slug)
	{
		$object = $this->calendario->fetch_object('slug', $slug);

		$this->data['object'] = $object;

		$seo_tags = array(
			'title' => $object->titulo,
			'description' => get_excerpt($object->descricao,15)
			);
		$this->load->library('seo', $seo_tags);

		$this->data['conteudo'] = 'calendario/evento';
		$this->load->view('layout/template', $this->data);
	}

	/**
	 * Obtem o arquivo do calendário retornando no formato passado como
	 * parâmetro.
	 * 
	 * @param mixed $format json ou array
	 * 
	 * @return mixed retorno no formato passado como parâmetro
	 */
	public function get_archive($format)
	{
		if($format = 'array')
		{
			$archive = $this->calendario->get_archive();
		}
		return $archive;
	}

	/**
	 * Verifica se a requisição é feita pelo próprio framework
	 * 
	 * @param ip $ip_address ip do request
	 * 
	 * @return boolean resultado da checagem
	 */
	private function _is_server_request($ip_address)
	{
		if($this->input->server('SERVER_ADDR') !== $ip_address)
		{
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Exibe a lista com todos os posts publicados
	 * 
	 * @return void
	 */
	public function serve_json($ano)
	{
		$archive = $this->_get_archive('array', $ano);
		$response = NULL;
		foreach ($archive as $year => $months)
		{
			foreach($months as $key => $value)
			{
				$response .= '<li><a class="archive-link" href="';
				$response .= site_url('calendario/mes/' . $year . '/' . $key);
				$response .= '">' . month_name($key, FALSE, TRUE);
				$response .= ' <small>(<span>'; 
				$response .= str_pad(count($key), 2, '0', STR_PAD_LEFT);
				$response .= '</span>)</small></a></li>';	
			}
		}

		echo $response;
	}

	/**
	 * Obtem o arquivo do calendário retornando no formato passado como
	 * parâmetro.
	 * 
	 * @param mixed $format json ou array
	 * 
	 * @return mixed retorno no formato passado como parâmetro
	 */
	private function _get_archive($format, $ano = NULL)
	{
		if($ano == NULL)
		{
			return $this->calendario->get_archive();
		}
		if($format == 'array')
		{
			return $this->calendario->get_archive($ano);
		}
		else
		{
			$arquivo = $this->blog->get_archive($ano);
			$result = array();
			foreach($arquivo as $mes)
			{
				foreach($mes as $key => $value)
				{
					//$result[$key]['mes'] = $key;
					$result[$key]['mes'] = $key;
					$result[$key]['posts'] = count($value);
				}
			}
			
			return json_encode($result);
		}
	}

	public function lista_eventos_mes($ano, $mes)
	{      
		$this->load->library('pagination');
		$pagination_config = array(
					'total_rows'     => $this->db
										->where(array(
											'YEAR(FROM_UNIXTIME(data))' => $ano,
											'MONTH(FROM_UNIXTIME(data))' => $mes
											))->get('eventos')->num_rows(),
					'per_page'       => 1,
					'num_links'      => 5,
					'next_link'      => '&gt',
					'prev_link'      => '&lt',
					'first_link'     => FALSE,
					'last_link'      => FALSE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		if($mes == NULL)
		{
			$mes = '01';	
		}
		
		$pagination_config['base_url'] = base_url() . 'calendario/mes/' . $ano . '/' . $mes . '/';
		$pagination_config['uri_segment'] = '5';
		
		//Obtendo resultados no banco
		$this->data['eventos'] = $this
							->calendario
						->get_archive_content($ano, $mes, $pagination_config['per_page'], 
									$this->uri->segment(5));
		$this->pagination->initialize($pagination_config);

		

		//Biblioteca de SEO
		$seo_tags = array(
			'title' => 'Calendário de Eventos',
			'description' => 'Calendário de Eventos da ADG - Associação de 
			Designers gráficos do Brasil'
			);
		$this->load->library('seo', $seo_tags);
		
		//Definição de parcial de conteúdo e retorno da view.
		$this->data['mes'] = $mes;
		$this->data['ano'] = $ano;
		$this->data['is_archive'] = 'TRUE';
		$this->data['conteudo'] = 'calendario/lista';
		$this->load->view('layout/template', $this->data);
	}

	private function _get_calendar_events($year, $month)
	{
		$events = $this->calendario->get_month_events($year, $month);
		
		$result = array();
		
		foreach($events as $event)
		{
			$result[date('d', $event->data)] = array(
													'link' => site_url('calendario/dia/' . date('Y/m/d', $event->data))
												);
		}

		$this->load->library('event_calendar');
		$result = $this->event_calendar->build($year, $month, $result);

		$return  = '<div class="calendarheader">';
		$return .= '<span class="month_name">' . $result['month_name'] . ' | ' . $result['year'] . '</span>';
		$return .= '<div class="nav-links">';
		$return .= '<a class="nav_link prev_link" data-year="' . $result['prev']['year'] . '" data-month="' . $result['prev']['month'] . '" href="#">&lt;</a>';
		$return .= '<a class="nav_link next_link" data-year="' . $result['next']['year'] . '" data-month="' . $result['next']['month'] . '" href="#">&lt;</a>';
		$return .= '</div>';
		$return .= '</div>';
		$return .= '<div class="clearfix"></div>';
		$return .= '<div class="calendar-wrapper">';
		$return .= $result['calendar'];
		$return .= '</div>';

		return $return;
	}

	public function serve_events_calendar($year, $month)
	{
		$events = $this->calendario->get_month_events($year, $month);
		
		$result = array();
		
		if($events)
		{
			foreach($events as $event)
			{
				$result[date('d', $event->data)] = array(
														'link' => site_url('calendario/dia/' . date('Y/m/d', $event->data))
													);
			}
		}

		$this->load->library('event_calendar');
		
		echo json_encode($this->event_calendar->build($year, $month, $result));
	}
}
/* End of file calendario.php */
/* Location: ./modules/calendario/controllers/calendario.php */