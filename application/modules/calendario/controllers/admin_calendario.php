<?php
/**
* File: admin_calendario.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_calendario
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_calendario extends CrudController
{
	/**
	 * Array com os dados para a view.
	 * 
	 * @var array
	 */
	var $data;

	/**
	 * Critério para ordenação da lista do CRUD.
	 * 
	 * @var boolean
	 */
	var $list;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct($model_path = 'calendario/calendario_model', $module = 'calendario');
		$this->list = FALSE;
	}
}
/* End of file admin_calendario.php */
/* Location: ./modules/calendario/controllers/admin_calendario.php */