<?php
/**
* File: calendario_model.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Calendario_model
 * 
 * @category ADG
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Calendario_model extends DataMapperExt
{
	/**
	 * Tabela referente ao modelo.
	 * 
	 * @var string
	 */
	var $table = 'eventos';

	/**
	 * Obtém os dados para o calendário de destaques
	 * 
	 * @return array array com os dados para o calendário
	 */
	function get_destaques_calendario($destaques_qtd)
	{
		$evento = new $this;

		$evento->where(array('destaque'=>TRUE, 'data >=' => time()))->get($destaques_qtd);
		
		$result_set = array();

		foreach($evento->all as $destaque)
		{
			$result_set[] = $destaque;
		}
		
		if (sizeof($result_set)) return $result_set;

		return FALSE;
	}


	function get_archive_content($ano, $mes, $limit, $offset)
	{
		$arquivo = new Calendario_model();
		$where = array();
		$arquivo->where(array(
			'YEAR(FROM_UNIXTIME(data))' => $ano,
			'MONTH(FROM_UNIXTIME(data))' => $mes,
			))->get($limit, $offset);

		$result = array();
		foreach($arquivo->all as $post)
		{
			$result[] = $post;
		}
		if(sizeof($result))
			return $result;
		return FALSE;
	}

	function get_eventos_home()
	{
		$arquivo = new Calendario_model();
		$arquivo->where(array('data >=', time()))->get(4);

		$result = array();
		foreach($arquivo->all as $post)
		{
			$result[] = $post;
		}
		if(sizeof($result))
			return $result;
		return FALSE;
	}

	/**
	 * Retorna os dados para o arquivo do calendário.
	 * 
	 * @return array dados do arquivo
	 */
	function get_archive($ano = NULL)
	{
		$list_order = array(
			'YEAR(FROM_UNIXTIME(data))',
			'DESC'
			);
		if(!$ano)
		{
			$blog = $this->fetchAll($list_order);
		}
		else
		{
			$blog = $this->fetch_by_key($list_order, 'YEAR(FROM_UNIXTIME(data))', $ano);
		}
		$result = array();
		foreach($blog as $evento)
		{
			$result[date('Y', $evento->data)][str_replace("'", "", date('m', $evento->data))][] = $evento->id;
		}
		return $result;
	}

	function get_month_events($year, $month)
	{
		$eventos = new Calendario_model();

		$eventos->where(array(
			'YEAR(FROM_UNIXTIME(data))' => $year,
			'MONTH(FROM_UNIXTIME(data))' => $month,
			))
				->get();

		return $this->_process_result($eventos);
	}

	private function _process_result($object_array)
	{
		$result = array();

		foreach($object_array->all as $row)
		{
			$result[] = $row;
		}
		if( ! sizeof($result)) return FALSE;
		return $result;
	}
}

/* End of file calendario_model.php */
/* Location: ./modules/calendario/models/calendario_model.php */