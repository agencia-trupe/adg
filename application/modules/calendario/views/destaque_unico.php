<div class="conteudo calendario destaques home">
	<?php foreach ($destaques as $destaque): ?>
			<article>
			<a href="<?php echo site_url('evento/' . $destaque->slug) ?>">
				<span class="image-container">
					<img width="220"
						 height="140"
						 src="<?php 
								echo base_url('assets/img/eventos/' . $destaque->imagem) ?>" 
						 alt="<?php echo $destaque->titulo ?>">
					<div class="clearfix"></div>
				</span>
				<span><?php echo $destaque->titulo ?></span><br>
				<p><?php echo substr($destaque->descricao, 0, 100) ?></p>
			</a>
		</article>
	<?php endforeach ?>
</div>