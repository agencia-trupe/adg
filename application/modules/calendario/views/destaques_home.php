<div class="conteudo calendario destaques home">
	<ul>
	<?php foreach ($destaques as $destaque): ?>
		<li>
			<article>
			<a href="<?php echo site_url('evento/' . $destaque->slug) ?>">
				<span class="image-container">
					<img width="185"
						 height="119"
						 src="<?php 
								echo base_url('assets/img/eventos/' . $destaque->imagem) ?>" 
						 alt="<?php echo $destaque->titulo ?>">
					<div class="clearfix"></div>
				</span>
				<span class="data">
					<?php echo date('d', $destaque->data)
								 			. ' ' . ucfirst(month_name($destaque->data, TRUE, FALSE))
								 			. ' ' . date('Y', $destaque->data) ?>
				</span>
				<h2><?php echo $destaque->titulo ?></h2>
			</a>
		</article>
		</li>
	<?php endforeach ?>
	</ul>
</div>