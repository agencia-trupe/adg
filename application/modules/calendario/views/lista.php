<div class="conteudo calendario lista">
	<h1 class="titulo">/Calendário</h1>
	<ul>
	<?php foreach ($eventos as $destaque): ?>
		<li>
			<article>
				<span class="image-container">
					<img src="<?php 
								echo base_url('assets/img/eventos/' . $destaque->imagem) ?>" 
						 alt="<?php echo $destaque->titulo ?>">
					<span class="image-meta">
						<span class="meta-data">
							<span class="mes"><?php echo month_name($destaque->data, TRUE, TRUE) ?></span>
							<span class="dia"><?php echo date('d', $destaque->data) ?></span>
						</span>
					</span>
					<div class="clearfix"></div>
				</span>
				<span class="info">
					<h2><?php echo $destaque->titulo ?></h2>
					<span class="descricao">
						<?php echo $destaque->descricao ?>
					</span>
					<h3>Serviço</h3>
					<span class="info-data">
						<b>Data:</b> <?php echo get_long_date($destaque->data) ?>
					</span>
					<div class="clearfix"></div>
					<span class="info-local">
						<b>Local:</b> <?php echo $destaque->local ?>
					</span>
					<div class="clearfix"></div>
					<span class="info-horario">
						<b>Horário:</b> <?php echo $destaque->horario ?>
					</span>
					<div class="clearfix"></div>
					<span class="info-endereco">
						<b>Endereço:</b> <?php echo $destaque->endereco ?>,
										<?php echo $destaque->cidade ?>, 
										<?php echo $destaque->uf ?>

					</span>
					<div class="clearfix"></div>
					<span class="info-mapa">
						<b>Mapa:</b>
						<a href="<?php echo $destaque->link_mapas ?>">
							Veja o local no Google Maps
						</a>
					</span>
				</span>
				<div class="clearfix"></div>
			</article>
			<div class="clearfix"></div>
		</li>
	<?php endforeach ?>
	</ul>
	<?php echo $this->pagination->create_links() ?>
</div>