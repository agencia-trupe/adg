<div class="row">
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
    <div class="alert alert-success">
        <?php echo $error; ?>
    </div>
    <?php endif; ?>
    <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Evento</legend>
    <?php if( $acao !== 'editar' )
    {
        $form = 'calendario/admin_calendario/processa_cadastro';
    }
    else
    {
        $form = 'calendario/admin_calendario/processa';
    }
    ?>
    <?=form_open_multipart($form); ?>
    <?php if($acao == 'editar'): ?>
    <?=form_hidden( 'id', $object->id ); ?>
    <?php endif; ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ($acao == 'editar') ? $object->titulo : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo'); ?>
        <br>
    <?=form_label('Descrição'); ?>
    <?=form_textarea(array(
        'name' => 'descricao',
        'value' => set_value('descricao', ($acao == 'editar') ? $object->descricao : ''),
        'class' => 'tinymce span8'
    )); ?>
    <?=form_error('texto'); ?>
    <br>
    <?=form_label('Data'); ?>
    <?=form_input(array(
        'name' => 'data',
        'value' => set_value('data', ($acao == 'editar') ? date('d/m/Y', $object->data) : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('data'); ?>
    <br>
     <?=form_label('Horário'); ?>
    <?=form_input(array(
        'name' => 'horario',
        'value' => set_value('horario', ($acao == 'editar') ? $object->horario : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('horario'); ?>
    <br>
     <?=form_label('Local'); ?>
    <?=form_input(array(
        'name' => 'local',
        'value' => set_value('local', ($acao == 'editar') ? $object->local : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('local'); ?>
    <br>
     <?=form_label('Endereço'); ?>
    <?=form_input(array(
        'name' => 'endereco',
        'value' => set_value('endereco', ($acao == 'editar') ? $object->endereco : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('endereco'); ?>
    <br>
     <?=form_label('Cidade'); ?>
    <?=form_input(array(
        'name' => 'cidade',
        'value' => set_value('cidade', ($acao == 'editar') ? $object->cidade : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('cidade'); ?>
    <br>
     <?=form_label('UF'); ?>
    <?=form_input(array(
        'name' => 'uf',
        'value' => set_value('uf', ($acao == 'editar') ? $object->uf : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('uf'); ?>
    <br>
    <!--<select>
        <?php //foreach( $templates as $template ): ?>
        <option value="<?//=str_replace('.php', '', $template); ?>">
            <?//=ucfirst(str_replace('.php', '', $template)); ?>
        </option>
        <?php //endforeach; ?>
    </select>-->
    <br>
    <div class="clearfix"></div>
    <br>

     <?php if($acao == 'editar' && $object->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/eventos/<?php echo $object->imagem; ?>" alt="" >
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?=($acao == 'editar') ? 'Alterar' : 'Cadastrar'; ?> Imagem</label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
    <!--<div class="row-fluid">
        <div class="alert alert-info span4">Criada em <?//=date( 'd/m/Y', $object->created ); ?></div>
        <div class="alert alert-success span4 ">Última atualização: 
            <?//=(isset($object->updated)) ? date( 'd/m/Y', $object->updated ) : 'Nunca'; ?></div>
    </div>-->
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/calendario/', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
</div>