<?php
class Slide extends Datamapper
{
    var $table = 'slides';
    
    function get_all()
    {
        $s = new Slide();
        $s->order_by('ordem', 'asc');
        $s->get();

        $arr = array();
        foreach ($s->all as $slide)
        {
            $arr[] = $slide;
        }

        return $arr;
    }

    function cadastra_imagem($dados){

        $slide = new Slide();

        $slide->titulo = $dados['titulo'];
        $slide->link = $dados['link'];
        $slide->ordem = $dados['ordem'];
        $slide->imagem = $dados['imagem'];

        if($slide->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    function atualiza_imagem($dados){

        $slide = new Slide();
        $slide->where('id', $dados['id']);
        $update = $slide->update(array(
            'titulo' => $dados['titulo'],
            'link' => $dados['link'],
            'ordem' => $dados['ordem'],
            'imagem' => $dados['imagem'],
        ));
        return $update;
    }

    function atualiza($dados){

        $slide = new Slide();
        $slide->where('id', $dados['id']);
        $update = $slide->update(array(
            'titulo' => $dados['titulo'],
            'link' => $dados['link'],
            'ordem' => $dados['ordem'],
        ));
        return $update;
    }

    function get_slide($id)
    {
        $slide = new Slide();
        $slide->where('id', $id);
        $slide->limit(1);
        $slide->get();

        return $slide;
    }

    function delete_slide($id){
        $slide = new slide();
        $slide->where('id', $id)->get();


        if($slide->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
}