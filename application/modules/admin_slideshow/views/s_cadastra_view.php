<div class="span12">

<legend><?php echo $acao == 'editar' ? 'Editar slideshow' : 'Novo slideshow' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php
    echo form_open_multipart(($acao == 'editar') ? 'painel/slideshow/atualiza' : 'painel/slideshow/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php if($acao == 'edita')
      {
        echo form_hidden('acao', 'edita');
      }
      else{
        echo form_hidden('acao', 'cadastra');
      }
      ?>  
      </div>
    <?php if($acao == 'editar') : ?>
    <?php echo
    form_hidden('id', set_value('id', $slide->id));
    ?>
  <?php endif; ?>
    <div class="row-fluid">
        <div class="span9">
            <label for="">Título</label>
            <?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $slide->titulo : ''), 'class' => 'span12',)); ?>
            <?php echo form_error('titulo'); ?>
        </div>
    </div>
    <br>
  <div class="row-fluid">
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="link">Link</label>
            <div class="controls">
              <?php echo form_input('link', set_value('link', $acao == 'editar' ? $slide->link : ''), 'class="span11"'); ?>
              <span class="help-inline"><?php echo form_error('link'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <label class="control-label" for="ordem">Ordem</label>
            <div class="controls">
              <?php echo form_input('ordem', set_value('ordem', $acao == 'editar' ? $slide->ordem : ''), 'class="span1"'); ?>
              <span class="help-inline"><?php echo form_error('ordem'); ?></span>
            </div>
      </div>
     <?php if($acao == 'editar'): ?>
     <?php if($slide->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/slides/<?php echo $slide->imagem; ?>" alt="<?php echo $slide->titulo; ?>" >
     <?php endif; ?>
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Adicionar / Alterar Imagem' : 'Imagem'); ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
  </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>