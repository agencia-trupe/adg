<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_slideshow extends MX_Controller
{
    var $data;
    
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'slides';
        $this->data['painel_module'] = 'slides';
    }
    function index(){

            $this->lista();
        }
    /**
     *Lista todos os fotos cadastrados atualmente
     *
     * @return [type] [description]
     */
    function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Schmillevitch - Erro de acesso';
            $this->load->view('start/templatenonav', $this->data);
        } else {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
            {
                $this->load->model('admin_slideshow/slide');
                $this->data['slides'] = $this->slide->get_all();
                
                $this->data['main_content'] = 'admin_slideshow/lista';
                $this->load->view('includes/template', $this->data);
            } else {

                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                Você precisa ser 
                                                administrador para realizar essa ação');
            redirect();

          }
        }
    }
    /**
     * Mostra a página de edição de um foto cujo id foi passado como 
     * parâmetro.
     *
     * @param  [int] $id [description]
     * @return void
     */
    

    function editar($id)
    {
        /**
         * Verifica se o usuário está logado para então prosseguir ou não.
         */
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Schmillevitch - Erro de acesso';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            //Verifica se o usuário tem nível de acesso permitido
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
            {
                $id = $this->uri->segment(4);
                if(!$id)
                {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/slideshow/lista');
                }
                else
                {
                    $this->load->model('admin_slideshow/slide');
                    
                    $this->data['title'] = 'Schmillevitch - slideshow - Editar';

                    if($this->slide->get_slide($id))
                    {
                        $this->data['slide'] = $this->slide->get_slide($id);
                        $this->data['acao'] = 'editar';
                        $this->data['main_content'] = 's_cadastra_view';
                        $this->load->view('includes/template', $this->data);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('painel/slideshow/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();
            }
        }
    }

    function cadastra()
    {

        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Schmillevitch - Erro de acesso';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
              if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
              {
                  $this->data['title'] = 'Schmillevitch - slideshow - Novo noticia';
                  
                  $this->data['acao'] = 'cadastra';
                  $this->data['main_content'] = 's_cadastra_view';
                  $this->load->view('includes/template', $this->data);
              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão.
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function salva(){

        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Schmillevitch - Erro de acesso';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $config = array(
                array(
                    'field' => 'titulo',
                    'label' => 'titulo',
                    'rules' => 'required|max_length[255]',
                ),
                array(
                    'field' => 'link',
                    'label' => 'link',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'ordem',
                    'label' => 'ordem',
                    'rules' => 'required',
                ),
            );
            $this->load->library('form_validation');
            $this->form_validation->set_rules($config);
            $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');
            $this->data['acao'] = $this->input->post('acao');

            if($this->form_validation->run() == FALSE ){
                  $this->data['title'] = 'Schmillevitch - Novo Slide';
                  
                  $this->data['main_content'] = 's_cadastra_view';
                  $this->load->view('includes/template', $this->data);
            }
            else
            {   //Verifica se foi feito o upload de uma imagem
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    $config['upload_path'] = './assets/img/slides/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2000';
                    $config['max_width']  = '1600';
                    $config['max_height']  = '1200';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {
                            $this->data['error'] = array('error' => $this->upload->display_errors());

                            $this->data['title'] = 'Schmillevitch - Novo Slide';
                            
                            $this->data['main_content'] = 's_cadastra_view';
                            $this->load->view('includes/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                        if(
                            $this->image_moo->load($file_uploaded)
                                ->resize_crop(666,370)
                                ->save($new_file,true)
                            )
                        {
                             $this->load->model('admin_slideshow/slide');

                            //prepara o array com os dados para enviar ao model
                            $dados = array(
                                    'imagem' => $upload_data['file_name'],
                                    'titulo' => $this->input->post('titulo'),
                                    'subtitulo' => $this->input->post('subtitulo'),
                                    'link' => $this->input->post('link'),
                                    'ordem' => $this->input->post('ordem'),
                                );

                            if( ! $this->slide->cadastra_imagem($dados))
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/slideshow/cadastra');
                            }
                            else
                            {
                                $this->session->set_flashdata('success', 'noticia cadastrada
                                com sucesso!');
                                redirect('painel/slideshow/lista');
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/slideshow/cadastra');
                        }
                    }
                }
                else
                {
                    $this->data['title'] = 'Schmillevitch - Novo Slide';
                    
                    $this->data['main_content'] = 's_cadastra_view';
                    $this->load->view('includes/template', $this->data);
                }
            }
        }
    }

    function atualiza()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Schmillevitch - Erro de acesso';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
            {
                $config = array(
                    array(
                    'field' => 'titulo',
                    'label' => 'titulo',
                    'rules' => 'required|max_length[255]',
                    ),
                    array(
                        'field' => 'link',
                        'label' => 'link',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'ordem',
                        'label' => 'ordem',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                if($this->form_validation->run() == FALSE )
                {
                    $id  = $this->input->post('id');
                    $this->load->model('admin_slideshow/slide');
                    
                    $this->data['title'] = 'Schmillevitch - Editar Slide';
                    $this->data['slide'] = $this->slide->get_slide($id);
                    $this->data['acao'] = 'editar';
                    $this->data['main_content'] = 's_cadastra_view';
                    $this->load->view('includes/template', $this->data);
                }
                else
                {
                    //verifica se foi postada uma imagem
                    if(strlen($_FILES["imagem"]["name"])>0)
                    {
                        $config['upload_path'] = './assets/img/slides/';
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = '2000';
                        $config['max_width']  = '1600';
                        $config['max_height']  = '1200';

                        $this->load->library('upload', $config);

                        if ( ! $this->upload->do_upload('imagem'))
                        {
                                $this->data['error'] = array('error' => $this->upload->display_errors());

                                $id  = $this->input->post('id');
                                $this->load->model('admin_slideshow/slide');
                                
                                $this->data['title'] = 'Schmillevitch - Editar Slide';
                                $this->data['slide'] = $this->slide->get_slide($id);
                                $this->data['acao'] = 'editar';
                                $this->data['main_content'] = 's_cadastra_view';
                                $this->load->view('includes/template', $this->data);
                        }
                        else
                        {
                            $this->load->library('image_moo');
                            //Is only one file uploaded so it ok to use it with $uploader_response[0].
                            $upload_data = $this->upload->data();
                            $file_uploaded = $upload_data['full_path'];
                            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                            if(
                                $this->image_moo->load($file_uploaded)
                                    ->resize_crop(666,370)
                                    ->save($new_file,true)
                                
                                )
                            {
                                 $this->load->model('admin_slideshow/slide');

                                //prepara o array com os dados para enviar ao model
                                $dados = array(
                                        'id'     => $this->input->post('id'),
                                        'imagem' => $upload_data['file_name'],
                                        'titulo' => $this->input->post('titulo'),
                                        'subtitulo' => $this->input->post('subtitulo'),
                                        'link' => $this->input->post('link'),
                                        'ordem' => $this->input->post('ordem'),
                                    );

                                if( ! $this->slide->atualiza_imagem($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/slideshow/cadastra');
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'Slide cadastrado
                                    com sucesso!');
                                    redirect('painel/slideshow/lista');
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/slideshow/cadastra');
                            }
                        }
                    }
                    else
                    {
                         $this->load->model('admin_slideshow/slide');

                                //prepara o array com os dados para enviar ao model
                                $dados = array(
                                        'id'     => $this->input->post('id'),
                                        'titulo' => $this->input->post('titulo'),
                                        'subtitulo' => $this->input->post('subtitulo'),
                                        'link' => $this->input->post('link'),
                                        'ordem' => $this->input->post('ordem'),
                                    );

                                if( ! $this->slide->atualiza($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/slideshow/cadastra');
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'Slide atualizado
                                    com sucesso!');
                                    redirect('painel/slideshow/lista');
                                }
                    }
                }

              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
              redirect();

              }
        }
    }

    function apaga($id)
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Schmillevitch - Erro de acesso';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager')  )
            {
                $id = $this->uri->segment(4);
                if(!$id)
                {
                $this->session->set_flashdata('error', 'A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                redirect('painel/slideshow/lista');
                }
                else
                {
                    $this->load->model('admin_slideshow/slide');
                    if($this->slide->delete_slide($id))
                    {
                        $this->session->set_flashdata('success', 'Registro apagado
                        com sucesso');
                         redirect('painel/slideshow/lista');
                    }
                     else
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                         realizada, tente novamente ou entre em contado com o suporte');
                        redirect('painel/slideshow/lista');
                    }
                }
            }
            else
            {
                Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser administrador para realizar essa ação');
                redirect();

            }
        }

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */