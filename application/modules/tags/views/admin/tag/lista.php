<div class="row">
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Eventos <a href="<?=site_url('painel/tags/cadastrar'); ?>"
                       class="btn btn-small btn-info">Cadastrar</a></legend>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($result as $evento): ?>
                <tr>
                    <td><?=$evento->nome; ?></td>
                    <td><?=anchor('painel/tags/editar/' . $evento->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?></a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>