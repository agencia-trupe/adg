<div class="row">
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
    <div class="alert alert-success">
        <?php echo $error; ?>
    </div>
    <?php endif; ?>
    <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Evento</legend>
    <?php if( $acao !== 'editar' )
    {
        $form = 'painel/tags/processa_cadastro';
    }
    else
    {
        $form = 'painel/tags/processa';
    }
    ?>
    <?=form_open_multipart($form); ?>
    <?php if($acao == 'editar'): ?>
    <?=form_hidden( 'id', $object->id ); ?>
    <?php endif; ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'nome',
        'value' => set_value('nome', ($acao == 'editar') ? $object->nome : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('nome'); ?>
        <br>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/calendario/', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
</div>