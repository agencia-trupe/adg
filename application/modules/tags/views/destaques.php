<div class="conteudo calendario destaques">
	<h1 class="titulo">/Calendário</h1>
	<ul>
	<?php foreach ($destaques as $destaque): ?>
		<li>
			<article>
			<a href="<?php echo site_url($destaque->slug) ?>">
				<span class="image-container">
					<img src="<?php 
								echo base_url('assets/img/eventos/' . $destaque->imagem) ?>" 
						 alt="<?php echo $destaque->titulo ?>">
					<span class="image-meta">
						<?php if(isset($destaque->uf)): ?>
						<span class="uf">
							<?php echo $destaque->uf ?>
						</span>
						<?php endif; ?>
						<span class="meta-data">
							<span class="mes"><?php echo month_name($destaque->data, TRUE, TRUE) ?></span>
							<span class="dia"><?php echo date('d', $destaque->data) ?></span>
						</span>
					</span>
					<div class="clearfix"></div>
				</span>
				<span class="data"><?php echo get_long_date($destaque->data) ?></span>
				<h2><?php echo $destaque->titulo ?></h2>
			</a>
		</article>
		</li>
	<?php endforeach ?>
	</ul>
</div>