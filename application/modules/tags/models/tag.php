<?php
/**
* File: tag.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Tag
 * 
 * @category ADG
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Tag extends DataMapperExt
{
	/**
	 * Nome da tabela.
	 * 
	 * @var string
	 */
	var $table = 'tags';

	/**
	 * Relacionamento 1 to n.
	 * 
	 * @var array
	 */
	var $has_many = array('blog_model');

	/**
	 * Auto popular relacionamento.
	 * 
	 * @var boolean
	 */
	var $auto_populate_has_many = TRUE;

	/**
	 * Retorna as tags relacionadas com o parâmetro passado.
	 * 
	 * @param string $sample fragmento de tag
	 * 
	 * @return string string json
	 */
	public function get_like($sample)
	{
		$tag_list = new Tag();
		$tag_list->like('nome', $sample)->get();
		$result_array = array();
		foreach($tag_list->all as $single_tag)
		{
			$result_array[$single_tag->id]['nome'] = $single_tag->nome;
			$result_array[$single_tag->id]['id'] = $single_tag->id;
		}
		return $result_array;
	}

	public function fetch_posts_limit($tag, $limit, $offset)
	{
		$result = array();

		$tags = new Tag();
		$tags->where('slug', $tag)->get('1');

		$result['nome'] = $tags->nome;
		
		$tags->blog_model->get_iterated();
		$total = array();
		foreach($tags->blog_model as $post)
		{
			$total[] = $post;
		}
		$result['total_posts'] = count($total);

		$tags->blog_model->get_iterated($limit, $offset);
		

		foreach($tags->blog_model as $post)
		{
			$result['posts'][] = $post;
		}

		return $result;
	}

	public function get_menu()
	{
		$tags = new Tag();
		$tags->get();
		
		$result = array();
		$tag_posts = array();

		foreach($tags->all as $tag)
		{
			foreach ($tag->blog_model as $blog) 
			{
				$posts[$tag->id][] = $blog->titulo;
			}
			if(isset($posts[$tag->id]))
			{
				$result[$tag->nome]['posts'] = count($posts[$tag->id]);
			}
			else
			{
				$result[$tag->nome]['posts'] = '0';
			}
			$result[$tag->nome]['slug'] = $tag->slug;
		}
		return $result;
	}
}

/* End of file tag.php */
/* Location: ./modules/tags/models/tag.php */