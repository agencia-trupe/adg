<?php
/**
* File: tags.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Tags
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Tags extends BlogController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	public  $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->data['is_tag'] = TRUE;
	}
	/**
	 * Carrega a ação principal
	 * 
	 * @return object um objeto view
	 */
	public function index()
	{
		$this->destaques();
	}

	/**
	 * Exibe a lista com todos os eventos que ainda não aconteceram
	 * 
	 * @return void
	 */
	public function lista($tag)
	{
		$per_page = '3';
		$offset = $this->uri->segment(4);
		$result = $this->tag->fetch_posts_limit($tag, $per_page, $offset);

		$this->data['tag_name'] = $result['nome'];

		$this->load->library('pagination');
		$pagination_config = array(
					'base_url'       => base_url() . 'blog/tag/' . $tag . '/',
					'total_rows'     => $result['total_posts'],
					'per_page'       => 3,
					'num_links'      => 5,
					'next_link'      => '&gt',
					'prev_link'      => '&lt',
					'first_link'     => FALSE,
					'last_link'      => FALSE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'  => '<li>',
					'next_tag_close' => '</li>',
					'prev_tag_open'  => '<li>',
					'prev_tag_close' => '</li>',
					'uri_segment'	 =>	'4',
			);
		$this->pagination->initialize($pagination_config);

		//Obtendo resultados no banco
		$this->data['result'] = $result['posts'];

		//Biblioteca de SEO
		$seo_tags = array(
			'title' => 'Calendário de Eventos',
			'description' => 'Calendário de Eventos da ADG - Associação de 
			Designers gráficos do Brasil'
			);
		$this->load->library('seo', $seo_tags);
		
		//Definição de parcial de conteúdo e retorno da view.
		$this->data['conteudo'] = 'blog/lista';
		$this->load->view('layout/template', $this->data);
	}
	/**
	 * Obtem o arquivo do calendário retornando no formato passado como
	 * parâmetro.
	 * 
	 * @param mixed $format json ou array
	 * 
	 * @return mixed retorno no formato passado como parâmetro
	 */
	public function get_archive($format, $ano = NULL)
	{
		if($format = 'array')
		{
			$archive = $this->blog->get_archive();
		}
		return $archive;
	}
}
/* End of file tags.php */
/* Location: ./modules/tags/controllers/tags.php */