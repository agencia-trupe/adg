<?php
/**
* File: Admin_tags.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_tags
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_tags extends CrudController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	var $data;

	/**
	 * Critério de ordenação da lista do CRUD.
	 *
	 *  @var boolean
	 */
	var $list;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct($model_path = 'tags/tag', $module = 'tags');
		$this->list = FALSE;
        $this->data['painel_module'] = 'blog';
	}

	/**
	 * Obtem as tags baseando-se no fragmento passado como parâmetro.
	 * 
	 * @return string json string
	 */
	public function get_tags()
	{  
			$sample = $this->input->post('term');   	
			$tags = $this->tag->get_like($sample);
			echo json_encode($tags);
	}

	public function processa_cadastro()
    {
        if(!$this->form_validation->run('tag'))
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'tags/admin/edita';
            $this->load->view('start/template', $this->data);
        }
        else
        {

            $post = array();

            //Itera sobre os dados do post e atribui suas chaves e valores para
            //o array $post
            foreach($_POST as $key => $value)
            {
                $post[$key] = $value;
            }

            $this->load->helper('blog');
       		$post['slug'] = get_slug($post['nome']);
       		//var_dump($post['slug']); exit;

            //Verifica se foi feita a postagem de um arquivo de imagem
            //e faz o upload caso verdadeiro

            if($this->tag->insert($post))
            {
                $this->session->set_flashdata('success', 'Registro incluido com sucesso');
                redirect('painel/tags');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível incluir o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/tags');
            }
        }
    }
}

/* End of file admin_tags.php */
/* Location: ./modules/tags/controllers/admin_tags.php */