<?php if($this->session->userdata('alert_warning')): ?>
    <div class="alert alert-error span4 offset6">
        <p>Você precisa estar logado para realizar esta ação. Faça login com seu
        nome de usuário e senha ou clique em registrar para criar uma conta de 
        usuário</p>
    </div>
    
<?php endif; ?>

<div class="span4 offset6" style="margin-top:80px;">
        <div class="well">
        <div id="login_form">
            <h2 style="color:#0086b3;">Concept</h2>
            <h4 style="color:#0086b3;">Login</h4>
            <hr>
<?php

$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
        'class' => 'span3'
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
        'class' => 'span3'
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>
<?php echo form_open($this->uri->uri_string()); ?>
<?php echo form_label($login_label, $login['id']); ?>
<?php echo form_input($login); ?>
<?php echo form_error($login['name']); ?><br><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
<?php echo form_label('Senha', $password['id']); ?>
<?php echo form_password($password); ?>
<?php echo form_error($password['name']); ?><br><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
	

	<?php if ($show_captcha) {
		if ($use_recaptcha) { ?>
	
		
			<div id="recaptcha_image"></div>
		
	
			<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
			<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
			<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>


			<div class="recaptcha_only_if_image">Enter the words above</div>
			<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
	<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" /></td>
		<?php echo form_error('recaptcha_response_field'); ?>
		<?php echo $recaptcha_html; ?>

	<?php } else { ?>
			<p>Enter the code exactly as it appears:</p>
			<?php echo $captcha_html; ?>

		<?php echo form_label('Confirmation Code', $captcha['id']); ?>
		<?php echo form_input($captcha); ?>
		<?php echo form_error($captcha['name']); ?>
	
	<?php }
	} ?>


			<br><?php echo form_checkbox($remember); ?>
			<?php $atributes = array('style' => 'display:inline'); echo form_label('Lembrar Senha', $remember['id'], $atributes); ?>
                        <br>
                        <br>
                            <?php echo form_submit('submit', 'Entrar', 'class="btn btn-primary"'); 
                                  
                            ?>
                        <br>
                        <br>
<?php echo form_close(); ?>
        </div>
        </div>
    </div>