<div class="span12">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Usuários</legend>
    <div class="row-fluid">
        <div class="span3">
            <a href="<?=site_url('auth/register'); ?>" class="btn btn-block btn info">Novo usuário</a>
        </div>
        <div class="span7">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Usuário</th><th>Email</th><th class="span1">Ação</th>
                    </tr>
                    <tbody>
                        <?php foreach($usuarios as $usuario): ?>
                        <tr>
                            <td><?=$usuario->username; ?></td>
                            <td><?=$usuario->email; ?></td>
                            <td><a href="<?=site_url('auth/delete/' . $usuario->id); ?>" class="btn btn-danger btn-mini">Excluir usuário</a></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </thead>
            </table>
        </div>
    </div>
</div>