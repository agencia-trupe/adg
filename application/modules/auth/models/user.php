<?php

class User extends DataMapper
{
    var $table = 'users';
    var $has_one = array("perfil");
    var $auto_populate_has_one = TRUE;
    
    function get_user($user_id)
    {
        $user = new User();
        $user->where('id', $user_id)->limit(1)->get();
        
        return $user;
    }
    
    function get_users()
    {
        $u = new User();
        $u->order_by('created', 'desc')->get();
        
        $arr = array();
        foreach($u->all as $user)
        {
            $arr[] = $user;
        }
        
        return $arr;
    }
    function ban_user($id, $reason)
    {
        $user = new User();
        $user->where('id', $id);
        $update = $user->update(array(
            'banned' => '1',
            'ban_reason' => $reason
        ));
        
        return $update;
    }
    function rehab_user($id)
    {
        $user = new User();
        $user->where('id', $id);
        $update = $user->update(array(
            'banned' => '0',
        ));
        
        return $update;
    }

    function delete_user($id)
    {
        $user = new User();
        $user->where('id', $id)->get();
        if($user->delete())
        {
            return TRUE;
        } else {
            return 'Lame!';
        }

    }
}

/* End of file file.php */
/* Location: ./application/moules/module/controllers/controller.php */