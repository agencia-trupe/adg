<div class="row">
    <legend><?=($acao == 'editar') ? 'Editar' : 'Nova'; ?> Postagem</legend>
    <?php 
        if($acao == 'editar')
            $form_address = 'painel/blog/processa';
        else
            $form_address = 'painel/blog/processa_cadastro';
    ?>
    <div class="span8">
        <?=form_open_multipart($form_address); ?>
            <?php if($acao == 'editar'): ?>
            <?=form_hidden( 'id', $object->id ); ?>
            <?php endif; ?>
            <?=form_label('Título'); ?>
            <?=form_input(array(
                'name' => 'titulo',
                'value' => set_value('titulo', ($acao == 'editar') ? $object->titulo : ''),
                'class' => 'span5'
            )); ?>
            <?=form_error('titulo'); ?>
                <br>
            <?=form_label('Texto'); ?>
            <?=form_textarea(array(
                'name' => 'texto',
                'value' => set_value('texto', ($acao == 'editar') ? $object->texto : ''),
                'class' => 'tinymce span8'
            )); ?>
            <?=form_error('texto'); ?>
            <br>
            <div class="clearfix"></div>
             <?php if($acao == 'editar' && $object->imagem):?>
             <img src="<?php echo base_url(); ?>assets/img/paginas/<?php echo $object->imagem; ?>" alt="" >
             <?php endif; ?>
             <div class="control-group">
                    <label class="control-label" for="imagem"><?=($acao == 'editar') ? 'Alterar' : 'Cadastrar'; ?> Imagem</label>
                    <div class="controls">
                      <?php echo form_upload('imagem', set_value('imagem')); ?>
                      <span class="help-inline"><?php echo form_error('imagem'); ?></span>
                    </div>
             </div>
            <!--<div class="row-fluid">
                <div class="alert alert-info span4">Criada em <?//=date( 'd/m/Y', $object->created ); ?></div>
                <div class="alert alert-success span4 ">Última atualização: 
                    <?//=(isset($object->updated)) ? date( 'd/m/Y', $object->updated ) : 'Nunca'; ?></div>
            </div>-->
            <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
            <?=anchor('paginas/admin_paginas/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
        
    </div>
    <div class="span4">
        <div class="well">
            <?=form_label('<i class="icon-calendar"></i> Data de publicação'); ?>
            <?=form_input(array(
                'name' => 'data',
                'value' => set_value('data', ($acao == 'editar') ? date('d/m/Y', $object->data) : ''),
                'class' => 'span3',
                'id'    => 'datepicker'
            )); ?>
            <?=form_error('data'); ?>
            
            <label for="ativo"><i class="icon-eye-open"></i> Status</label>
            <select name="ativo" id="status">
                <option value="1" <?php echo ('acao' == 'editar') ? ($object->ativo == TRUE) ? 'selected' : '' : ''?>>Publico</option>
                <option value="0" <?php echo ('acao' == 'editar') ? ($object->ativo == FALSE) ? 'selected' : '' : '' ?>>Privado</option>
            </select>
            
            <label class="checkbox">
                <input type="checkbox" name="destaque" value="1" <?php echo ($acao == 'editar') ? ($object->destaque == '1') ? 'checked' : '' : '' ?> /> Destaque
            </label>
            
            <?php if($acao == 'editar')
            {
                if(isset($object->tag))
                {
                    foreach($object->tag as $tag)
                    {
                        $tags[] = $tag->nome;
                    }
                    $tags = implode(', ', $tags);
                }
            }
            ?>
            <label for="myTags"><i class="icon-tags"></i> Tags</label>
            <ul id="myTags" class="span3">
                
            </ul>
            <div class="clearfix"></div>
            <div class="btn-group">
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                Adicionar Mídia
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a class="newimage" href="#">Imagem</a></li>
                <li><a class="newyoutube" href="#">Vídeo do Youtube</a></li>
              </ul>
            </div>
            <?=form_close(); ?>
            <hr>
            <div class="available-images">
                <label>Clique na miniatura para adicionar a mídia</label>

            </div>
            <hr>
            
        </div>
    </div>
</div>