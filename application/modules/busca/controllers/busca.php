<?php
/**
* File: busca.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Busca
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Busca extends ProfillingController
{
	public function index()
	{

	}

	public function query()
	{

		$this->load->library('shortcodes');

		$this->load->model('busca/busca_model', 'busca');
		$this->load->model('blog/blog_model');
		$this->load->model('calendario/calendario_model');
		$this->load->model('paginas/historia');
		$this->load->model('paginas/pagina');

		$result = $this->busca->query($this->input->post('term'));

		if(isset($result))
		{
			if(isset($result['paginas']))
			{
				foreach($result['paginas'] as $pagina)
				{
					echo '<h2>' . $pagina->titulo . '</h2>';
					echo $this->shortcodes->parse(get_excerpt($pagina->texto, 200));
					echo anchor(site_url($pagina->slug), 'Leia mais');
				}
			}
			if(isset($result['historia']))
			{

				foreach($result['historia'] as $historia)
				{
					echo '<h2>História</h2>';
					echo $this->shortcodes->parse(get_excerpt($historia->texto, 200));
					echo anchor(site_url(), 'Leia mais');
				}
			}
			if(isset($result['blog']))
			{
				foreach($result['blog'] as $blog)
				{
					echo '<h2>' . $blog->titulo . '</h2>';
					echo $this->shortcodes->parse(get_excerpt($blog->texto, 200));
					echo anchor(site_url('blog/post/' . $blog->slug), 'Leia mais');
				}
			}
			if(isset($result['eventos']))
			{
				foreach($result['eventos'] as $evento)
				{
					echo '<h2>' . $evento->titulo . '</h2>';
					echo $this->shortcodes->parse(get_excerpt($evento->descricao, 200));
					echo anchor(site_url('calendario/detalhe/' . $evento->slug), 'Leia mais');
				}
			}
		}
		else
		{
			echo 'Nenhum resultado encontrado';
		}
	}
} 