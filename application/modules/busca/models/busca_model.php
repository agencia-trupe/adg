<?php
/**
* File: blog_model.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Blog_model
 * 
 * @category ADG
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Busca_model extends DataMapperExt
{
	var $table = 'buscas';
	public function query($term)
	{
		$result = array();

		$paginas = new Pagina();
		$paginas->like('titulo', $term, 'both')
				->or_like('texto', $term, 'both')
				->get();
		$paginas_result = array();
		foreach($paginas->all as $pagina)
		{
			$paginas_result[] = $pagina;
		}
		if(count($paginas_result))
			$result['paginas'] = $paginas_result;

		$blogs = new Blog_model();
		$blogs->like('titulo', $term, 'both')
				->or_like('texto', $term, 'both')
				->get();
		$blogs_result = array();
		foreach($blogs->all as $blog)
		{
			$blogs_result[] = $blog;
		}
		if(count($blogs_result))
			$result['blog'] = $blogs_result;

		$eventos = new Calendario_model();
		$eventos->like('titulo', $term, 'both')
				->or_like('descricao', $term, 'both')
				->get();
		$eventos_result = array();
		foreach($eventos->all as $evento)
		{
			$eventos_result[] = $evento;
		}
		if(count($eventos_result))
			$result['eventos'] = $eventos_result;

		$historias = new Historia();
		$historias->like('titulo', $term, 'both')
				->or_like('conteudo', $term, 'both')
				->get();
		$historias_result = array();
		foreach($historias->all as $historia)
		{
			$historias_result[] = $historia;
		}
		if(count($historias_result))
			$result['historia'] = $historias_result;

		if( ! count($result))
			return FALSE;

		return $result;
	}
}
/* End of file blog_model.php */
/* Location: ./modules/blog/models/blog_model.php */