<?php
class Admin_contatos extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'contato';
        $this->load->model('contato/endereco');
    }
    public function index()
    {
        $this->editar();
    }

    public function editar()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['contato'] = $this->endereco->get_info();
            $this->data['conteudo'] = 'contato/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if( !$this->form_validation->run('endereco') )
            {
                $this->data['contato'] = $this->endereco->get_info();
                $this->data['conteudo'] = 'contato/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if($this->endereco->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/contato');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/contato');
                }
            }
        }
    }
}