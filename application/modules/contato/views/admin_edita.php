<div class="row">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Editar contatos</legend>
    <?=form_open('contato/admin_contatos/processa'); ?>
    <?=form_hidden( 'id', $contato->id ); ?>
    <?=form_label('Endereço'); ?>
    <?=form_input(array(
        'name' => 'endereco',
        'value' => set_value('endereco', $contato->endereco),
        'class' => 'span7'
    )); ?>
    <?=form_error('endereco'); ?>
    <?=form_label('Bairro'); ?>
    <?=form_input(array(
        'name' => 'bairro',
        'value' => set_value('bairro', $contato->bairro),
        'class' => 'span3'
    )); ?>
    <?=form_error('bairro'); ?>
    <?=form_label('Cidade'); ?>
    <?=form_input(array(
        'name' => 'cidade',
        'value' => set_value('cidade', $contato->cidade),
        'class' => 'span3'
    )); ?>
    <?=form_error('cidade'); ?>
    <?=form_label('UF'); ?>
    <?=form_input(array(
        'name' => 'uf',
        'value' => set_value('uf', $contato->uf),
        'class' => 'span1'
    )); ?>
    <?=form_error('uf'); ?>
    <?=form_label('CEP'); ?>
    <?=form_input(array(
        'name' => 'cep',
        'value' => set_value('cep', $contato->cep),
        'class' => 'span2'
    )); ?>
    <?=form_error('cep'); ?>
    <?=form_label('Email'); ?>
    <?=form_input(array(
        'name' => 'email',
        'value' => set_value('email', $contato->email),
        'class' => 'span4'
    )); ?>
    <?=form_error('email'); ?>
    <?=form_label('Telefone'); ?>
    <?=form_input(array(
        'name' => 'telefone',
        'value' => set_value('telefone', $contato->telefone),
        'class' => 'span3'
    )); ?>
    <?=form_error('telefone'); ?>
    <?=form_label('Twitter'); ?>
    <?=form_input(array(
        'name' => 'twitter',
        'value' => set_value('twitter', $contato->twitter),
        'class' => 'span4'
    )); ?>
    <?=form_error('twitter'); ?>
    <?=form_label('Facebook'); ?>
    <?=form_input(array(
        'name' => 'facebook',
        'value' => set_value('facebook', $contato->facebook),
        'class' => 'span4'
    )); ?>
    <?=form_error('facebook'); ?>
    <br>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('contatos/admin_contatos/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
</div>