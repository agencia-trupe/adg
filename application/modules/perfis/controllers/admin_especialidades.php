<?php
/**
* File: admin_especialidades.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_especialidades
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Admin_especialidades extends CrudController
{
	/**
	 * Dados retornados para a view.
	 * 
	 * @var array
	 */
	var $data;
	/**
	 * Critério de ordenação da lista do CRUD.
	 *
	 *  @var boolean
	 */
	var $list;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct($model_path = 'perfis/especialidade', $module = 'perfis', $path = 'especialidades');
		$this->list = FALSE;
		$this->data['painel_module'] = 'perfil';
		$this->load->model('perfis/perfil');
		$this->load->model('perfis/especialidade');
	}

	public function processa()
    {
        if( ! $this->form_validation->run($this->model))
        {
            $this->data['acao'] = 'editar';
            $this->data['object'] = $this->especialidade->fetch_object('id', $this->input->post( 'id' ));
            $this->data['conteudo'] = 'perfis' . '/admin/' . 'especalidade' . '/edita';
            $this->load->view('start/template', $this->data);
        }
        $post = array();

       
        //Itera sobre os dados do post e atribui suas chaves e valores para
        //o array $post
        foreach($_POST as $key => $value)
        {
            $post[$key] = $value;
        }
        
        
        if($this->especialidade->change($post))
        {
            $this->session->set_flashdata('success', 'Registro alterado com sucesso');
            redirect('painel/' . 'especialidades');
        }
        else
        {
            $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                Tente novamente ou entre em contato com o suporte');
            redirect('painel/' . 'especialidades' . '/edita/' . $post['id']);
        }
    }

    public function processa_cadastro()
    {
        $model = $this->model;
        if(!$this->form_validation->run($this->model))
        {

            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'perfis' . '/admin/'. 'especialidade' .'/edita';
            $this->load->view('start/template', $this->data);
        }
        else
        {

            $post = array();

            //Itera sobre os dados do post e atribui suas chaves e valores para
            //o array $post
            foreach($_POST as $key => $value)
            {
                $post[$key] = $value;
            }

            if($this->especialidade->insert($post))
            {
                $this->session->set_flashdata('success', 'Registro incluido com sucesso');
                redirect('painel/' . 'especialidades');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível incluir o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/' . 'especialidades');
            }
        }
    }

    public function serve_json()
    {
    	$autocompletiondata = $this->especialidade->get_json();


    	/*$autocompletiondata = array(
		    3 => 'Hazel Grouse',
		    4 => 'Common Quail',
		    5 => 'Common Pheasant',
		    6 => 'Northern Shoveler',
		    7 => 'Greylag Goose',
		    8 => 'Barnacle Goose',
		    9 => 'Lesser Spotted Woodpecker',
		    10 => 'Eurasian Pygmy-Owl',
		    11 => 'Dunlin',
		    13 => 'Black Scoter',
		    14 => 'Eurasian Wryneck',
		    15 => 'Little Owl',
		    16 => 'Eurasian Curlew',
		    17 => 'Ruff',
		    18 => 'Little Tern',
		    19 => 'Merlin',
		    20 => 'Bluethroat',
		    21 => 'Redwing',
		    22 => 'Wood Nuthatch',
		    23 => 'Firecrest',
		    24 => 'Goldcrest',
		    25 => 'Bearded Parrotbill',
		    26 => 'Chaffinch',
		    27 => 'Brambling',
		    28 => 'Hawfinch',
		    29 => 'Goldcrest',
		); */

		$term = $this->input->get('term');

		if(isset($term)) {
		    $result = array();
		    foreach($autocompletiondata as $key => $value) {
		        if(strlen($term) == 0 || strpos(strtolower($value), strtolower($term)) !== false) {
		            $result[] = '{"id":"'.$key.'","label":"'.$value.'","value":"'.$value.'"}';
		        }
		    }
		    
		    echo "[".implode(',', $result)."]";
		}
    }
}
/* End of file perfis.php */
/* Location: ./modules/perfil/controllers/perfis.php */