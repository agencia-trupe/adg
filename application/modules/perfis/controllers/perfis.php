<?php
/**
* File: perfis.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Perfis
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Perfis extends ProfillingController
{
	private $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('perfis/perfil');
		$this->load->model('perfis/foto');
		$this->data['pagina'] = 'designers';
		$this->data['categoria_mae'] = 'designers';
	}

	public function index()
	{
		$this->lista();
	}

	public function lista()
	{      
		$this->load->library('pagination');
		$pagination_config = array(
					'base_url'       => base_url() . 'designers/lista',
					'total_rows'     => $this->db->get('designers')->num_rows(),
					'per_page'       => 3,
					'num_links'      => 5,
					'next_link'      => '&gt',
					'prev_link'      => '&lt',
					'first_link'     => FALSE,
					'last_link'      => FALSE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		$this->pagination->initialize($pagination_config);

		//Obtendo resultados no banco
		$this->data['result'] = $this
								->perfil
								->fetch_limit($pagination_config['per_page'], 
									$this->uri->segment(3));

		//Biblioteca de SEO
		$seo_tags = array(
			'title' => 'Encontre um designer',
			'description' => 'Encontre um designer associado à ADG'
			);
		$this->load->library('seo', $seo_tags);
		
		//Definição de parcial de conteúdo e retorno da view.
		$this->data['conteudo'] = 'perfis/lista';
		$this->load->view('layout/template', $this->data);
	}

	public function detalhe($id)
	{
		$designer = $this->perfil->fetch_object('id', $id);
		//Biblioteca de SEO
		$seo_tags = array(
			'title' => 'Designer - ' . $designer->nome,
			'description' => 'Encontre um designer associado à ADG'
			);
		$this->load->library('seo', $seo_tags);
		
		$this->data['designer'] = $designer;
		//Definição de parcial de conteúdo e retorno da view.
		$this->data['conteudo'] = 'perfis/detalhe';
		$this->load->view('layout/template', $this->data);
	}

	public function busca()
	{
		$criterio = $this->input->post('critero');
		if($criterio === 'especialidade')
		{
			$this->data['result'] = $this->especialidade->get_like('nome', $valor);
		} else 
		{
			$this->data['result'] = $this->perfil->fetch_by_key(null, 'criterio', $valor);
		}

		$seo_tags = array(
			'title' => 'Designers - Busca - ' . ucfirst($criterio),
			'description' => 'Encontre um designer associado à ADG'
			);
		$this->load->library('seo', $seo_tags);
		
		//Definição de parcial de conteúdo e retorno da view.
		$this->data['conteudo'] = 'perfis/detalhe';
		$this->load->view('layout/template', $this->data);
	}

	public function destaque_home()
	{
		$data['designer'] = $this->perfil->get_destaque();
		$data['foto'] = $this->foto->get_random_perfil($data['designer']->id);
		$this->load->view('perfis/destaque_home', $data);
	}
	
}
/* End of file perfis.php */
/* Location: ./modules/perfil/controllers/perfis.php */