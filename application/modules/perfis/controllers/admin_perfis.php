<?php
/**
* File: Admin_perfis.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_perfis
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/

class Admin_perfis extends CrudController
{
	/**
	 * Dados retornados para a view.
	 * 
	 * @var array
	 */
	var $data;
	/**
	 * Critério de ordenação da lista do CRUD.
	 *
	 *  @var boolean
	 */
	var $list;

	/**
	 * Id do usuário logado atualmente.
	 * 
	 * @var int
	 */
	var $user_id;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct($model_path = 'perfis/perfil', $module = 'perfis');
		$this->list = FALSE;
		$this->data['painel_module'] = 'perfil';
		$this->user_id = $this->tank_auth->get_user_id();
		$this->load->model('perfis/perfil');
        $this->load->model('perfis/foto');
	}

	public function editar()
    {
    	$this->data['object'] = $this->perfil
    								 ->fetch_object('user_id', $this->user_id);
        $this->data['fotos'] = $this->foto->get_perfil($this->data['object']->id);
        $this->data['acao'] = 'editar';
       	$this->data['module'] = 'perfil';
       	$this->data['painel_module'] = 'perfil';
        $this->data['conteudo'] = 'perfis/admin/perfil/edita';
        $this->load->view('start/template', $this->data);
    }

    public function processa()
    {
        if( ! $this->form_validation->run('perfil') )
        {
        	$this->data['object'] = $this->perfil
    								 ->fetch_object('user_id', $this->user_id);
	        $this->data['acao'] = 'editar';
	       	$this->data['module'] = 'perfil';
	       	$this->data['painel_module'] = 'perfil';
	        $this->data['conteudo'] = 'perfis/admin/perfil/edita';
	        $this->load->view('start/template', $this->data);
        }
        else
        {
        	 $post = array();

       
	        //Itera sobre os dados do post e atribui suas chaves e valores para
	        //o array $post
	        foreach($_POST as $key => $value)
	        {
	            $post[$key] = $value;
	        }

	        foreach($post['especialidades'] as $key => $value)
	        {
	        	$especialidades[] = $this->_get_especialidade_id($key);
	        }

	        $post['especialidades'] = $especialidades;

            //Verifica se foi feita a postagem de um arquivo de imagem
            //e faz o upload caso verdadeiro
            if(sizeof($_FILES) && strlen($_FILES["imagem"]["name"])>0)
            {
                //Configurações para o upload
                $config['upload_path'] = './assets/img/perfil/avatar';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2000';
                $config['max_width']  = '1600';
                $config['max_height']  = '1200';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('imagem'))
                {
                    echo $this->upload->display_errors(); exit;
                    //Caso a tentativa de upload não seja bem sucedida, retorna
                    //para a página de edição, exibindo o erro
                    $this->data['error'] = array('error' => $this->upload->display_errors());
                    $this->data['acao'] = 'editar';
                    $this->data['object'] = $this->perfil->fetch_object('user_id', $this->user_id );
                    $this->data['conteudo'] = 'perfis/admin/perfil/edita';
                    $this->load->view('start/template', $this->data);
                }
                else
                {

                    $this->load->library('image_moo');
                    //Resposta do upload
                    $upload_data = $this->upload->data();

                    //Caminho para o arquivo atual
                    $file_uploaded = $upload_data['full_path'];
                    //Caminho para o arquivo alterado
                    $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                    $thumbnail = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

                    if(
                        $this->image_moo->load($file_uploaded)
                            ->resize(600,600,$pad=FALSE)
                            ->save($new_file,true)
                        &&
                        $this->image_moo->load($file_uploaded)
                            ->resize_crop(220,140)
                            ->save($thumbnail,true)
                        )
                    {
                        $post['imagem'] = $upload_data['file_name'];
                    }
                    else
                    {
                        //Caso a tentativa de alteração da imagem não seja 
                        //bem sucedida, retorna para a página de edição, 
                        //exibindo o erro
                        $this->data['error'] = 'Erro de upload';

                       $this->data['acao'] = 'editar';
                        $this->data['object'] = $this->perfil->fetch_object('user_id', $this->user_id );
                        $this->data['conteudo'] = 'perfis/admin/perfil/edita';
                        $this->load->view('start/template', $this->data);
                    }
                }
            }

	        if($this->perfil->change($post))
	        {
	            $this->session->set_flashdata('success', 'Registro alterado com sucesso');
	            redirect('painel/' . $this->data['painel_module'] . '/editar/');
	        }
	        else
	        {
	            $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
	                Tente novamente ou entre em contato com o suporte');
	            redirect('painel/' . $this->data['painel_module'] . '/editar/');
	        }
        }
    }

    private function _get_especialidade_id($especialidade)
    {
    	explode('-', preg_replace('/\[/', '', $especialidade));
    	return $especialidade[0];
    }

    public function adiciona_foto()
    { 
        $this->load->library('form_validation');

        $status = "";
        $msg = "";
        $file_element_name = 'projeto-foto-upload';
   
        $config['upload_path'] = './assets/img/perfil';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']  = 1024 * 8;
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_element_name))
        {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
            $id = '';
            $nome = '';
        }
        else
        {
            $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
            $upload_data = $this->upload->data();
            $file_uploaded = $upload_data['full_path'];
            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
            $thumb = $upload_data['file_path'] . 'thumbs/' . $upload_data['file_name'];
            $destaque = $upload_data['file_path'] . 'destaques/' . $upload_data['file_name'];

            if(
                $this->image_moo->load($file_uploaded)
                                ->resize(1200,1200)
                                ->save($new_file,true)
                &&
                $this->image_moo->load($new_file)
                                ->resize(570,570,true)
                                ->save($thumb, true)
                &&
                $this->image_moo->load($new_file)
                                ->resize_crop(379, 243)
                                ->save($destaque)
              )
            {
                $imagem = $upload_data['file_name'];
                    $data = array(
                    'imagem' => $imagem,
                    'perfil_id' => $this->input->post('perfil_id'),
                    'titulo' => 'Teste'
                    );
                $adiciona = $this->foto->insert($data);
                if($adiciona)
                {
                    $status = "success";
                    $msg = "Foto adicionada com sucesso.";
                    $id = $adiciona;
                    $nome = $data['imagem'];
                }
                else
                {
                    unlink($data['file_data']['full_path']);
                    $status = "error";
                    $msg = "Houve um erro ao processar sua requisição. Por vavor, tente novamente, ou entre em contato com o suporte.";
                    $id = '';
                    $nome = '';
                }
            }
            else
            {
                $status = 'error';
                $msg = $this->image_moo->display_errors();
                $id = '';
                $nome = '';
            }
        }   
        @unlink($_FILES[$file_element_name]);
        echo json_encode(array('status' => $status, 'msg' => $msg, 'id' => $id, 'nome' => $nome));
    }

    public function deleta_foto()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($this->input->post('ajax') == 1)
            {
                $id = $this->input->post('foto_id');
                $foto = $this->foto->get_conteudo($id);
                $deleta = $this->foto->deleta_foto($id);
                $unlink = unlink('assets/img/perfil/' . $foto->imagem);
                $unlink_thumbs = unlink('assets/img/perfil/thumbs/' . $foto->imagem);
                if($deleta && $unlink && $unlink_thumbs)
                {
                    $response['status'] = 'success';
                    $response['msg'] = 'Foto apagada com sucesso.';
                }
                else
                {
                    $response['status'] = 'error';
                    $response['msg'] = 'Erro ao apagar, por favor, 
                    tente novamente';
                }
                echo json_encode($response);
            }
        }
    }

    public function sort_fotos()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Concept - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('foto');
            if ($itens)
            {
                $ordenar = $this->foto->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }
}
/* End of file perfis.php */
/* Location: ./modules/perfil/controllers/perfis.php */