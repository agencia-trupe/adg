<?php
/**
* File: perfil.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Perfil
 * 
 * @category ADG
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/
 * @todo  	 [1] Mover o $relation_save da função change($dados) para uma 
 *           função privada
 **/ 
class Perfil extends DataMapperExt
{
	/**
	 * Nome da tabela.
	 * 
	 * @var string
	 */
	var $table = 'designers';
	var $has_one = array('user');
	var $has_many = array('especialidade', 'foto');
	var $auto_populate_has_many = TRUE;

	public function insert($user_id)
	{
		$designer = new Perfil();
		$designer->user_id = $user_id;
		if ( $designer->save() ) return TRUE;
		return FALSE; 
	}

	public function change($dados)
	{
		$especialidades = $dados['especialidades'];

		unset($dados['especialidades']);

		$this->where('id', $dados['id']);
        
        $update_data = array();
        
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }

        

        $save_relations = $this->_gerencia_especialidades($especialidades, $dados['id']);
        
        //echo 'Pausa 6 <br>';
        //var_dump($save_relations); exit;
        
        //echo 'Pausa 7 <br>';
        //var_dump($this->update($update_data)); exit;

        if( ! $save_relations || ! $this->update($update_data) ) return FALSE;

        return TRUE;
	}

	public function get_destaque()
	{
		$perfil = new Perfil();
		$perfil->order_by('nome', 'RAND')->get(1);
		return $perfil;
	}

	private function _save_many($key, $value, $data)
	{
		$this->where($key, $value)->get();

	}

	private function _gerencia_especialidades($especialidades, $id)
	{

		//Obtem o objeto perfil
		$perfil = new Perfil();
		$perfil->where('id', $id)->get();

		//Obtem o array com as atuais especialidades
		$atuais = array();

		foreach($perfil->especialidade as $especialidade)
		{
			$atuais[] = $especialidade->id;
		}

		//var_dump($atuais);

		$remover = array_diff($atuais, $especialidades);

		//print_r($remover);
		
		$adicionar = array_diff($especialidades, $atuais);

		//print_r($adicionar);


		if(count($remover))
		{
			$removidos = $this->_remove_especialidades($remover, $id);
			if(count($remover) !== count($removidos)) return FALSE;
		}
		
		if(count($adicionar))
		{	
			$adicionados = $this->_adiciona_especialidades($adicionar, $id);
			if(count($adicionar) !== count($adicionados)) return FALSE;
		}
			

		return TRUE;
		
	}

	private function _adiciona_especialidades($adicionar, $id)
	{
		foreach ($adicionar as $especialidade)
		{
			$perfil = new Perfil();

			$perfil->where('id', $id)->get();
			
			$e = new Especialidade();
			$e->where('id', $especialidade)->get();
			
			if($perfil->save($e))
				$result[] = $especialidade;
		}

		if(count($result) !== count($adicionar)) return FALSE;

		return $result;
	}

	private function _remove_especialidades($remover, $id)
	{
		$result = array();

		foreach ($remover as $especialidade)
		{
			$perfil = new Perfil();

			$perfil->where('id', $id)->get();
			
			$e = new Especialidade();
			$e->where('id', $especialidade)->get();

			if($perfil->delete($e))
				$result[] = $especialidade;
		}


		if(count($result) !== count($remover)) return FALSE;

		//var_dump($result); exit;

		return $result;
	}

}
/* End of file pagina.php */
/* Location: ./modules/paginas/models/pagina.php */