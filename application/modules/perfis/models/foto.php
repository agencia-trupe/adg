<?php
class Foto extends Datamapper
{
    var $table = 'fotos';
    var $has_one = array('perfil');

    function get_perfil($perfil_id)
    {
        $foto = new Foto();
        $foto->where('perfil_id', $perfil_id);
        $foto->order_by('ordem', 'asc');
        $foto->get();
        $arr = array();
        foreach( $foto->all as $f )
        {
            $arr[] = $f;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return FALSE;
    }

    function get_random_perfil($perfil_id)
    {
        $foto = new Foto();
        $foto->where('perfil_id', $perfil_id)
             ->order_by('id', 'RAND')->get(1);
        return $foto;
    }

    function get_conteudo( $id )
    {
        $foto = new Foto();
        $foto->where( 'id', $id )->get();
        if( $foto->exists() ){
            return $foto;
        }
        return NULL;
    }

    function get_related( $id, $position )
    {
        $foto = new Foto();
        switch ( $position ) {
            case 'prev':
                $foto->where( 'id <', $id);
                break;
            case 'next':
                $foto->where( 'id >', $id);
                break;
        }
        $foto->get(1);
        if($foto->exists())
        {
            return $foto->id;
        }
        return FALSE;
    }
    /**
     * Temporario para desenvolvimento
     * @param  [type] $dados [description]
     * @return [type]        [description]
     */
    function insert($dados)
    {
        $foto = new Foto();
        foreach ($dados as $key => $value)
        {
            $foto->$key = $value;
        }
        $foto->created = time();
        $insert = $foto->save();
        if($insert)
        {
            return $foto->id;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $foto = new Foto();
            $foto->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($foto->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function deleta_foto($foto_id)
    {
        $foto = new Foto();
        $foto->where('id', $foto_id)->get();
        if($foto->delete())
        {
            return TRUE;
        }
        return FALSE;
    }
}