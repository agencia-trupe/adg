<?php
/**
* File: especialidade.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Especialidade
 * 
 * @category ADG
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Especialidade extends DataMapperExt
{
	/**
	 * Nome da tabela.
	 * 
	 * @var string
	 */
	var $table = 'especialidades';
	
	/**
	 * [$has_many description]
	 * @var array
	 */
	var $has_many = array('perfil');


	public function get_json()
	{
		$e = new Especialidade();
		$e->order_by('nome', 'ASC')->get();

		$result = array();

		foreach($e->all as $especialidade)
		{
			$result[$especialidade->id] = $especialidade->nome;
		}

		if( ! sizeof($result) ) return FALSE;

		return $result;
	}

}
/* End of file pagina.php */
/* Location: ./modules/paginas/models/pagina.php */