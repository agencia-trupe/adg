<div class="row">
         <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Especialidade</legend>
    <?php if( $acao !== 'editar' )
    {
        $form = 'painel/especialidades/processa_cadastro';
    }
    else
    {
        $form = 'painel/especialidades/processa';
    }
    ?>
    <?php echo form_open($form);
        if($acao === 'editar')
        echo form_hidden( 'id', $object->id );?>

    <?php echo form_label('Nome');
        echo form_input(array(
        'name' => 'nome',
        'value' => set_value('nome', ($acao === 'editar') ? $object->nome : ''),
        'class' => 'span5'
        ));
        echo form_error('nome');
    ?>
    <br>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/especialidades/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
</div>