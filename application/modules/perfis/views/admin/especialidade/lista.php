<div class="row">
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Especialidades <a href="<?=site_url('painel/especialidades/cadastrar'); ?>"
                       class="btn btn-mini btn-info">Cadastrar</a></legend>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nome</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($result as $perfil): ?>
                <tr>
                    <td><?=$perfil->nome; ?></td>
                    <td>
                        <?=anchor('painel/especialidades/editar/' . $perfil->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?=anchor('painel/especialidades/apagar/' . $perfil->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>