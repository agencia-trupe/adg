<div class="row">
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Perfis <a href="<?=site_url('painel/perfil/cadastrar'); ?>"
                       class="btn btn-mini btn-info">Cadastrar</a></legend>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($result as $perfil): ?>
                <tr>
                    <td><?=$perfil->titulo; ?></td>
                    <td><?=anchor('perfils/perfil/editar/' . $perfil->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?></a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>