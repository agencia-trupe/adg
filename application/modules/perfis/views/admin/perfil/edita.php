<div class="row">
    <?php if($this->session->flashdata('error') != NULL): ?>
        <div class="alert alert-error">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <div class="alert alert-success">
            <?php echo $error['error']; ?>
        </div>
    <?php endif; ?>
    <legend>Editar Perfil</legend>
    <?php echo form_open_multipart('painel/perfil/processa'); ?>
    <input name="id" type="hidden" value="<?php echo $object->id ?>" class="id">
    <?php echo form_label('Nome');
        echo form_input(array(
        'name' => 'nome',
        'value' => set_value('nome', ($acao == 'editar') ? $object->nome : ''),
        'class' => 'span5'
        ));
        echo form_error('nome');
    ?>
    <?php echo form_label('Especialidades');
        echo form_input(array(
        'name' => 'especialidades[]',
        'class' => 'span5 tag'
        ));
        echo form_error('especialidades');
    ?>
    <?php foreach($object->especialidade as $especialidade): ?>
    <?php echo form_input(array(
        'name' =>  'especialidades['. $especialidade->id . '-a]',
        'value' => $especialidade->nome,
        'class' => 'tag'
        )) ?>
    <?php endforeach; ?>
    <?php echo form_label('Cidade');
        echo form_input(array(
        'name' => 'cidade',
        'value' => set_value('cidade', ($acao == 'editar') ? $object->cidade : ''),
        'class' => 'span5'
        ));
        echo form_error('cidade');
    ?>
    <?php echo form_label('UF');
        echo form_input(array(
        'name' => 'uf',
        'value' => set_value('uf', ($acao == 'editar') ? $object->uf : ''),
        'class' => 'span5'
        ));
        echo form_error('uf');
    ?>
    <?php echo form_label('Website');
        echo form_input(array(
        'name' => 'website',
        'value' => set_value('website', ($acao == 'editar') ? $object->website : ''),
        'class' => 'span5'
        ));
        echo form_error('website');
    ?>
    <?php echo form_label('Twitter');
        echo form_input(array(
        'name' => 'twitter',
        'value' => set_value('twitter', ($acao == 'editar') ? $object->twitter : ''),
        'class' => 'span5'
        ));
        echo form_error('twitter');
    ?>
    <?php echo form_label('Facebook');
        echo form_input(array(
        'name' => 'facebook',
        'value' => set_value('facebook', ($acao == 'editar') ? $object->facebook : ''),
        'class' => 'span5'
        ));
        echo form_error('facebook');
    ?>
    <br>
    <!--Foto do perfil-->
    <?php if($acao == 'editar' && $object->imagem):?>
        <img src="<?php echo base_url(); ?>assets/img/perfil/avatar/thumbs/<?php echo $object->imagem; ?>" alt="" >
    <?php endif; ?>
    <!--fim de foto atual-->
    <div class="control-group">
        <label class="control-label" for="imagem"><?=($acao == 'editar') ? 'Alterar' : 'Cadastrar'; ?> foto do perfil</label>
        <div class="controls">
            <?php echo form_upload('imagem', set_value('imagem')); ?>
            <span class="help-inline"><?php echo form_error('imagem'); ?></span>
        </div>
    </div>
    <!--fim de adicionar foto-->
    <!--Fim de foto do perfil-->
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/perfil/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <legend>Portifólio 
        
        <a href="#" class="btn btn-mini btn-info btn-reordenar">
            <i class="icon-retweet icon-white"></i> Reordenar
        </a>
        <a href="#" class="btn btn-mini btn-info btn-adicionar-foto">
            <i class="icon-picture icon-white"></i> Adicionar Foto
        </a>
    </legend>
    <div class="row">
        <div class="form-adicionar-foto well span4 invisible">
            <?=form_open('','id="projetos-upload"'); ?>
            <?=form_upload('projeto-foto-upload', '', 'id="projeto-foto-upload"'); ?>
            <input type="submit" class="btn btn-mini btn-success btn-adicionar-foto-upload"
            value="Fazer upload" >  
            <a href="#" class="btn btn-mini btn-danger btn-adicionar-foto-cancela">
            <i class="icon-remove-sign icon-white"></i> Cancelar</a>
        </div>
    </div> 
    <div class="fotos-lista">
        <ul id="projeto-images" class="ui-sortable" style="list-style-type:none; padding:0">
        <?php if(isset($fotos)): ?>
        <?php foreach ($fotos as $foto): ?>
            <li class="projeto-foto" id="foto_<?=$foto->id; ?>">
                <img width="121" height="121" style="margin-bottom:10px;" src="<?=base_url('assets/img/perfil/thumbs/' . $foto->imagem); ?>" alt="">
                <a href="#" data-id="<?=$foto->id; ?>" class="btn btn-delete btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
            </li>
        <?php endforeach; ?>
        <?php endif; ?>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>