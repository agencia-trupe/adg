<div class="destaque-home">
	<header>
		<h1>Designer em destaque</h1>
		<a href="<?php echo site_url('designers') ?>" class="more">veja todos</a>
	</header>
	<a href="<?php echo site_url('designers/detalhe/' . $designer->id ) ?>" class="destaque-wrapper">
		<img src="<?php echo base_url('assets/img/perfil/destaques/' . $foto->imagem) ?>" 
			 alt="<?php echo $designer->nome ?>">
		<span class="designers-destaque-nome">
			<?php echo $designer->nome ?>	
		</span>
	</a>
</div>