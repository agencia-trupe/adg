<div class="conteudo perfil detalhe">
	<h1 class="titulo">/<?php echo $designer->nome ?></h1>
	<div class="designer-meta">
		<img width="160" height="102" src="<?php echo base_url('assets/img/perfil/avatar/thumbs/' . $designer->imagem) ?>" alt="ADG - Designers - <?php echo $designer->nome ?>">
		<div class="local"><?php echo $designer->cidade ?>, <?php echo $designer->uf ?></div>
		<div class="separador"></div>
		<div class="especialidades">
			<h2>Especialidades</h2>
			<?php foreach ($designer->especialidade as $especialidade): ?>
				<a href="<?php echo site_url('designers/especialidade/' . $especialidade->slug) ?>">
					<?php echo $especialidade->nome ?>
				</a>
			<?php endforeach ?>
		</div>
		<div class="separador"></div>
		<div class="website">
			<h2>Website</h2>
			<a href="<?php echo $designer->website ?>"><?php echo str_replace('http://', '', $designer->website) ?></a>
		</div>
		<div class="separador"></div>
		<div class="outros-links">
			<h2>Outros Links</h2>
			<a href="<?php echo $designer->twitter ?>"><?php echo str_replace('http://', '', $designer->twitter) ?></a>
			<a href="<?php echo $designer->facebook ?>"><?php echo str_replace('http://', '', $designer->facebook) ?></a>
		</div>
	</div>
	<div class="designer-portifolio">
		<?php foreach ($designer->foto as $foto): ?>
			<img src="<?php echo base_url('assets/img/perfil/' . $foto->imagem) ?>" alt="">
		<?php endforeach ?>
		<div class="clearfix"></div>
	</div>
</div>
<div class="clearfix"></div>