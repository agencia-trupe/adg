<div class="conteudo perfil lista">
	<h1 class="titulo">/Destaque</h1>
	<?php if(isset($destaque)): ?>
		<p>Destaque</p>
	<?php endif ?>
	
	<div class="separador"></div>

	<?php if ( isset( $result ) ): ?>
		<?php foreach($result as $designer): ?>
			<div class="perfil-wrapper">
				<a 	class="perfil-link" 
					href="<?php echo site_url('designers/detalhe/' . $designer->id) ?>">
					<img src="<?php echo base_url('assets/img/perfil/avatar/thumbs/' . $designer->imagem) ?>" 
						 alt="ADG - Designers - <?php echo $designer->nome ?>">
					<span class="nome"><?php echo $designer->nome ?></span>
				</a>
				<div class="especialidades-wrapper">
					<?php foreach ($designer->especialidade as $especialidade): ?>
						<a href="<?php echo site_url('designers/especialidade/' . $especialidade->slug) ?>"><?php echo $especialidade->nome ?></a>
					<?php endforeach ?>
				</div>
			</div>
		<?php endforeach ?>
		<div class="clearfix"></div>
		<?php echo $this->pagination->create_links() ?>
	<?php endif ?>
</div>