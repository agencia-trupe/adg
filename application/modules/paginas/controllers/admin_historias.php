<?php
/**
* File: admin_categorias.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_paginas
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_historias extends CrudController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	public $data;

	/**
	 * Critério de ordenação da lista do CRUD.
	 * array('campo', 'criterio');
	 *
	 *  @var mixed
	 */
	public $list;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct($model_path = 'paginas/historia', $module = 'paginas');
		$this->data['painel_module'] = 'historia';
		$this->list = array('ano', 'ASC');
	}
}

/* End of file admin_categorias.php */
/* Location: ./modules/paginas/controllers/admin_categorias.php */