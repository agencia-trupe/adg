<?php
/**
* File: paginas.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Paginas
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Paginas extends ProfillingController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	public  $data;

	/**
	 * [__construct construtor]
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('paginas/pagina');
		$this->load->model('paginas/categoria');
	}
	/**
	 * Carrega a ação principal
	 * 
	 * @return object um objeto view
	 */
	public function index()
	{
		$this->view();
	}
	/**
	 * Exibe uma página baseando-se em um slug
	 * 
	 * @param string $slug string da página a ser exibida.
	 * 
	 * @return void
	 */
	public function view($slug)
	{
		//busca os dados de uma página cujo slug foi passado como parâmetro
		$this->data['dados_pagina'] = $this->pagina->fetch_object('slug', $slug);
		

		//Carrega a biblioteca de SEO e a inicializa.
		$seo_tags = array(
			'title' => $this->data['dados_pagina']->titulo,
			'description' => $this->data['dados_pagina']->description,
			);
		$this->load->library('seo', $seo_tags);
		//Define a variável de página para o menu
		$this->data['pagina'] = $slug;
		//Define a view utilizada | Template dinâmica ou fixa.
		if( ! empty($this->data['dados_pagina']->template))
		{
			$this->data['conteudo'] = 'paginas/templates/' . $this->data['dados_pagina']->template;
		}
		else
		{
			$this->data['conteudo'] = 'paginas/templates/index';
		}
		//Obtem o slug da categoria mãe para o carregamento da view;
		$categoria_mae = $this->data['dados_pagina']->categoria_mae_id;
		$this->data['categoria_mae'] = $this->_get_categoria_slug($categoria_mae);
		$this->data['sidebar'] = $this->pagina->get_sidebar($categoria_mae);
		$this->load->view('layout/template', $this->data);
	}
	/**
	 * Obtem o slug de uma categoria baseando-se em seu id
	 * 
	 * @param integer $categoria_id id da categoria.
	 * 
	 * @return string                slug da categoria
	 */
	private function _get_categoria_slug($categoria_id)
	{
		return $this->categoria->getSlug($categoria_id);
	}
}
/* End of file paginas.php */
/* Location: ./modules/paginas/controllers/paginas.php */