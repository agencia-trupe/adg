<?php
/**
* File: paginas.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Paginas
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Historias extends ProfillingController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	public  $data;

	/**
	 * [__construct construtor]
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('paginas/pagina');
		$this->load->model('paginas/categoria');
		$this->load->model('paginas/historia');
		$this->load->library('shortcodes');
	}
	/**
	 * Carrega a ação principal
	 * 
	 * @return object um objeto view
	 */
	public function index()
	{
		$this->view();
	}
	/**
	 * Exibe uma página baseando-se em um slug
	 * 
	 * @param string $slug string da página a ser exibida.
	 * 
	 * @return void
	 */
	public function view()
	{
		//busca os dados de uma página cujo slug foi passado como parâmetro
		$this->data['historias'] = $this->historia->get_anos();
		

		//Carrega a biblioteca de SEO e a inicializa.
		$seo_tags = array(
			'title' => 'História',
			'description' => 'Conheça a história da ADG - Associação de Designers
								Gráficos do Brasil.'
			);

		$this->load->library('seo', $seo_tags);
		//Define a variável de página para o menu
		$this->data['pagina'] = 'historia';
		//Define a view utilizada | Template dinâmica ou fixa.
		$this->data['conteudo'] = 'paginas/templates/historia';

		//Obtem o slug da categoria mãe para o carregamento da view;
		$this->data['categoria_mae'] = 'institucional';
		$this->data['sidebar'] = $this->pagina->get_sidebar(1);
		$this->load->view('layout/template', $this->data);
	}
}
/* End of file paginas.php */
/* Location: ./modules/paginas/controllers/paginas.php */