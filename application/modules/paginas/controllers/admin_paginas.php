<?php
/**
* File: admin_paginas.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_paginas
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_paginas extends CrudController
{
	/**
	 * Dados retornados para a view.
	 * 
	 * @var array
	 */
	var $data;
	/**
	 * Critério de ordenação da lista do CRUD.
	 *
	 *  @var boolean
	 */
	var $list;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct($model_path = 'paginas/pagina', $module = 'paginas');
		$this->list = FALSE;
		$this->data['painel_module'] = 'paginas';
	}
}

/* End of file admin_paginas.php */
/* Location: ./modules/paginas/controllers/admin_paginas.php */