<?php
/**
* File: admin_categorias.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_paginas
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_categorias extends CrudController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	public $data;

	/**
	 * Critério de ordenação da lista do CRUD.
	 *
	 *  @var boolean
	 */
	public $list;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct($model_path = 'paginas/categoria', $module = 'paginas');
		$this->list = FALSE;
	}
}

/* End of file admin_categorias.php */
/* Location: ./modules/paginas/controllers/admin_categorias.php */