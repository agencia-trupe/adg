<?php
/**
* File: categoria.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_paginas
 * 
 * @category ADG
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Historia extends DataMapperExt
{
	/**
	 * Tabela referente ao modelo.
	 * 
	 * @var string
	 */
	var $table = 'historia';

	/**
	 * Obtem a categoria tendo como parâmetro a categoria mãe.
	 * 
	 * @param integer $categoria_mae id da categoria mãe
	 * 
	 * @return array array com categorias filhas
	 */
	public function fetch_by_parent($categoria_mae)
	{
		$this->where('categoria_mae', $categoria_mae)->get();
		$result_array = array();
		foreach ($this->all as $categoria)
		{
			$result_array[] = $categoria;
		}
		if(sizeof($result_array))
		{
			return $result_array;
		}
		return FALSE;
	}

	public function get_anos()
	{
		$this->order_by('ano', 'ASC')->get();
		foreach($this->all as $historia)
		{
			$result[] = $historia;
		}
		return $result;
	}
}

/* End of file categoria.php */
/* Location: ./modules/paginas/models/categoria.php */