<?php
/**
* File: pagina.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Pagina
 * 
 * @category ADG
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Pagina extends DataMapperExt
{
	/**
	 * Nome da tabela.
	 * 
	 * @var string
	 */
	var $table = 'paginas';
}
/* End of file pagina.php */
/* Location: ./modules/paginas/models/pagina.php */