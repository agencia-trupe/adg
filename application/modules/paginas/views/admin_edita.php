<div class="row">
    <legend>Editar página</legend>
    <?=form_open_multipart('paginas/admin_paginas/processa'); ?>
    <?=form_hidden( 'id', $pagina->id ); ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', $pagina->titulo),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo'); ?>
        <br>
    <?=form_label('Texto'); ?>
    <?=form_textarea(array(
        'name' => 'texto',
        'value' => set_value('texto', $pagina->texto),
        'class' => 'tinymce span8'
    )); ?>
    <?=form_error('texto'); ?>
    <br>
    <!--<select>
        <?php //foreach( $templates as $template ): ?>
        <option value="<?//=str_replace('.php', '', $template); ?>">
            <?//=ucfirst(str_replace('.php', '', $template)); ?>
        </option>
        <?php //endforeach; ?>
    </select>-->
    <br>
    <div class="clearfix"></div>
    <br>
     <?php if($pagina->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/paginas/<?php echo $pagina->imagem; ?>" alt="" >
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem">Alterar Imagem</label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
    <!--<div class="row-fluid">
        <div class="alert alert-info span4">Criada em <?//=date( 'd/m/Y', $pagina->created ); ?></div>
        <div class="alert alert-success span4 ">Última atualização: 
            <?//=(isset($pagina->updated)) ? date( 'd/m/Y', $pagina->updated ) : 'Nunca'; ?></div>
    </div>-->
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('paginas/admin_paginas/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
</div>