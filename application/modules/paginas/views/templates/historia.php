<div class="conteudo paginas blog post <?=$pagina; ?>">
    <h1 class="titulo">/História</h1>
    <div id="tabs">
    	<ul>
    		<?php foreach ($historias as $historia): ?>
    			<li>
    				<a href="#tabs-<?php echo $historia->id ?>">
    					<?php echo $historia->ano ?>
    				</a>
    			</li>
    		<?php endforeach ?>
    	</ul>
    	<div class="clearfix"></div>
    	<?php foreach ($historias as $historia): ?>
    		<div id="tabs-<?php echo $historia->id ?>">
    		 	<?php echo $this->shortcodes->parse($historia->conteudo) ?>
    		</div>
    	<?php endforeach ?>
    </div>
    <div class="clearfix"></div>
</div>