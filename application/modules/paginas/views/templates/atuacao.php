<div class="conteudo pagina <?=$pagina; ?>">
    <? if(!is_null($dados_pagina->imagem)): ?>
        <div class="pagina-imagem">
            <img src="<?=base_url(); ?>assets/img/paginas/<?=$dados_pagina->imagem; ?>" alt="<?=$dados_pagina->titulo; ?>">
        </div>
    <? endif; ?>
    <div class="pagina-texto <?=(!is_null($dados_pagina->imagem)) ? 'has-image' : ''; ?>">
        <?=$dados_pagina->texto; ?>
        <h2>Conheça os nossos serviços</h2>
        <?=Modules::run('servicos/parcial'); ?>
    </div>
    <div class="clearfix"></div>
</div>