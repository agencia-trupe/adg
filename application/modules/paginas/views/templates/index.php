<div class="conteudo paginas blog post <?=$pagina; ?>">
    <h1 class="titulo">/<?php echo $dados_pagina->titulo?></h1>
    <article>
    	<header>
    		<h2 class="blog-titulo"><?php echo $dados_pagina->subtitulo ?></h2>
    	</header>
    	<?=$dados_pagina->texto; ?>
    </article>
    <div class="clearfix"></div>
</div>