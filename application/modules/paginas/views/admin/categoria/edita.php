<div class="row">
    <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Página</legend>
    <?=form_open_multipart('paginas/admin_categorias/processa'); ?>
    <?php if($acao == 'editar'): ?>
    <?=form_hidden( 'id', $object->id ); ?>
    <?php endif; ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ($acao == 'editar') ? $object->titulo : ''),
        'class' => 'span5'
    )); ?>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('paginas/admin_paginas/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
</div>