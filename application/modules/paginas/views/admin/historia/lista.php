<div class="row">
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>História <a href="<?=site_url('painel/historia/cadastrar'); ?>"
                       class="btn btn-mini btn-info">Cadastrar</a></legend>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Ano</th><th>Resumo</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($result as $pagina): ?>
                <tr>
                    <td><?php echo $pagina->ano; ?></td>
                    <td><?php echo get_excerpt($pagina->conteudo, 20); ?></td>
                    <td><?=anchor('paginas/admin_historias/editar/' . $pagina->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?></a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>