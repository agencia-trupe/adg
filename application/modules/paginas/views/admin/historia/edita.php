<div class="row">
    <legend><?=($acao == 'editar') ? 'Editar' : 'Cadatrar'; ?> Conteúdo &middot; História</legend>
    <?php if( $acao !== 'editar' )
    {
        $form = 'paginas/admin_historias/processa_cadastro';
    }
    else
    {
        $form = 'paginas/admin_historias/processa';
    }
    ?>
    <?=form_open_multipart($form); ?>
    <?php if($acao == 'editar'): ?>
    <?=form_hidden( 'id', $object->id ); ?>
    <?php endif; ?>
    <?=form_label('Ano'); ?>
    <?=form_input(array(
        'name' => 'ano',
        'value' => set_value('ano', ($acao == 'editar') ? $object->ano : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('ano'); ?>
        <br>
    <?=form_label('Conteúdo'); ?>
    <?=form_textarea(array(
        'name' => 'conteudo',
        'value' => set_value('conteudo', ($acao == 'editar') ? $object->conteudo : ''),
        'class' => 'tinymce span8'
    )); ?>
    <?=form_error('conteudo'); ?>
    <br>
    <!--<select>
        <?php //foreach( $templates as $template ): ?>
        <option value="<?//=str_replace('.php', '', $template); ?>">
            <?//=ucfirst(str_replace('.php', '', $template)); ?>
        </option>
        <?php //endforeach; ?>
    </select>-->
    <br>
    <div class="clearfix"></div>
    <br>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('paginas/admin_paginas/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
</div>