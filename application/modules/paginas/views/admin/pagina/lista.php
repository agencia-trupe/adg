<div class="row">
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Páginas <a href="<?=site_url('painel/paginas/cadastrar'); ?>"
                       class="btn btn-mini btn-info">Cadastrar</a></legend>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($result as $pagina): ?>
                <tr>
                    <td><?=$pagina->titulo; ?></td>
                    <td><?=anchor('paginas/admin_paginas/editar/' . $pagina->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?></a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>