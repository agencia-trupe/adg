<div class="row">
    <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Página</legend>
    <?php if( $acao !== 'editar' )
    {
        $form = 'paginas/admin_paginas/processa_cadastro';
    }
    else
    {
        $form = 'paginas/admin_paginas/processa';
    }
    ?>
    <?=form_open_multipart($form); ?>
    <?php if($acao == 'editar'): ?>
    <?=form_hidden( 'id', $object->id ); ?>
    <?php endif; ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ($acao == 'editar') ? $object->titulo : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo'); ?>
        <br>
    <?=form_label('Texto'); ?>
    <?=form_textarea(array(
        'name' => 'texto',
        'value' => set_value('texto', ($acao == 'editar') ? $object->texto : ''),
        'class' => 'tinymce span8'
    )); ?>
    <?=form_error('texto'); ?>
    <br>
    <!--<select>
        <?php //foreach( $templates as $template ): ?>
        <option value="<?//=str_replace('.php', '', $template); ?>">
            <?//=ucfirst(str_replace('.php', '', $template)); ?>
        </option>
        <?php //endforeach; ?>
    </select>-->
    <br>
    <div class="clearfix"></div>
    <br>

     <?php if($acao == 'editar' && $object->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/paginas/<?php echo $object->imagem; ?>" alt="" >
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?=($acao == 'editar') ? 'Alterar' : 'Cadastrar'; ?> Imagem</label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
    <!--<div class="row-fluid">
        <div class="alert alert-info span4">Criada em <?//=date( 'd/m/Y', $object->created ); ?></div>
        <div class="alert alert-success span4 ">Última atualização: 
            <?//=(isset($object->updated)) ? date( 'd/m/Y', $object->updated ) : 'Nunca'; ?></div>
    </div>-->
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('paginas/admin_paginas/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
</div>