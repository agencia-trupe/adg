<?php
/**
* File: blog_model.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Blog_model
 * 
 * @category ADG
 * @package  Models
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Blog_model extends DataMapperExt
{
	/**
	 * Nome da tabela.
	 * 
	 * @var string
	 */
	var $table = 'blog';

	/**
	 * Relacionamento 1 to N.
	 * 
	 * @var array
	 */
	var $has_many = array('tag');

	/**
	 * Auto popular o relacionamento.
	 * 
	 * @var boolean
	 */
	var $auto_populate_has_many = TRUE;

	function get_destaques()
	{
		$destaques = new Blog_model();
		$destaques->where('destaque', 1)->get(8);

		$result = array();

		foreach($destaques->all as $destaque)
		{
			$result[] = $destaque;
		}
		
		if( ! count($result)) return NULL;
		return $result;
	}

	function get_archive($ano = NULL)
	{
		$list_order = array(
			'YEAR(FROM_UNIXTIME(data))',
			'DESC'
			);
		if(!$ano)
		{
			$blog = $this->fetchAll($list_order);
		}
		else
		{
			$blog = $this->fetch_by_key($list_order, 'YEAR(FROM_UNIXTIME(data))', $ano);
		}
		$result = array();
		foreach($blog as $evento)
		{
			$result[date('Y', $evento->data)][str_replace("'", "", date('m', $evento->data))][] = $evento->id;
		}
		return $result;
	}

	function get_archive_content($ano, $mes, $limit, $offset)
	{
		$arquivo = new Blog_model();
		$arquivo->where(array(
			'YEAR(FROM_UNIXTIME(data))' => $ano,
			'MONTH(FROM_UNIXTIME(data))' => $mes,
			))->get($limit, $offset);
		$result = array();
		foreach($arquivo->all as $post)
		{
			$result[] = $post;
		}
		if(sizeof($result))
			return $result;
		return FALSE;
	}

	function get_related($object)
	{
		$tags = array();

		foreach($object->tag as $tag)
		{
			$tags[] = $tag->id;
		}

		$posts = new Blog_model();
		$posts->order_by('id', 'RANDOM');
		$posts->where_in_related('tag', 'id', $tags)->get(3);
		foreach($posts as $post)
		{
			$result[$post->id]['titulo'] = $post->titulo;
			$result[$post->id]['slug'] = $post->slug;
			$result[$post->id]['data'] = $post->data;
		}
		return $result;
	}

	function get_noticias_destaque()
	{
		$posts = new Blog_model();
		$posts->order_by('data', 'DESC');
		$posts->where_in_related('tag', 'slug', 'noticias')->get(3);
		foreach($posts as $post)
		{
			$result[] = $post;
		}
		return $result;
	}
}
/* End of file blog_model.php */
/* Location: ./modules/blog/models/blog_model.php */