<div class="conteudo blog post">
	<h1 class="titulo">/Blog</h1>
		<article>
				<h1 class="blog-titulo"><?php echo $object->titulo ?></h1>
				<div class="meta">
					<div class="blog-post-data">
						<?php echo date('d', $object->data)
						 			. '.' . month_name($object->data, NULL, TRUE)
						 			. '.' . date('Y', $object->data)?>
					</div>
					<div class="blog-post-tags">
						<?php foreach ($object->tag as $tag): ?>
							<a href="<?php echo site_url('blog/tag/' . $tag->slug) ?>">
								<span><?php echo $tag->nome ?> </span>
							</a>
						<?php endforeach ?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="blog-post-texto">
					<?php echo $this->shortcodes->parse($object->texto) ?>
				</div>
		</article>
		<div class="relacionados">
			<h2>Posts relacionados</h2>
			<?php foreach ($relacionados as $relacionado): ?>
				<a href="<?php echo site_url('blog/post/' . $relacionado['slug']); ?>">
					<span class="relacionado-data">
						<?php echo date('d.m.Y', $relacionado['data']); ?>
					</span>
					<span class="relacionado-titulo">
						<?php echo $relacionado['titulo']; ?>
					</span>	
				</a>
				<div class="clearfix"></div>
			<?php endforeach ?>
		</div>
</div>