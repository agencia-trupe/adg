<div class="conteudo blog destaques">
		<h1>Destaques</h1>
		<?php if($result): ?>
			<?php foreach ($result as $object): ?>
				<article>
					<a href="<?php echo site_url('blog/post/' . $object->slug) ?>">
						<span class="img-destaque">
							<img src="<?php echo base_url('assets/img/blog/destaques/thumbs/' . $object->imagem) ?>" alt="">
						</span>
						<div class="meta">
							<div class="blog-lista-data">
								<?php echo date('d', $object->data)
								 			. ' ' . ucfirst(month_name($object->data, TRUE, FALSE))
								 			. ' ' . date('Y', $object->data) ?>
							</div>
							<div class="clearfix"></div>
						</div>
						<h2 class="blog-lista-titulo">
							<?php echo $object->titulo ?>
						</h2>
					</a>
				</article>
			<?php endforeach ?>
		<?php endif; ?>
</div>