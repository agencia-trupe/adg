<div class="noticias home">
	<div class="separador"></div>
	<h1>Notícias</h1>
	<?php if (isset($noticias)): ?>
		<?php foreach ($noticias as $noticia): ?>
			<a href="<?php echo site_url('blog/post/' . $noticia->slug) ?>" class="noticia-home">
				<div class="noticia-data">
					<?php echo date('d', $noticia->data)
						. ' ' . ucfirst(month_name($noticia->data, TRUE, FALSE))
						. ' ' . date('Y', $noticia->data) ?>
				</div>
				<span class="noticia-titulo">
					<h2><?php echo $noticia->titulo ?></h2>
				</span>
			</a>
			 
		<?php endforeach ?>
		<div class="clearfix"></div>
		<a href="<?php echo site_url('blog/tag/noticias') ?>" class="more">Mais notícias</a>
	<?php endif ?>
</div>