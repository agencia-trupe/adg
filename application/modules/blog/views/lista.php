<div class="conteudo blog lista">
	<h1 class="titulo">/Blog</h1>
	<ul>
	<?php if(isset($is_archive)): ?>
		<?php if($result): ?>
			<h2>Arquivo: <?php echo ucfirst(month_name($mes, FALSE, FALSE)); ?>
				&middot; <?php echo $ano; ?></h2>
			<?php foreach ($result as $object): ?>
				<article>
					<a href="<?php echo site_url('blog/post/' . $object->slug) ?>">
						<h1 class="blog-titulo"><?php echo $object->titulo ?></h1>
						<div class="meta">
							<div class="blog-lista-data">
								/ <?php echo date('d', $object->data)
								 			. '.' . month_name($object->data, NULL, TRUE)
								 			. '.' . date('Y', $object->data)?>
							</div>
							<div class="blog-lista-tags">
								<?php foreach ($object->tag as $tag): ?>
									<span><?php echo $tag->nome ?></span> 
								<?php endforeach ?>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="blog-lista-excerpt">
							<?php echo get_excerpt($object->texto, 38) ?>
						</div>
					</a>
				</article>
			<?php endforeach ?>
		<?php else: ?>
			<h3>Conteúdo não encontrado</h3>
			<p>Não foi encontrada nenhuma postagem referente ao período
				selecionado. Navegue pelos arquivos através do menu à esquerda,
				ou utilize a busca.
			</p>
		<?php endif; ?>
	<?php elseif(isset($is_tag)): ?>
		<?php if($result): ?>
			<h2>Tag: <?php echo ucfirst($tag_name); ?></h2>
			<?php foreach ($result as $object): ?>
				<article>
					<a href="<?php echo site_url('blog/post/' . $object->slug) ?>">
						<h1 class="blog-titulo"><?php echo $object->titulo ?></h1>
						<div class="meta">
							<div class="blog-lista-data">
								/ <?php echo date('d', $object->data)
								 			. '.' . month_name($object->data, NULL, TRUE)
								 			. '.' . date('Y', $object->data)?>
							</div>
							<div class="blog-lista-tags">
								<?php foreach ($object->tag as $tag): ?>
									<span><?php echo $tag->nome ?></span> 
								<?php endforeach ?>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="blog-lista-excerpt">
							<?php echo get_excerpt($object->texto, 38) ?>
						</div>
					</a>
				</article>
			<?php endforeach ?>
		<?php else: ?>
			<h3>Conteúdo não encontrado</h3>
			<p>Não foi encontrada nenhuma postagem referente ao período
				selecionado. Navegue pelos arquivos através do menu à esquerda,
				ou utilize a busca.
			</p>
		<?php endif; ?>
	<?php else: ?>
		<?php foreach ($result as $object): ?>
			<article>
				<a href="<?php echo site_url('blog/post/' . $object->slug) ?>">
					<h1 class="blog-titulo"><?php echo $object->titulo ?></h1>
					<div class="meta">
						<div class="blog-lista-data">
							/ <?php echo date('d', $object->data)
							 			. '.' . month_name($object->data, NULL, TRUE)
							 			. '.' . date('Y', $object->data)?>
						</div>
						<div class="blog-lista-tags">
								<?php foreach ($object->tag as $tag): ?>
									<span><?php echo $tag->nome ?></span> 
								<?php endforeach ?>
							</div>
						<div class="clearfix"></div>
					</div>
					<div class="blog-lista-excerpt">
						<?php echo get_excerpt($object->texto, 38) ?>
					</div>
				</a>
			</article>
		<?php endforeach ?>
	<?php endif; ?>
	</ul>
	<?php echo $this->pagination->create_links() ?>
</div>