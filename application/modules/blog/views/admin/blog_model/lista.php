<div class="row">
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend>Postagens <a href="<?=site_url('painel/blog/cadastrar'); ?>"
                       class="btn btn-small btn-info">Cadastrar</a></legend>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span1">Data de publicação</th><th>Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($result as $pagina): ?>
                <tr>
                    <td><?php echo date('d/m/Y', $pagina->data) ?></td>
                    <td><?=$pagina->titulo; ?></td>
                    <td><?=anchor('painel/blog/editar/' . $pagina->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?></a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>