<?php
/**
* File: blog.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Blog
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Blog extends BlogController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	public  $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
	}
	/**
	 * Carrega a ação principal
	 * 
	 * @return object um objeto view
	 */
	public function index()
	{
		$this->lista();
	}

	public function destaques()
	{
		$data['result'] = $this->blog->get_destaques(8);
		$this->load->view('blog/destaques', $data);
	}

	/**
	 * Exibe a lista com todos os posts publicados
	 * 
	 * @return void
	 */
	public function lista()
	{      
		$this->load->library('pagination');
		$pagination_config = array(
					'base_url'       => base_url() . 'blog/lista',
					'total_rows'     => $this->db->get('blog')->num_rows(),
					'per_page'       => 3,
					'num_links'      => 5,
					'next_link'      => '&gt',
					'prev_link'      => '&lt',
					'first_link'     => FALSE,
					'last_link'      => FALSE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		$this->pagination->initialize($pagination_config);

		//Obtendo resultados no banco
		$this->data['result'] = $this
								->blog
								->fetch_limit($pagination_config['per_page'], 
									$this->uri->segment(5));

		//Biblioteca de SEO
		$seo_tags = array(
			'title' => 'Calendário de Eventos',
			'description' => 'Calendário de Eventos da ADG - Associação de 
			Designers gráficos do Brasil'
			);
		$this->load->library('seo', $seo_tags);
		
		//Definição de parcial de conteúdo e retorno da view.
		$this->data['conteudo'] = 'blog/lista';
		$this->load->view('layout/template', $this->data);
	}
	/**
	 * Exibe uma página baseando-se em um slug
	 * 
	 * @param string $slug string da página a ser exibida.
	 * 
	 * @return void
	 */
	public function post($slug)
	{
		//busca os dados de uma página cujo slug foi passado como parâmetro
		$this->data['object'] = $this->blog->fetch_object('slug', $slug);
		$this->data['relacionados'] = $this->blog->get_related($this->data['object']);

		//Carrega a biblioteca de SEO e a inicializa.
		$seo_tags = array(
			'title' => $this->data['object']->titulo,
			'description' => $this->data['object']->description,
			);
		$this->load->library('seo', $seo_tags);

		$this->data['conteudo'] = 'blog/post';
		$this->load->view('layout/template', $this->data);
	}
	/**
	 * Obtem o slug de uma categoria baseando-se em seu id
	 * 
	 * @param integer $categoria_id id da categoria.
	 * 
	 * @return string                slug da categoria
	 */
	private function _get_categoria_slug($categoria_id)
	{
		return $this->categoria->getSlug($categoria_id);
	}

	public function shortcodes()
	{
		$str = '[ai:imagem nome=teste|alt=foto de teste]';
        
        // Parse the string using the shortcode lib
        $this->load->library('shortcodes');
        $str = $this->shortcodes->parse($str);
        
        echo $str;
	}

	public function noticias_home()
	{
		$data['noticias'] = $this->blog->get_noticias_destaque();
		$this->load->view('blog/noticias_home', $data);
	}
}
/* End of file blog.php */
/* Location: ./modules/blog/controllers/blog.php */