<?php
/**
* File: Admin_blog.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Admin_blog
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Admin_blog extends CrudController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	var $data;

	/**
	 * Critério de ordenação da lista do CRUD.
	 *
	 *  @var boolean
	 */
	var $list;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct($model_path = 'blog/blog_model', $module = 'blog');
		$this->list = array('data', 'DESC');
		$this->load->model('tags/tag');
		$this->load->model('blog/blog_model', 'blog');
        $this->data['painel_module'] = 'blog';
	}

	    public function processa()
    {
        $model = $this->model;
        if(!$this->form_validation->run($this->model))
        {
            $this->data['acao'] = 'editar';
            $this->data['object'] = $this->$model->fetch_object('id', $this->input->post( 'id' ), 'id');
            $this->data['conteudo'] = $this->data['module'] . '/admin/edita';
            $this->load->view('start/template', $this->data);
        }
        else
        {
            $post = array();

            //Itera sobre os dados do post e atribui suas chaves e valores para
            //o array $post
            foreach($_POST as $key => $value)
            {
                $post[$key] = $value;
            }
            
            //Verifica se algum dos dados do post é do tipo data e realiza 
            //as conversões necessárias
            $post['data'] = $this->_filtra_data($post['data']);

            if(! isset($post['destaque']))
            {
                $post['destaque'] = FALSE;
            }

            if(sizeof($_FILES) && strlen($_FILES["imagem"]["name"])>0)
            {
                //Configurações para o upload
                $config['upload_path'] = './assets/img/blog/destaques';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2000';
                $config['max_width']  = '1600';
                $config['max_height']  = '1200';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('imagem'))
                {

                    //Caso a tentativa de upload não seja bem sucedida, retorna
                    //para a página de edição, exibindo o erro
                    $this->data['error'] = array('error' => $this->upload->display_errors());
                    $this->data['acao'] = 'editar';
                    $this->data['object'] = $this->blog->fetch_object('id', $post['id'] );
                    $this->data['conteudo'] = 'blog/admin/blog_model/edita';
                    $this->load->view('start/template', $this->data);
                }
                else
                {

                    $this->load->library('image_moo');
                    //Resposta do upload
                    $upload_data = $this->upload->data();

                    //Caminho para o arquivo atual
                    $file_uploaded = $upload_data['full_path'];
                    //Caminho para o arquivo alterado
                    $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                    $thumbnail = $upload_data['file_path'] . '/thumbs/' . $upload_data['file_name'];

                    if(
                        $this->image_moo->load($file_uploaded)
                            ->resize(507,269,$pad=FALSE)
                            ->save($new_file,true)
                        &&
                        $this->image_moo->load($file_uploaded)
                            ->resize_crop(268, 172)
                            ->save($thumbnail, true)
                        )
                    {
                        $post['imagem'] = $upload_data['file_name'];
                    }
                    else
                    {
                        //Caso a tentativa de alteração da imagem não seja 
                        //bem sucedida, retorna para a página de edição, 
                        //exibindo o erro
                        $this->data['error'] = 'Erro de upload';

                        $this->data['error'] = array('error' => $this->upload->display_errors());
                        $this->data['acao'] = 'editar';
                        $this->data['object'] = $this->blog->fetch_object('id', $post['id'] );
                        $this->data['conteudo'] = 'blog/admin/blog_model/edita';
                        $this->load->view('start/template', $this->data);
                    }
                }
            }
            
            if($this->$model->change($post))
            {
                $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                redirect('painel/' . $this->data['module']);
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/' . $this->data['module'] . '/edita/' . $post['id']);
            }
        }
    }

    public function processa_cadastro()
    {
        if(!$this->form_validation->run($this->model))
        {

            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'blog/admin/edita';
            $this->load->view('start/template', $this->data);
        }
        else
        {

            $post = array();

            //Itera sobre os dados do post e atribui suas chaves e valores para
            //o array $post
            foreach($_POST as $key => $value)
            {
                $post[$key] = $value;
            }

            //Obtem o slug da postagem, baseando-se no título enviado
            $this->load->helper('blog');
            $post['slug'] = get_slug($post['titulo']);

            //Converte a data de um formato para leitura de humanos para uma 
            //unix timestamp
            $post['data'] = $this->_filtra_data($post['data']);

            if(sizeof($_FILES) && strlen($_FILES["imagem"]["name"])>0)
            {
                //Configurações para o upload
                $config['upload_path'] = './assets/img/blog/destaques';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2000';
                $config['max_width']  = '1600';
                $config['max_height']  = '1200';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('imagem'))
                {

                    //Caso a tentativa de upload não seja bem sucedida, retorna
                    //para a página de edição, exibindo o erro
                    $this->data['error'] = array('error' => $this->upload->display_errors());
                    $this->data['acao'] =  'cadastrar';
                    $this->data['conteudo'] = 'blog/admin/blog_model/edita';
                    $this->load->view('start/template', $this->data);
                }
                else
                {

                    $this->load->library('image_moo');
                    //Resposta do upload
                    $upload_data = $this->upload->data();

                    //Caminho para o arquivo atual
                    $file_uploaded = $upload_data['full_path'];
                    //Caminho para o arquivo alterado
                    $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                    $thumbnail = $upload_data['file_path'] . '/thumbs/' . $upload_data['file_name'];

                    if(
                        $this->image_moo->load($file_uploaded)
                            ->resize(507,269,$pad=FALSE)
                            ->save($new_file,true)
                        &&
                        $this->image_moo->load($file_uploaded)
                            ->resize_crop(268, 172)
                            ->save($thumbnail, true)
                        )
                    {
                        $post['imagem'] = $upload_data['file_name'];
                    }
                    else
                    {
                        //Caso a tentativa de alteração da imagem não seja 
                        //bem sucedida, retorna para a página de edição, 
                        //exibindo o erro
                        $this->data['error'] = 'Erro de upload';

                         //Caso a tentativa de upload não seja bem sucedida, retorna
                        //para a página de edição, exibindo o erro
                        $this->data['error'] = array('error' => $this->upload->display_errors());
                        $this->data['acao'] =  'cadastrar';
                        $this->data['conteudo'] = 'blog/admin/blog_model/edita';
                        $this->load->view('start/template', $this->data);
                    }
                }
            }

            if($this->blog->insert($post))
            {
                $this->session->set_flashdata('success', 'Registro incluido com sucesso');
                redirect('painel/blog');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível incluir o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/blog');
            }
        }
    }

    private function _filtra_data($post)
    {
        $data = explode('/', $post);
        $data = $data[1] . '/' . $data[0] . '/' . $data[2];
        return strtotime($data);
    }
}
/* End of file admin_blog.php */
/* Location: ./modules/blog/controllers/admin_blog.php */