<?php
/**
* File: arquivo.php
* 
* PHP version 5.3
*
* @category ADG
* @package  ADG
* @author   Nilton de Freitas <nilton@trupe.net>
* @license  copyright  http://trupe.net
* @link     http://trupe.net  
*/

/**
 * Class Arquivo
 * 
 * @category ADG
 * @package  Controllers
 * @author   Nilton de Freitas <nilton@trupe.net>
 * @license  copyright http://trupe.net
 * @link     http://trupe.net/  
 **/
class Arquivo extends BlogController
{
	/**
	 * Array de variáveis passadas para a view.
	 * 
	 * @var array;
	 */
	public  $data;

	/**
	 * Construtor
	 */
	public function __construct()
	{
		parent::__construct();
		//$this->data['archive'] = $this->_get_archive('array');
		$this->data['is_archive'] = TRUE;
	}
	/**
	 * Carrega a ação principal
	 * 
	 * @return object um objeto view
	 */
	public function index()
	{
	}

	/**
	 * Exibe a lista com todos os posts publicados
	 * 
	 * @return void
	 */
	public function mes($ano, $mes = NULL)
	{      
		$this->load->library('pagination');
		$pagination_config = array(
					'total_rows'     => $this->db
										->where(array(
											'YEAR(FROM_UNIXTIME(data))' => $ano,
											'MONTH(FROM_UNIXTIME(data))' => $mes
											))->get('blog')->num_rows(),
					'per_page'       => 1,
					'num_links'      => 5,
					'next_link'      => '&gt',
					'prev_link'      => '&lt',
					'first_link'     => FALSE,
					'last_link'      => FALSE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		if($mes == NULL)
		{
			$mes = '01';	
		}
		
		$pagination_config['base_url'] = base_url() . 'blog/arquivo/mes/' . $ano . '/' . $mes . '/';
		$pagination_config['uri_segment'] = '6';
		
		//Obtendo resultados no banco
		$this->data['result'] = $this
							->blog
						->get_archive_content($ano, $mes, $pagination_config['per_page'], 
									$this->uri->segment(6));
		$this->pagination->initialize($pagination_config);

		

		//Biblioteca de SEO
		$seo_tags = array(
			'title' => 'Calendário de Eventos',
			'description' => 'Calendário de Eventos da ADG - Associação de 
			Designers gráficos do Brasil'
			);
		$this->load->library('seo', $seo_tags);
		
		//Definição de parcial de conteúdo e retorno da view.
		$this->data['mes'] = $mes;
		$this->data['ano'] = $ano;
		$this->data['conteudo'] = 'blog/lista';
		$this->load->view('layout/template', $this->data);
	}

	public function ano($ano)
	{      
		$this->load->library('pagination');
		$pagination_config = array(
					'total_rows'     => $this->db->get('blog')->num_rows(),
					'per_page'       => 1,
					'num_links'      => 5,
					'next_link'      => '&gt',
					'prev_link'      => '&lt',
					'first_link'     => FALSE,
					'last_link'      => FALSE, 
					'full_tag_open'  => '<div class="pagination center"><ul>',
					'full_tag_close' => '</ul></div>',
					'cur_tag_open'   => '<li class="active"><a href="#">',
					'cur_tag_close'  => '</a></li>',
					'num_tag_open'   => '<li>',
					'num_tag_close'  => '</li>',
					'next_tag_open'   => '<li>',
					'next_tag_close'  => '</li>',
					'prev_tag_open'   => '<li>',
					'prev_tag_close'  => '</li>',
			);
		if($mes == NULL)
		{
			$mes = '01';	
		}
		
		$pagination_config['base_url'] = base_url() . 'blog/arquivo/' . $ano . '/' . $mes . '/';
		$pagination_config['uri_segment'] = '6';
		
		//Obtendo resultados no banco
		$this->data['result'] = $this
							->blog
						->get_archive_content($ano, $mes, $pagination_config['per_page'], 
									$this->uri->segment(6));
		$this->pagination->initialize($pagination_config);

		

		//Biblioteca de SEO
		$seo_tags = array(
			'title' => 'Calendário de Eventos',
			'description' => 'Calendário de Eventos da ADG - Associação de 
			Designers gráficos do Brasil'
			);
		$this->load->library('seo', $seo_tags);
		
		//Definição de parcial de conteúdo e retorno da view.
		$this->data['conteudo'] = 'blog/lista';
		$this->load->view('layout/template', $this->data);
	}

	/**
	 * Exibe a lista com todos os posts publicados
	 * 
	 * @return void
	 */
	public function serve_json($ano)
	{
		$archive = $this->_get_archive('array', $ano);
		$response = NULL;
		foreach ($archive as $year => $months)
		{
			foreach($months as $key => $value)
			{
				$response .= '<li><a class="archive-link" href="';
				$response .= site_url('blog/arquivo/mes/' . $year . '/' . $key);
				$response .= '">' . month_name($key, FALSE, TRUE);
				$response .= ' <small>(<span>'; 
				$response .= str_pad(count($key), 2, '0', STR_PAD_LEFT);
				$response .= '</span>)</small></a></li>';	
			}
		}

		echo $response;
	}
	/**
	 * Exibe uma página baseando-se em um slug
	 * 
	 * @param string $slug string da página a ser exibida.
	 * 
	 * @return void
	 */
	public function post($slug)
	{
		//busca os dados de uma página cujo slug foi passado como parâmetro
		$this->data['object'] = $this->blog->fetch_object('slug', $slug);

		//Carrega a biblioteca de SEO e a inicializa.
		$seo_tags = array(
			'title' => $this->data['object']->titulo,
			'description' => $this->data['object']->description,
			);
		$this->load->library('seo', $seo_tags);

		$this->data['conteudo'] = 'blog/post';
		$this->load->view('layout/template', $this->data);
	}
	/**
	 * Obtem o slug de uma categoria baseando-se em seu id
	 * 
	 * @param integer $categoria_id id da categoria.
	 * 
	 * @return string                slug da categoria
	 */
	private function _get_categoria_slug($categoria_id)
	{
		return $this->categoria->getSlug($categoria_id);
	}

	public function shortcodes()
	{
		$str = '[ai:imagem nome=teste|alt=foto de teste]';
        
        // Parse the string using the shortcode lib
        $this->load->library('shortcodes');
        $str = $this->shortcodes->parse($str);
        
        echo $str;
	}

	/**
	 * Obtem o arquivo do calendário retornando no formato passado como
	 * parâmetro.
	 * 
	 * @param mixed $format json ou array
	 * 
	 * @return mixed retorno no formato passado como parâmetro
	 */
	private function _get_archive($format, $ano = NULL)
	{
		if($ano == NULL)
		{
			return $this->blog->get_archive();
		}
		if($format == 'array')
		{
			return $this->blog->get_archive($ano);
		}
		else
		{
			$arquivo = $this->blog->get_archive($ano);
			$result = array();
			foreach($arquivo as $mes)
			{
				foreach($mes as $key => $value)
				{
					//$result[$key]['mes'] = $key;
					$result[$key]['mes'] = $key;
					$result[$key]['posts'] = count($value);
				}
			}
			return json_encode($result);
			//return json_encode($this->blog->get_archive($ano));
		}
	}
}
/* End of file blog.php */
/* Location: ./modules/blog/controllers/blog.php */