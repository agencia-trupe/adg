<?php 

$config = array(
     'pagina' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'texto',
                     'label' => 'texto',
                     'rules' => 'required'
                  ),                       
        ),
     'blog_model' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'texto',
                     'label' => 'texto',
                     'rules' => 'required'
                  ),
            array(
                     'field' => 'data',
                     'label' => 'data de publicação',
                     'rules' => 'required'
                  ),                       
        ),

     'especialidade' => array(
             array(
                     'field' => 'nome',
                     'label' => 'nome',
                     'rules' => 'required'
                  ),                  
        ),

     'perfil' => array(
             array(
                     'field' => 'nome',
                     'label' => 'nome',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'cidade',
                     'label' => 'cidade',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'especialidades',
                     'label' => 'especialidades',
                     'rules' => 'required'
                  ),
            array(
                     'field' => 'uf',
                     'label' => 'uf',
                     'rules' => 'required|max_length[2]'
                  ),                       
        ),
     'calendario_model' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'descricao',
                     'label' => 'descricao',
                     'rules' => 'required'
                  ),                       
        ),
     'tag' => array(
             array(
                     'field' => 'nome',
                     'label' => 'nome',
                     'rules' => 'required'
                  ),                      
        ),
     'historia' => array(
             array(
                     'field' => 'ano',
                     'label' => 'ano',
                     'rules' => 'required'
                  ), 
            array(
                     'field' => 'conteudo',
                     'label' => 'conteúdo',
                     'rules' => 'required'
                  ),                      
        ),
     'projetos' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'texto',
                     'label' => 'texto',
                     'rules' => 'required'
                  ),
            array(
                    'field' => 'data',
                    'label' => 'ano',
                    'rules' => 'required'
                ),                    
        ),
    'servicos' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'descricao',
                     'label' => 'descricao',
                     'rules' => 'required'
                  ),                       
        ),
    'dicas' => array(
            array(
                     'field' => 'titulo_nav',
                     'label' => 'título (navegação)',
                     'rules' => 'required'
                  ),
            array(
                     'field' => 'titulo_conteudo',
                     'label' => 'título (conteúdo)',
                     'rules' => 'required'
                  ),
            array(
                'field' => 'texto',
                'label' => 'texto',
                'rules' => 'required'
                ),
        ),
    'endereco' => array(
            array(
                'field' => 'endereco',
                'label' => 'endereço',
                'rules' => 'required' 
                ),
            array(
                'field' => 'bairro',
                'label' => 'bairro',
                'rules' => 'required' 
                ),
            array(
                'field' => 'cidade',
                'label' => 'cidade',
                'rules' => 'required' 
                ),
            array(
                'field' => 'uf',
                'label' => 'uf',
                'rules' => 'required|max_length[2]' 
                ),
            array(
                'field' => 'cep',
                'label' => 'cep',
                'rules' => 'required' 
                ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required' 
                ),
            array(
                'field' => 'telefone',
                'label' => 'telefone',
                'rules' => 'required' 
                ),
            array(
                'field' => 'twitter',
                'label' => 'twitter',
                'rules' => 'required' 
                ),
            array(
                'field' => 'facebook',
                'label' => 'facebook',
                'rules' => 'required' 
                ),
        ),
        'midia' => array(
            array(
                'field' => 'titulo',
                'label' => 'título',
                'rules' => 'required'  
            ),
        ),
        'contato' => array(
            array(
                'field' => 'nome',
                'label' => 'nome',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'telefone',
                'label' => 'telefone',
                'rules' => 'required'
            ),
            array(
                'field' => 'assunto',
                'label' => 'assunto',
                'rules' => 'required'
            ),
            array(
                'field' => 'mensagem',
                'label' => 'mensagem',
                'rules' => 'required|min_length[30]'
            ),
        )
     );