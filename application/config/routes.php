<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'home';

$route['login']  = 'auth/login';
$route['logout'] = 'auth/logout';
$route['cadastro'] = 'auth/register';

$route['painel'] = 'blog/admin_blog';
$route['painel/blog'] = 'blog/admin_blog';
$route['painel/calendario'] = 'calendario/admin_calendario';
$route['painel/paginas'] = 'paginas/admin_paginas';
$route['painel/historia'] = 'paginas/admin_historias';
$route['painel/tags'] = 'tags/admin_tags';
$route['painel/perfil'] = 'perfis/admin_perfis';
$route['painel/especialidades'] = 'perfis/admin_especialidades';


$route['painel/paginas/cadastrar'] = 'paginas/admin_paginas/cadastrar';
$route['painel/paginas/editar/(:any)'] = 'paginas/admin_paginas/editar/$1';
$route['painel/paginas/processa'] = 'paginas/admin_paginas/processa';
$route['painel/paginas/processa_cadastro'] = 'paginas/admin_paginas/processa_cadastro';


$route['painel/blog/cadastrar'] = 'blog/admin_blog/cadastrar';
$route['painel/blog/editar/(:any)'] = 'blog/admin_blog/editar/$1';
$route['painel/blog/processa'] = 'blog/admin_blog/processa';
$route['painel/blog/processa_cadastro'] = 'blog/admin_blog/processa_cadastro';

$route['painel/tags/cadastrar'] = 'tags/admin_tags/cadastrar';
$route['painel/tags/editar/(:any)'] = 'tags/admin_tags/editar/$1';
$route['painel/tags/processa'] = 'tags/admin_tags/processa';
$route['painel/tags/processa_cadastro'] = 'tags/admin_tags/processa_cadastro';

$route['painel/historia/cadastrar'] = 'paginas/admin_historias/cadastrar';
$route['painel/historia/editar/(:any)'] = 'paginas/admin_historias/editar/$1';
$route['painel/historia/processa'] = 'paginas/admin_historias/procepaginasssa';
$route['painel/historia/processa_cadastro'] = 'paginas/admin_historias/processa_cadastro';

$route['painel/calendario/cadastrar'] = 'calendario/admin_calendario/cadastrar';
$route['painel/calendario/editar/(:any)'] = 'calendario/admin_calendario/editar/$1';
$route['painel/calendario/processa'] = 'calendario/admin_blog/processa';
$route['painel/calendario/processa_cadastro'] = 'calendario/admin_blog/processa_cadastro';

$route['painel/perfil/cadastrar'] = 'perfis/admin_perfis/cadastrar';
$route['painel/perfil/editar'] = 'perfis/admin_perfis/editar';
$route['painel/perfil/processa'] = 'perfis/admin_perfis/processa';
$route['painel/perfil/processa_cadastro'] = 'perfis/admin_perfis/processa_cadastro';
$route['painel/perfil/adiciona_foto'] = 'perfis/admin_perfis/adiciona_foto';
$route['painel/perfil/deleta_foto'] = 'perfis/admin_perfis/deleta_foto';
$route['painel/perfil/sort_fotos'] = 'perfis/admin_perfis/sort_fotos';


$route['painel/especialidades/cadastrar'] = 'perfis/admin_especialidades/cadastrar';
$route['painel/especialidades/editar/(:any)'] = 'perfis/admin_especialidades/editar/$1';
$route['painel/especialidades/processa'] = 'perfis/admin_especialidades/processa';
$route['painel/especialidades/processa_cadastro'] = 'perfis/admin_especialidades/processa_cadastro';
$route['painel/especialidades/apagar/(:any)'] = 'perfis/admin_especialidades/apagar/$1';

$route['institucional'] = 'paginas/view/apresentacao';

$controller_exceptions = array('institucional', 'contato', 'busca','banners','designers','perfis', 'teste', 'evento','historia', 'site_midia', 'tags','blog/arquivo','blog/arquivo/lista', 'blog/tags','blog','painel','contato','auth','paginas', 'calendario');

$route['^((?!\b'.implode('\b|\b', $controller_exceptions).'\b).*)$'] = 'paginas/view/$1';
$route['blog/^((?!\b'.implode('\b|\b', $controller_exceptions).'\b).*)$'] = 'blog/post/$1';

$route['blog/arquivo/(:num)'] = 'blog/arquivo/lista/$1';
$route['blog/arquivo/(:num)/(:num)'] = 'blog/arquivo/mes/$1/$2';
$route['blog/arquivo/(:num)/(:num)/(:num)'] = 'blog/arquivo/mes/$1/$2/$3';
$route['blog/tag/(:any)'] = 'tags/lista/$1';

$route['evento/(:any)'] = 'calendario/view/$1';

$route['calendario/mes/(:num)/(:num)'] = 'calendario/lista_eventos_mes/$1/$2';
$route['calendario/mes/(:num)/(:num)/(:num)'] = 'calendario/lista_eventos_mes/$1/$2/$3';

$route['historia'] = 'paginas/historias';

$route['designers'] = 'perfis';
$route['designers/lista'] = 'perfis/lista';
$route['designers/lista/(:num)'] = 'perfis/lista/$1';
$route['designers/detalhe/(:any)'] = 'perfis/detalhe/$1';

$route['404_override'] = '';

$route['painel/slideshow'] = 'admin_slideshow';
$route['painel/slideshow/(:any)'] = 'admin_slideshow/$1';
$route['painel/slideshow/(:any)/(:any)'] = 'admin_slideshow/$1/$2';

$route['painel/banners'] = 'banners/admin_banners';
$route['painel/banners/(:any)'] = 'banners/admin_banners/$1';
$route['painel/banners/(:any)/(:any)'] = 'banners/admin_banners/$1/$2';




/* End of file routes.php */
/* Location: ./config/routes.php */