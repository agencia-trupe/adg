<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Detalhes SEO
|
| Configurações básicas de SEO.
|--------------------------------------------------------------------------
*/
$config['site_name'] = 'ADG Brasil - Associação dos Designers Gráficos do Brasil';

$config['site_decription'] = 'Associação dos Designers Gráficos do Brasil.';

$config['site_keywords'] = 'ADG, Design, Design Gráfico, Designers Gráficos, Brasil, Associação';

$config['site_author'] = 'Trupe Agência Criativa - http://trupe.net';
/* End of file seo.php */
/* Location: ./application/config/seo.php */