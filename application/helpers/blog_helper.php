<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Blog Helper
 * 
 * @author      Nilton Freitas
 * @link        http://trupe.net
 */

// ------------------------------------------------------------------------

if( ! function_exists('get_excerpt'))
{
	function get_excerpt($text, $number_of_words) 
	{
	   // Where excerpts are concerned, HTML tends to behave
	   // like the proverbial ogre in the china shop, so best to strip that
	   $text = strip_tags($text);

	   // \w[\w'-]* allows for any word character (a-zA-Z0-9_) and also contractions
	   // and hyphenated words like 'range-finder' or "it's"
	   // the /s flags means that . matches \n, so this can match multiple lines
	   $text = preg_replace("/^\W*((\w[\w'-]*\b\W*){1,$number_of_words}).*/ms", '\\1', $text);

	   // strip out newline characters from our excerpt
	   return '<p>' . str_replace("\n", "", $text) . '[...]</p>';
	}
}

if( ! function_exists('get_slug'))
{
	function get_slug($text)
	{ 

	  	//Remove os acentos
       	$a = ' ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ'; 
        $b = '-aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr'; 
        $text = utf8_decode($text);     
        $text = strtr($text, utf8_decode($a), $b); 
        $text = strtolower($text); 
        $text = utf8_encode($text); 

	  	// replace non letter or digits by -
	  	$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	  	// trim
	  	$text = trim($text, '-');

	  	// transliterate
	  	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  	// lowercase
	  	$text = strtolower($text);

	  	// remove unwanted characters
	  	$text = preg_replace('~[^-\w]+~', '', $text);

	  	if (empty($text)) return 'n-a';

	  	return $text;
	}
}
