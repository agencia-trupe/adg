<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Videosc extends CI_Model
{

    function __construct ()
    {
        parent::__construct();
    }

    public function run ($params = array())
    {
        $str  = '<iframe id="ytplayer" type="text/html" width="507" height="390"';
        $str .= 'src="http://www.youtube.com/embed/' . $params['id'];
        $str .= '?autoplay=1&origin="' . site_url() . '" frameborder="0"/>';
        
        return $str;
    }
}