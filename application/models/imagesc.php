<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Imagesc extends CI_Model
{

    function __construct ()
    {
        parent::__construct();
    }

    public function run ($params = array())
    {
        $str  = '<img src="' . base_url('assets/img/images') . '/' . $params['nome'];
        $str .= '" alt="' . $params['alt'] . '">';
        
        return $str;
    }
}