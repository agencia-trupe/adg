<?php

/**
 * Event Calendar Class
 *
 * Build calendar with events linked to days.
 *
 * @license 	WTFPL
 * @package		YAPHPEC - Yet Another PHP Event Calendar
 * @category	YAPHPEC - Yet Another PHP Event Calendar
 * @author  	Nilton de Freitas
 * @link			http://nilton.name/yaphpec
 * @version 	0.1
 */
class Event_calendar
{

	public function __construct()
	{
		//$this->date = ($month && $year) ? strtotime($year . '-' . $month) : time();
		//$this->day = date('d', $this->date);
		//$this->month = date('m', $this->date);
		//$this->year = date('Y', $this->date);
	}

	/**
	 * Build an html calendar with events linked to days
	 * 
	 * @param  int $month targeted year
	 * @param  int $year  targeted month
	 * @param  array $data  array of events array('day' => '18', 'events' => array(), link' => 'url')
	 *
	 * @return array array containing month and calendar data
	 */
	public function build($year, $month, $data, $pad = FALSE)
	{

		//Here we generate the first day of the month 
		$first_day = mktime(0,0,0,$month, 1, $year); 

		//This gets us the translated month name 
		$title = $this->_getTranslatedName(date('m', $first_day), 'pt_br');

		//Here we find out what day of the week the first day of the month falls on 
		$day_of_week = date('D', $first_day); 

		//Once we know what day of the week it falls on, we know how many blank days occure before it. If the first day of the week is a Sunday then it would be zero
		switch($day_of_week){ 
				case "Sun": $blank = 0; break; 
				case "Mon": $blank = 1; break; 
				case "Tue": $blank = 2; break; 
				case "Wed": $blank = 3; break; 
				case "Thu": $blank = 4; break; 
				case "Fri": $blank = 5; break; 
				case "Sat": $blank = 6; break;
		} 
		//Get the previous and next month value
		switch ($month) {
				case $month == 1:
						$prev_month = 12;
						$next_month = $month + 1;
						$prev_year = $year - 1;
						$next_year = $year;
						break;
				
				case $month == 12:
						$next_month = 1;
						$prev_month = $month - 1;
						$next_year = $year + 1;
						$prev_year = $year;
						break;

				default:
						$prev_month = $month - 1;
						$next_month = $month + 1;
						$next_year = $year;
						$prev_year = $year;
						break;
		}

		//We then determine how many days are in the current month
		$days_in_month = cal_days_in_month(0, $month, $year); 

		//Here we start building the table heads 
		$calendar_head  = '<div class="event-calendar">';

		//$calendar_head .= "<tr>";
		//$calendar_head .= "<th colspan=1>$prev_link</th><th colspan=5> $title $year </th><th colspan=1>$next_link</th></tr>";


		//This counts the days in the week, up to 7
		$day_count = 1;

		//Now we start the calendar body
		$calendar_body = '<div class="calendar-row">';

		//first we take care of those blank days
		while ( $blank > 0 ) 
		{ 
				$calendar_body .= '<div class="blank-day"></div>';
				$blank = $blank-1; 
				$day_count++;
		} 

		//sets the first day of the month to 1 
		$day_num = 1;
		
		//Sets the dais wich contains events
		$event_days = array();
		foreach($data as $key => $value)
		{
			$event_days[] = $key;
		}

		//count up the days, untill we've done all of them in the month
		while ( $day_num <= $days_in_month ) 
		{
			if($pad) $day_num = str_pad($day_num, 2, '0', STR_PAD_LEFT);

			if($day_num == date('d'))
			{
				$calendar_body .= '<div class="day today">' . $day_num . '</div>';
			}
			elseif(isset($event_days) && in_array($day_num, $event_days))
			{
				if($pad)
				{
					$calendar_body .= '<div class="day"><a href="' . $data[str_pad($day_num, 2, '0', STR_PAD_LEFT)]['link'] . '">' . str_pad($day_num, 2, '0', STR_PAD_LEFT) . '</a></div>';
				}
				else
				{
					$calendar_body .= '<div class="day"><a href="' . $data[str_pad($day_num, 2, '0', STR_PAD_LEFT)]['link'] . '">' . $day_num . '</a></div>';
				}
			}
			else
			{
					$calendar_body .= '<div class="day">' . $day_num . '</div>'; 
			}
			
			$day_num++; 
			$day_count++;
			//Make sure we start a new row every week
			if ($day_count > 7)
			{
					$calendar_body .= '</div><div class="calendar-row">';
					$day_count = 1;
			}
		} 
		//Finaly we finish out the table with some blank details if needed
		while ( $day_count >1 && $day_count <=7 ) 
		{ 
				$calendar_body .= '<div class="blank-day"> </div>'; 
				$day_count++;
		}
		
		$calendar_body .= '</div><div class="clearfix"></div></div>';

		$calendar = $calendar_head . $calendar_body;

		$result = array(
			'month' => $month,
			'year'	=> $year,
			'month_name' => $title,
			'prev' => array('year' => $prev_year, 'month' => $prev_month),
			'next' => array('year' => $next_year, 'month' => $next_month),
			'calendar' => $calendar
			);
		return $result;
	}

	private function _getTranslatedName($month = '', $language = '')
	{
			if( $month < 1 || $month > 12 || $language != 'pt_br' )
			{
					return FALSE;
			}
			else
			{
					$monthNames = array(
							'01'  => 'Janeiro',
							'02'  => 'Fevereiro',
							'03'  => 'Março',
							'04'  => 'Abril',
							'05'  => 'Maio',
							'06'  => 'Junho',
							'07'  => 'Julho',
							'08'  => 'Agosto',
							'09'  => 'Setembro',
							'10' => 'Outubro',
							'11' => 'Novembro',
							'12' => 'Dezembro'
							);
					
					return $monthNames[$month];
			}
	} 
}