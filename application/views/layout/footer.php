			</div>
			<div class="clearfix"></div>
			<footer>
				<div class="master">
					<div class="description">
						<h3>ADG <span>Brasil</span></h3>
						<p>Lorem ipsum dolor sit amet, consectetur 
							adipisicing elit. Cupiditate inventore 
							odio explicabo quod esse reiciendis nisi
							 ex suscipit pariatur enim laborum et 
							 veritatis ea accusamus nobis eos vitae 
							 accusantium atque.
						<p>
					</div>
					<div class="social-footer">
						<h3>Conecte-se</h3>
						<ul>
							<li><a class="mail" href="#">contato@adg.org.br</a></li>
							<li><a class="twitter" href="#">@adgbr</a></li>
							<li><a class="facebook" href="#">facebook.com/adgbrasil</a></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<span class="copy">
					&copy; 2012 Concept Arquitetura &middot; Todos os direitos reservados
					&middot; Criação de sites: Trupe Agência Criativa 
					<a target="_blank" href="http://trupe.net">
						<img src="<?php echo base_url(); ?>assets/img/trupe.png" alt="Criação de Sites - Trupe Agência Criativa">
					</a>
				</span>
			</footer>
			</div>
		</div>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
		 <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
		  <script  src="<?php echo base_url(); ?>assets/js/jquery.dropkick-1.0.0.js"></script>
		<script src="<?php echo base_url('assets/prettyphoto/js/jquery.prettyPhoto.js'); ?>"></script>
		<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
		<script>
			var _gaq=[['_setAccount','UA-35751088-1'],['_trackPageview']];
			(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
			g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
			s.parentNode.insertBefore(g,s)}(document,'script'));
		</script>
	</body>
</html>
