<?php

$this->load->view('layout/header');

if(isset($categoria_mae))
	$this->load->view('layout/' . $categoria_mae . '_sidebar');

$this->load->view($conteudo);

if(! in_array($pagina, array('pagina', 'designers', 'home')))
	$this->load->view('layout/right_sidebar');

$this->load->view('layout/footer');

/* End of file template.php */
/* Location: ./views/layout/template.php */