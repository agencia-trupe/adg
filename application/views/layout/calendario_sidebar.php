<div class="sidebar">
    <?php echo $calendario_sidebar ?>
    <div class="drop-select">
        <select name="arquivo_eventos" id="arquivo_eventos" class="arquivo_eventos">
            <?php foreach ($archive as $key => $value): ?>
                <option value="<?php echo $key ?>" 
                    <?php echo (isset($is_archive)) ? ($key == $ano) ? 'selected' : '' : ($key == date('Y') ? 'selected' : '') ?>>
                    <?php echo $key ?></option>
            <?php endforeach ?>
        </select>
    </div>
    <div class="clearfix"></div>
    <nav class="calendario">
    	<ul class="arquivo">
        <?php 	foreach ($archive as $year => $months): ?>
        	<?php if(isset($is_archive)): ?>
                <?php if($year == $ano): ?>
                    <?php foreach($months as $key => $value): ?>
                        <li><a class="archive-link" href="#"><?php echo month_name($key, FALSE, TRUE)
                        . ' <small>(<span>' . count($key) . '</span>)</small>' ?></a></li>  
                    <?php  endforeach; ?>
                <?php endif; ?>
            <?php else: ?>
                <?php if($year == date('Y')): ?>
        		    <?php foreach($months as $key => $value): ?>
        				<li><a class="archive-link" href="#"><?php echo month_name($key, FALSE, TRUE)
        				. ' <small>(<span>' . count($key) . '</span>)</small>' ?></a></li>	
                    <?php endforeach; ?>
                <?php endif; ?>
        	<?php endif; ?>
        <?php endforeach ?>
    	</ul>
    </nav>
</div>