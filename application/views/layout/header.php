<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <?php $this->seo->build_meta(); ?>
        <meta name="viewport" content="width=device-width,
                               maximum-scale=1.0" />
        <link rel="stylesheet/less" href="<?php echo base_url(); ?>assets/css/main.less">
        <script src="<?php echo base_url(); ?>assets/js/vendor/less-1.3.0.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Dosis:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?=base_url('assets/prettyphoto/css/prettyPhoto.css'); ?>">
         <link rel="stylesheet" href="<?=base_url('assets/css/jquery.maximage.css'); ?>">

        <title><?=$this->seo->get_title(); ?></title>
    </head>
    <body>
        <div class="clearfix"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="wrapper">
            <div class="main">
                <header>
                    <div class="marca-adg left">
                        <a href="#">
                            <img src="<?=base_url('assets/img/marca-adg.png'); ?>" alt="<?=$this->seo->site_name()?>">
                        </a>
                    </div>
                    <div class="right-header right">
                        <div class="busca right">
                            <?php echo form_open('busca/query') ?>
                            <?php echo form_input(array('name'=>'query', 'value'=>set_value('query'), 'class'=>'query', 'placeholder'=>'Busca')) ?>
                            <?php echo form_close() ?>
                        </div>
                        <div class="login-socios right">
                            <a href="<?php echo site_url('auth/login') ?>">Login Sócio ADG</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="nav-wrapper">
                            <div class="clearfix"></div>
                            <nav>
                                <ul>
                                    <li>
                                        <a 
                                            class="<?php 
                                            echo ($pagina == 'institucional') 
                                            ? 'active' : 'nav-item'?>" 
                                            href="<?=site_url('institucional'); ?>">
                                            Institucional
                                        </a>
                                    </li>
                                    <li>
                                        <a 
                                            class="<?php 
                                            echo ($pagina == 'calendario') 
                                            ? 'active' : 'nav-item'?>"
                                            href="<?=site_url('calendario'); ?>">
                                            Calendário
                                        </a>
                                    </li>
                                    <li>
                                        <a 
                                            class="<?php 
                                            echo ($pagina == 'recursos') 
                                            ? 'active' : 'nav-item'?>"
                                            href="<?=site_url('recursos'); ?>">
                                            Recursos
                                        </a>
                                    </li>
                                    <li>
                                        <a 
                                            class="<?php 
                                            echo ($pagina == 'blog') 
                                            ? 'active' : 'nav-item'?>"
                                            href="<?=site_url('blog'); ?>">
                                            Blog
                                        </a>
                                    </li>
                                    <li>

                                        <a  class="<?php 
                                            echo ($pagina == 'encontre-designer') 
                                            ? 'active' : 'nav-item'?>"
                                            href="<?=site_url('encontre-designer'); ?>">
                                            Encontre um Designer
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="clearix"></div>
                </header>
                <div class="clearfix"></div>
                <div class="main_content">