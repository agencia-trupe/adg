<div class="right-sidebar">
	<?php echo Modules::run('banners/display', '5') ?>
	<div class="separador"></div>
	<h2>Destaques</h2>
	<?php echo Modules::run('calendario/destaque_unico') ?>
	<div class="separador"></div>
	<h2>Notícias</h2>
	<?php echo Modules::run('blog/noticias_home') ?>
	<?php echo Modules::run('twitter') ?>
	<div class="separador"></div>
	<h2>Facebook/ADGBR</h2>
	<div class="facebook-outer">
		<div class="facebook-inner">
            <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fadgbrasil&amp;width=300&amp;height=258&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;border_color=%23fff&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:258px;" allowTransparency="true"></iframe>
		</div>
	</div>
</div>