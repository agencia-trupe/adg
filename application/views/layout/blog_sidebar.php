<div class="sidebar">
	<h2>Arquivo:</h2>
	<select name="arquivo_ano" id="arquivo_ano" class="arquivo_ano">
		<?php foreach ($archive as $key => $value): ?>
			<option value="<?php echo $key ?>"><?php echo $key ?></option>
		<?php endforeach ?>
	</select>
	<div class="clearfix"></div>
    <nav class="calendario">
    	<ul class="arquivo">
        <?php 	foreach ($archive as $year => $months):
        			if($year == date('Y')):
        				foreach($months as $key => $value): ?>
        				<li><a class="archive-link" 
                                href="<?php echo site_url('blog/arquivo/mes/' . $year . '/' . $key) ?> ">
                                <?php echo month_name($key, FALSE, TRUE)
        				    . ' <small>(<span>' . str_pad(count($key), 2, '0', STR_PAD_LEFT) . '</span>)</small>' ?>
                            </a>
                        </li>	
        <?php 			endforeach;
        			endif;
        		endforeach ?>
    	</ul>
        <div class="separador-sidebar"></div>
        <ul class="tags">
            <h2 class="tags">Tags:</h2>
            <?php foreach ($tag_list as $key => $value): ?>
                <?php if($value['posts'] > 0): ?>
                    <li><a class="archive-link" 
                                    href="<?php echo site_url('blog/tag/' . $value['slug']); ?>">
                                    <?php echo $key
                                . ' <small>(<span>' . str_pad($value['posts'], 2, '0', STR_PAD_LEFT) . '</span>)</small>' ?>
                                </a>
                    </li>
                <?php endif; ?>
            <?php endforeach ?>
        </ul>
    </nav>
</div>