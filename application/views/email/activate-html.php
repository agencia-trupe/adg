<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Bem vindo à <?php echo $site_name; ?>!</title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
<h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;">Bem vindo à <?php echo $site_name; ?>!</h2>
Você foi cadastrado no site <?php echo $site_name; ?>. Abaixo seguem seus detalhes de acesso.<br />
Para verificar seu email, por favor clique no link abaixo:<br />
<br />
<big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #3366cc;">Complete seu registro...</a></b></big><br />
<br />
Se o link não funcionar, copie o endereço abaixo e cole na barra de endereços do seu navegador:<br />
<nobr><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #3366cc;"><?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?></a></nobr><br />
<br />
Por favor, verifique seu email em até <?php echo $activation_period; ?> horas. Se o email não for verificado, seu cadastro será cancelado e precisará ser refeito.<br />
<br />
<br />
<?php if (strlen($username) > 0) { ?>Seu nome de usuário: <?php echo $username; ?><br /><?php } ?>
Seu endereço de email: <?php echo $email; ?><br />
<?php if (isset($password)) {  ?>Sua senha: <?php echo $password; ?><br /><?php  } ?>
<br />
<br />

Atenciosamente,
<?php echo $site_name; ?>
</td>
</tr>
</table>
</div>
</body>
</html>