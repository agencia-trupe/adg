<div class="span12">
<div class="span6 offset5" style="margin-top:80px;">
    <div class="well">
        <div class="row-fluid">
                <h2 style="color:#0086b3;">Concept</h2>
                <hr>

                <p>Você precisa estar logado para efetuar esta ação. Utilize o botão abaixo para fazer login.</p>
                <div class="row-fluid">
                <?php
                echo anchor('login', 'Login', 'class="btn btn-small btn-info span3"');
                ?>
                </div>

                
        </div>
    </div>
</div>
</div>